import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MainMenuProvider, UserProvider } from '../providers/providers'; 

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';
  username: string;

  pages: any = {};

  constructor(public platform: Platform, private auth: UserProvider, public statusBar: StatusBar, public splashScreen: SplashScreen, public menuCtrl: MenuController, public mainMenu: MainMenuProvider, public storage: Storage, private alertCtrl: AlertController) {
    this.initializeApp();
  }

  ngAfterContentChecked() {
    let link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    let _href = link.getAttribute('href');
    
    if (_href != "https://10.170.49.104/pruhub/favicon.png") {   
      link.setAttribute("href", "https://10.170.49.104/pruhub/favicon.png"); 
    }
  }

  initializeApp() {
    this.mainMenu.openMenu(false);
    this.auth.logout();
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.mainMenu.activeChildPage = 'Login';
    });
  }

  openPage(page) {    
    if (page.state == 'none' || page.state == '-') {
      return false;
    }    
    this.mainMenu.activePage = page.name;
    this.mainMenu.activeChildPage = page.children[0].name;
  }
 
  openChildPage(page) {
    if (page.state == 'none' || page.state == '-') {
      return false;
    }
    if (page.state) {
      let split_state = page.state.split("-");

      if (split_state.length >= 2 ) {
        if (split_state.length == 2) {
          this.mainMenu.selectedMenu = split_state[1];
        }else if(split_state.length == 3){
          this.mainMenu.selectedMenu = split_state[2];
        }
      }else{
        this.nav.setRoot(page.state).catch(this.handleErrorLink);
      }
      this.mainMenu.activeChildPage = page.name;
    }else{
      page.checked = !page.checked;
    }      
    }

  handleErrorLink(error){
    console.warn(error);
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  openSubMenu(e){
    let body = document.getElementsByTagName('body')[0];
    body.classList.toggle('open-sub-menu');
  }

  public logout() {
    this.auth.logout();
    this.mainMenu.openMenu(false);
    this.nav.setRoot('LoginPage');
  }

  isAuthenticated() {
    return this.auth.loggedIn();
  }

  checkAvailablilityRoleDistributionList(roleDistributionListsMenu, n){ 
    if (this.auth.roleDistributionList) { 
      for (var i = roleDistributionListsMenu.length - 1; i >= 0; i--) { 
        let roleDistribution = { a: roleDistributionListsMenu[i]}
        for (var j = this.auth.roleDistributionList.length - 1; j >= 0; j--) { 
          if ( roleDistribution.a == this.auth.roleDistributionList[j].id.roleDistribution) { 
            i = 0; 
            j = 0; 
            return true; 
          } 
        } 
      } 
    }
  } 

  showError(text) { 
    let alert = this.alertCtrl.create({
      title: 'Login Failed',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  expandMenu(e, item){
    e.stopPropagation();
    item.checked = !item.checked;
  }
}
