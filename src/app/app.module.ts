import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
import localeIdExtra from '@angular/common/locales/extra/id';

import { MyApp } from './app.component';
import { CKEditorModule } from 'ng2-ckeditor';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MainMenuProvider, UserProvider, TaskProvider, MasterDataProvider, AdminProvider } from '../providers/providers';
import { URLServices } from '../providers/url-services';
import { SmsPage } from '../pages/sms/sms';
import { PruforcePage } from '../pages/pruforce/pruforce';
import { EmailPage } from '../pages/email/email';
import { SmsProvider } from '../providers/sms/sms';
import { PruforceProvider } from '../providers/pruforce/pruforce';
import { SelectAgentProvider } from '../providers/select-agent/select-agent';
import { EmailProvider } from '../providers/email/email';
import { EmailTemplateProvider } from '../providers/email-template/email-template';
import { BodyEmailTemplatePage } from '../pages/body-email-template/body-email-template';
import { EditTemplatePage } from '../pages/edit-template/edit-template';
import { AddTemplatePage } from '../pages/add-template/add-template';
import { AuditTrailProvider } from '../providers/audit-trail/audit-trail';
import { Report1Provider } from '../providers/report1/report1';
import { Report2Provider } from '../providers/report2/report2';
import { SearchModalPage } from '../pages/search-modal/search-modal';
import { EmailListPage } from '../pages/email/email-list';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { SearchModalPageModule } from '../pages/search-modal/search-modal.module';

registerLocaleData(localeId);

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
    mode: 'ios',
    backButtonText: ''
    }),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpModule,
    CKEditorModule,
    SelectSearchableModule,
    SearchModalPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    UserProvider,
    SplashScreen,
    MainMenuProvider,
    {provide: LOCALE_ID, useValue: 'id'}, 
    TaskProvider,
    URLServices,
    MasterDataProvider,
    MainMenuProvider,
    AdminProvider,
    SmsProvider,
    PruforceProvider,
    SelectAgentProvider,
    EmailProvider,
    AuditTrailProvider,
    Report1Provider,
    Report2Provider,
    EmailTemplateProvider
  ]
})
export class AppModule {}
