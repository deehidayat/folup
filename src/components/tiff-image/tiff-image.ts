import { Component, ElementRef, Input, Renderer } from '@angular/core';

declare var Tiff: any;
/**
 * Generated class for the TiffImageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tiff-image',
  templateUrl: 'tiff-image.html'
})
export class TiffImageComponent {

  @Input() public src: string;
  // @Input() public width?: number;
  // @Input() public height?: number;
  @Input() private type: string; 
  @Input() private thumb: string = "thumb"; 

  constructor(private _elementRef: ElementRef, private _renderer: Renderer) {
  //  console.log('Hello TiffImageComponent Component', Tiff);
  }

  ngOnInit() {
    let self = this;
    let wrapper = this._elementRef.nativeElement.querySelector('.wrapper-tiff');
    let xhr = new XMLHttpRequest();
    xhr.responseType = 'arraybuffer';
    xhr.open('GET', this.src);
    xhr.onload = function (e) {
      let element = null;
      if(xhr.status === 200) {
        let tiff = new Tiff({buffer: xhr.response});
        if (self.type === self.thumb) { 
          element = tiff.toCanvas();         
          var tempCanvas=document.createElement("canvas"); 
          var tctx=tempCanvas.getContext("2d"); 
          var pct = 1/(element.width/160); 
          var canvas = element; 
          var cw=canvas.width; 
          var ch=canvas.height; 
          tempCanvas.width=cw; 
          tempCanvas.height=ch; 
          tctx.drawImage(canvas,0,0); 
          canvas.width*=pct; 
          canvas.height*=pct; 
          var ctx=canvas.getContext('2d'); 
          ctx.drawImage(tempCanvas,0,0,cw,ch,0,0,cw*pct,ch*pct); 
        }else{ 
          element = tiff.toCanvas(); 
          var tempCanvas=document.createElement("canvas"); 
          var tctx=tempCanvas.getContext("2d"); 
          var pct = 1/(element.width/self._elementRef.nativeElement.parentElement.offsetWidth); 
          var canvas = element; 
          var cw=canvas.width; 
          var ch=canvas.height; 
          tempCanvas.width=cw; 
          tempCanvas.height=ch; 
          tctx.drawImage(canvas,0,0); 
          canvas.width*=pct; 
          canvas.height*=pct; 
          var ctx=canvas.getContext('2d'); 
          ctx.drawImage(tempCanvas,0,0,cw,ch,0,0,cw*pct,ch*pct); 
        } 
      } else if(xhr.status === 404) {
        element = self._renderer.createElement(wrapper, 'span');
        self._renderer.createText(element, 'File tidak ditemukan');
      } else {
        element = self._renderer.createElement(wrapper, 'span');
        self._renderer.createText(element, 'Terjadi kesalahan');
      }
        wrapper.parentNode.replaceChild(element, wrapper);
    };
    xhr.ontimeout = function (e) {
      console.log('timeout: ', e);
    }
    xhr.send();
  }

}
