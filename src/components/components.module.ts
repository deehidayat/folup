import { NgModule } from '@angular/core';
import { IonicModule} from 'ionic-angular';
import { TiffImageComponent } from './tiff-image/tiff-image';
@NgModule({
	declarations: [TiffImageComponent],
	imports: [
    IonicModule,
  ],
	exports: [TiffImageComponent]
})
export class ComponentsModule {}
