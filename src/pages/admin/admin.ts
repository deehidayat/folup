import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { UserProvider, AdminProvider } from './../../providers/providers'; 
import { MainMenuProvider } from './../../providers/providers'; 

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {
  filter : string = '';
  sorting : string = '-';
  sortByItem: any[] = [{value:'', text:'Default Sort', sort:''}, {value:'activityName', text:'Activity Name A-Z', sort:'asc'}, {value:'activityName', text:'Activity Name Z-A', sort:'desc'}, {value:'spajNumber', text:'SPAJ Number A-Z', sort:'asc'}, {value:'spajNumber', text:'SPAJ Number Z-A', sort:'desc'}, {value:'createdDate', text:'Created Date', sort:'asc'}];
  filterByItem: any[] = [{value:'', text:'Default Filter'},{value:'premiumIndicator-N', text:'Premium Indicator = N'},{value:'premiumIndicator-Abc1234567', text:'Premium Indicator = Abc1234567'}];
  tempItems: any[] = [];
  listUser: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: UserProvider, public mainMenu: MainMenuProvider, private adminProvider: AdminProvider, public storage: Storage) {
    this.storage.get('Authorization').then((Authorization) => {
      adminProvider.getListUser(Authorization)
        .subscribe((result) => {
          if (result.status == 200) {
            this.listUser = result.content;
          }
        }, (error) => {
          console.log(error);
        }); 
    });
  }
  
  ionViewWillEnter(){ 
    if(this.auth.loggedIn()){
      return true
    }else{
      this.auth.logout();
      this.navCtrl.setRoot('LoginPage');
    }
  } 

  openUserDetails(user) {
    this.navCtrl.push('UserDetailsPage', user);
  }
}
