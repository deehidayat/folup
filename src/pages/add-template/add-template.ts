import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BodyEmailTemplatePage } from '../../pages/body-email-template/body-email-template';
import { EmailTemplateProvider } from '../../providers/email-template/email-template';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Storage } from '@ionic/storage';
import * as CKEditor from '@ckeditor/ckeditor5-build-decoupled-document';

@IonicPage()
@Component({
  selector: 'page-add-template',
  templateUrl: 'add-template.html',
})
export class AddTemplatePage implements OnInit {

  public totalAllowedCharacterCount = 2000;
  public charactersLeft = 2000;

  config: any;
  id: any;
  subject: any;
  body: any;
  listError: any [];
  valid: boolean;
  loading:any;
  username:string;
  editor: any;
  BodyEmailTemplatePage: BodyEmailTemplatePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public emailTemplateProvider: EmailTemplateProvider,
  public toastCtrl:ToastController, public loadingCtrl: LoadingController, private storage: Storage) {
    this.config = {
      uiColor: '#FFFFFF',
      allowedContent: true,
      extraAllowedContent: '[id]',
      protectedSource: '/<i[^>]></i>*id/g',
      height: 200,
      // extraPlugins : 'wordcount',
      removeButtons: "Subscript,Superscript,SpecialChar,"
        + "Source,Styles,Clipboard,Format,Maximize,Strike,RemoveFormat,About,Print,"
        + "Preview,Save,NewPage,Templates,IncreaseIndent,Image,Flash,Table,-,DocProps,Cut,"
        + "Copy,Paste,PasteText,PasteFromWord,-,Undo,Redo,Image,Flash,able,HorizontalRule,"
        + "Smiley,SpecialChar,PageBreak,Iframe,Outdent,ShowBlocks,-,NumberedList,BulletedList-,"
        + "Outdent,Indent,-,Blockquote,CreateDiv,-,-,BidiLtr,BidiRtl,Find,Replace,-,SelectAll,-,"
        + "SpellChecker,Scayt,Form,Unlink,Checkbox,Radio,TextField,Textarea,Select,Button,"
        + "ImageButton,HiddenField,Anchor,BulletedList,-,Language,CopyFormatting",
      wordcount: {
        showCharCount: true,
        showWordCount: false,
        countHTML: false,
        maxCharCount: 2000
      }
    };
  }

  ngOnInit() {
    CKEditor
    .create(document.querySelector('#document-editor-editable'))
    .then(editor => {
        const toolbarContainer = document.querySelector('.document-editor__toolbar');

        toolbarContainer.appendChild( editor.ui.view.toolbar.element );
        this.editor = editor;
    }) 
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTemplatePage');
    this.storage.get('username').then((username) => {
      this.username = username;
    })
  }

  insertAgentName(event) {
    event.insertText("[[AgentName]]")
  }

  //Fungsi Tambah subject dan Body
  post (id, subject){  
    //validasi
    this.body = this.editor.getData();

    if ( this.subject == null && this.body == null || this.body == '<p>&nbsp;</p>'){
      this.showToast("Subject & Body Email are Required");
      return;
    }
    if ( this.subject == '' || this.subject == null){
      alert ('Subject Email is Required');
      return;
    }
    if ( this.body == '' || this.body == null || this.body == '<p>&nbsp;</p>'){
      alert ('Body Email is Required');
      return;
    }
    if (this.body != null && this.body.length > 2000) {
      this.showToast("The length of email body exceeds the max characters allowed (2000 chars)");
      return;
  }
    //end validasi

    let emailtemplates = ({
      id: this.id,
      subject: this.subject,
      body: this.body,
      username: this.username
    });

    this.showLoading();
    this.storage.get('Authorization').then((Authorization) => {
    this.emailTemplateProvider.postEmailTemplate(emailtemplates, Authorization)
    .subscribe(data => {
      this.navCtrl.push(BodyEmailTemplatePage, {
        id: id,
        subject: subject,
        body: this.body
      });
      this.loading.dismiss();
      this.showToast("Adding Email Template Successful")
      this.navCtrl.setRoot(BodyEmailTemplatePage);
    }, error => {
      this.showToast(error.message);
      return;
    });
  });
  }
  
  cancel(){
    this.navCtrl.popTo('BodyEmailTemplatePage');
  }

showToast(text) {
  let toast = this.toastCtrl.create({
      message: text,
      duration: 15000,
      position: 'top',
      showCloseButton: true
  });
  toast.present();
}

showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

currentCharacterCount() {
    this.body = this.editor.getData();
    let len = this.body.length;
    return len;
}

remainingAvailableCharacterCount() {
    this.body = this.editor.getData();
    let len = this.body.length;
    if (len <= 2000) {
      this.charactersLeft = this.totalAllowedCharacterCount - len;
  } 

    if (this.body == '<p>&nbsp;</p>') {
      this.charactersLeft = 2000;
    }
}

}
