import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddTemplatePage } from './add-template';
import { ComponentsModule } from './../../components/components.module';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  declarations: [
    AddTemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(AddTemplatePage),
    ComponentsModule,
    CKEditorModule
  ],
  entryComponents: [
    AddTemplatePage
  ]
})
export class AddTemplatePageModule {}
