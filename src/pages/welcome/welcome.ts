import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MainMenuProvider, UserProvider} from './../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public mainMenu: MainMenuProvider, private auth: UserProvider) {
    mainMenu.activePage = 'User Action';
    mainMenu.activeChildPage = 'New Business';
  }

  ionViewDidLoad() {
    
  }

  ionViewWillEnter(){ 
    if(this.auth.loggedIn()){
      return true
    }else{
      this.auth.logout();
      this.navCtrl.setRoot('LoginPage');
    }
  } 
}
