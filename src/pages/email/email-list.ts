import { Component, ViewChild } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,Loading } from 'ionic-angular';
import { SearchModalPage } from '../search-modal/search-modal';
import { EmailReceiverDetailsPage } from '../email-receiver-details/email-receiver-details';
// import { ToasterServiceProvider } from '../../providers/toaster-service/toaster-service';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';

import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { EmailProvider } from '../../providers/email/email';
import { InfiniteScroll } from 'ionic-angular/components/infinite-scroll/infinite-scroll';

@IonicPage()
@Component({
    selector: 'page-email-list',
    templateUrl: 'email-list.html',
})
export class EmailListPage {

    subject:String = "";
    startDate:Date;
    endDate:Date;
    loading:Loading;
    
    pSubject:String = "";
    pStartDate:Date;
    pEndDate:Date;

    body:any;
    emails:any;
    pageNumber:0;
    size: 10;
    lengthData:0;
    emailReceivers=[];
    id: any;
    username:string;
    searchOption:any;
    
    constructor(public http: HttpClient, public navCtrl: NavController, public navParams: NavParams, public emailProvider: EmailProvider,
        public modalCtrl: ModalController, public toastCtrl: ToastController, private storage: Storage,
        private loadingCtrl: LoadingController) {
            this.search();
            this.navCtrl.popToRoot();
            this.searchOption = {sendTime:false, subject:false};
    }

    ionViewDidLoad() {
        this.pageNumber = 0;
        this.size = 10;
        this.lengthData = 0;
        this.storage.get('username').then((username) => {
            this.username = username;
        })
    }
    
    search() {
        this.emails = [];
        this.pageNumber = 0;
        this.showLoading();
        this.pSubject = this.subject;
        this.pStartDate = this.startDate;
        this.pEndDate = this.endDate;
        this.getData(null);
    }

    //infinite scroll
    doInfinite(infiniteScroll) {
        console.log('Begin async operation');
        this.pageNumber++;
        this.getData(infiniteScroll);
    }

    getData(infiniteScroll) {
        this.storage.get('Authorization').then((Authorization) => {
            this.emailProvider.getFilterEmail(this.pSubject, this.pStartDate, this.pEndDate, this.pageNumber, this.size, Authorization)
            .subscribe(
            result => {                 
                for (let i = 0; i < result.length; i++) {
                    this.emails.push(result[i]);        
                }
                this.loading.dismiss();
                this.lengthData = result.length;
                if (null != infiniteScroll)
                    infiniteScroll.complete();
            },
            err => {
                this.loading.dismiss();
                // this.showToast(err.message);
            });
        });
    }

    openEdit(i, id, agentInvolved) {
          // Sharing data using NavController
        this.navCtrl.push('EmailReceiverDetailsPage', {
            id : id,
            agentInvolved : this.emails[i].agentInvolved
        });  
    }
      

    changeStatus(i, id) {
        this.showLoading();        
        this.storage.get('Authorization').then((Authorization) => {
          this.emailProvider.resendEmail(id, this.username, Authorization)
          .subscribe((data: [any]) => {
              this.emails[i].status = "Pending";
              this.showToast("Resend Email Success");
              this.loading.dismiss();
        }, (error) => {
            this.loading.dismiss();
        });
          }
        )
      }

    openCompose() {
        this.navCtrl.push('EmailPage');
    }

    showToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 15000,
            position: 'top',
            showCloseButton: true,
            cssClass: 'toast'
        });
        toast.present();
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        this.loading.present();
      }
}