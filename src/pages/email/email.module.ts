import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailPage } from './email';
import { CKEditorModule } from 'ng2-ckeditor';
import { SearchModalPage } from '../search-modal/search-modal';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { SelectSearchablePage } from 'ionic-select-searchable/select-searchable-page.component';

@NgModule({
  declarations: [
    EmailPage,
    //SearchModalPage,
    // SelectAgentPage

    //,
    //EmailListPage
  ],
  imports: [
    IonicPageModule.forChild(EmailPage),
    //IonicPageModule.forChild(EmailListPage),    
    CKEditorModule,
    SelectSearchableModule
  ],
  entryComponents: [
    //SearchModalPage,
    // SelectAgentPage,
    // SelectSearchablePage
  ]
})
export class EmailPageModule {}
