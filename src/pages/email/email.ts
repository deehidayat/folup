import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,Loading } from 'ionic-angular';
import { SearchModalPage } from '../search-modal/search-modal';
import { Storage } from '@ionic/storage';
import { RequestOptions, ResponseContentType, Http } from '@angular/http';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../providers/file-download-helper';
import { URLServices } from '../../providers/url-services';
import { SelectAgentProvider } from '../../providers/select-agent/select-agent';

import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { EmailProvider } from '../../providers/email/email';
import { InfiniteScroll } from 'ionic-angular/components/infinite-scroll/infinite-scroll';
import { SelectSearchable } from 'ionic-select-searchable';
import { EmailTemplateProvider } from '../../providers/email-template/email-template';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as CKEditor from '@ckeditor/ckeditor5-build-decoupled-document';


@IonicPage()
@Component({
    selector: 'page-email',
    templateUrl: 'email.html',
})
export class EmailPage implements OnInit {
    @ViewChild('fileInput') fileInput;
    @ViewChild('fileAgentCodes') fileAgentCodes;
    @ViewChild('documentEditorEditable') documentEditorEditable;

    public subjectModel: any;
    public bodyModel: any;
    public sendToModel: string[] = [];
    public todayDate: Date;

    public currentDate = new Date();
    public nowDate = new Date(this.currentDate.setHours(this.currentDate.getHours()+7)).toISOString();
    public maxDate = new Date(this.currentDate.setHours(this.currentDate.getHours()+239)).toISOString();
    public totalAllowedCharacterCount = 2000;
    public charactersLeft = 2000;

    // public agentTypes = ['AG', 'MA', 'AA', 'AD', 'FC', 'MF', 'AS', 'NS', 'NA', 'GAO'];
    public agency = ['AA','AD','AG', 'GAO', 'MA'];
    public fd = ['AS','FC', 'MF', 'NA' , 'NS'];

    public Names:any;
    public agent: any;
    public selectedAgentTypes: any;
    public officeCodes: any;

    public message: string;

    public send: any;
    public sendnow: boolean = true;
    public sendlater: boolean = true;
    public sendTimeModel: any;
    loading: Loading;
    files:any;

    public emailAttachmentsModel: string[] = [];
    emailAddressModel: string;
    agentTypeModel: string;
    nameModel: string;
    actionOption: string= 'now';
    actionOption1: string= 'manual';
    tempSendTime: any;
    fileSaved:Array<File> = [];

    public emailReceiversModel: any[] = [this.emailAddressModel];

    datePicker: any;

    public report: string;
    public success: string;

    public listError: string[];
    public valid: boolean;

    public sendtos: any;

    public config: {};

    public charlimit = 15;

    public isEmail: boolean = true;

    public sendTimeLater: any;
    public fileUpload: Array<File> = [];

    public myMinDate: String = new Date().toISOString();

    //radiobutton
    toggleButton: boolean;

    public minute = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];

    public a: any;
    public b: {};
    public chosenOption;
    public otherChosenOption;
    public name;
    public c: any;
    myMaxDate = 10;
    public availableOfficeCodes: any;
    fileBrowser: any;

    body:any;
    subject:any;
    pageNumber:0;
    lengthData=0;
    username:string;
    editor: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public emailProvider: EmailProvider,
        public modalCtrl: ModalController, public toastCtrl: ToastController, private storage: Storage,
        private loadingCtrl: LoadingController, public agentProvider: SelectAgentProvider, public emailTemplateProvider:
        EmailTemplateProvider, public urlServices: URLServices, private http:Http, private alertCtrl:AlertController) {
        this.b = navParams.get('b');
        this.config = {
            uiColor: '#FFFFFF',
            allowedContent: true,
            extraAllowedContent: '[id]',
            protectedSource: '/<i[^>]></i>*id/g',
            height: 200,
            // extraPlugins : 'wordcount',
            removeButtons: "Subscript,Superscript,SpecialChar,"
                + "Source,Styles,Clipboard,Format,Maximize,Strike,RemoveFormat,About,Print,"
                + "Preview,Save,NewPage,Templates,IncreaseIndent,Image,Flash,Table,-,DocProps,Cut,"
                + "Copy,Paste,PasteText,PasteFromWord,-,Undo,Redo,Image,Flash,able,HorizontalRule,"
                + "Smiley,SpecialChar,PageBreak,Iframe,Outdent,ShowBlocks,-,NumberedList,BulletedList-,"
                + "Outdent,Indent,-,Blockquote,CreateDiv,-,-,BidiLtr,BidiRtl,Find,Replace,-,SelectAll,-,"
                + "SpellChecker,Scayt,Form,Unlink,Checkbox,Radio,TextField,Textarea,Select,Button,"
                + "ImageButton,HiddenField,Anchor,BulletedList,-,Language,CopyFormatting",
            wordcount: {
                showCharCount: true,
                showWordCount: false,
                countHTML: false,
                maxCharCount: 2000
            }
        };
        this.sendtos;
        this.getOfficeCodes();
        
         // passing
        this.subject = navParams.get('subject');
        this.body = navParams.get('body');
        
    }

    ngOnInit() {
        CKEditor
        .create(document.querySelector('#document-editor-editable'))
        .then(editor => {
            const toolbarContainer = document.querySelector('.document-editor__toolbar');

            toolbarContainer.appendChild( editor.ui.view.toolbar.element );
            this.editor = editor;
        })
    }

    ionViewDidLoad() {
        this.storage.get('username').then((username) => {
            this.username = username;
        })
        this.actionOption = "now";
        this.agent="";
    }

    insertAgentName(event) {
        event.insertText("[[AgentName]]")
      }

    nowTime() {
        this.sendTimeModel = this.nowDate;
    }


    checklength() {
        if (this.bodyModel.length <= this.charlimit) {
            return this.bodyModel.length;
        }
    }

    download() {
        const url = this.urlServices.diskomApi+"/agentapi/downloadTemplate";
        const options = new RequestOptions({responseType: ResponseContentType.Blob });
    
        // Process the file downloaded
        this.http.get(url, options).subscribe(res => {
            const fileName = getFileNameFromResponseContentDisposition(res);
            saveFile(res.blob(), fileName);
        });
      }

    //fungsi buka search body email
    openTemplate() {
        var data = data;
        var myTemplate = this.modalCtrl.create(SearchModalPage, data);
        myTemplate.present();
        myTemplate.onDidDismiss(data => {
            if (null == data) {
            } else {
                // console.log("data=>", data);
                this.subjectModel = data.subject;
                this.bodyModel = data.body;
                this.editor.setData(this.bodyModel);
                this.charactersLeft = this.totalAllowedCharacterCount - this.bodyModel.length;
            }
        })
    }

    // fungsi accordion
    // start
    openAccordionBody(e){
        let target = e.currentTarget ? e.currentTarget : e.target;
        let element = target.nextElementSibling;
        let height = element.children[0].offsetHeight;
        if (height > 1000){element.setAttribute("transition-duration", "1000");}
        else{element.setAttribute("transition-duration", "300");}
        if (element.hasAttribute('style')){
          element.removeAttribute("style");
          target.children[0].removeAttribute('style');
          target.children[1].setAttribute("style", "display:none");
        }else{
          element.setAttribute("style", "height: "+height+"px;");
          target.children[1].removeAttribute('style');
          target.children[0].setAttribute("style", "display:none");
        }     
      }
    
    openChildAccordion(e){
        let element = e.currentTarget.nextElementSibling;
        let height = element.children[0].offsetHeight;
        let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
        let parentAccHeight = parentAcc.offsetHeight;

        if (parentAcc.className != 'accordion-body'){
            let element = e.target;
            while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
            parentAcc = element.parentElement;
            }
            parentAccHeight = parentAcc.offsetHeight;
        }
        
        e.stopPropagation();
        if (height > 1000){element.setAttribute("transition-duration", "1000");}
        else{element.setAttribute("transition-duration", "300");}
        if (element.hasAttribute('style')){
            element.removeAttribute("style");
            parentAcc.setAttribute("style", "height:"+(parentAccHeight-height)+"px");
            e.currentTarget.children[0].removeAttribute('style');
            e.currentTarget.children[1].setAttribute("style", "display:none");
        }else{
            element.setAttribute("style", "height:"+height+"px;");
            parentAcc.setAttribute("style", "height:"+(height+parentAccHeight)+"px");
            e.currentTarget.children[1].removeAttribute('style');
            e.currentTarget.children[0].setAttribute("style", "display:none");
        }
    }
    
    setParentHeight(e){
        let parent = e.currentTarget ? e.currentTarget : e.target;
        if (parent.className != 'accordion-body'){
            let element = e.target;
            while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
            parent = element.parentElement;
            }
        }
        setTimeout(()=>{
            let contentHeight = parent.children[0].offsetHeight;
            parent.setAttribute("style", "height:"+contentHeight+"px");
        },100);
    }
    
    _stopPropagation(e){
        e.stopPropagation();
    }

    accordionLoaded(e):boolean{
        let target = e.currentTarget ? e.currentTarget : e.target;
        let element = target.parentElement;
        let loaded = element.classList.contains('loaded');
        return loaded;
    }

    //end accordion function

    openSendTo(e) {
        if (!this.accordionLoaded(e)) {
            this.showLoading();
            this.loading.dismiss().then(()=>{this.openAccordionBody(e);});
            e.target.parentElement.classList.add('loaded');
        } else{
            this.openAccordionBody(e);
        }
    }

    getOfficeCodes() {
        this.storage.get('Authorization').then((Authorization) => {
            this.agentProvider.getFilterOfficeCode(Authorization).
            subscribe(data => {
                this.availableOfficeCodes = data;
                // console.log(data);
            }, err => {
                return;
            });
        })
    }

    selectAll() {
        this.reset();
        for(let i = 0;i < this.availableOfficeCodes.length;i++)
            this.officeCodes.push(this.availableOfficeCodes[i]);
    }

    reset() {
        this.officeCodes = [];
    }


    onChange(event: any) {
        let files = [].slice.call(event.target.files);

        for(let file of files)
        this.fileSaved.push(file);
        this.Names = this.fileSaved.map(f => f.name).join(', ');
      }

    //fungsi post email 
    postEmail(subjectModel, sendTimeModel, emailAttachmentsModel, emailReceiversModel, emailAddressModel,
        agentTypeModel, sendtos) {

        this.bodyModel = this.editor.getData();

        //validasi kalo subjek/body. Tanggal kosong=send now, Tanggal
        if (this.subjectModel == '' || this.subjectModel == null) {
            this.showToast('Please fill subject email');
            return;
        }
        if (this.bodyModel == '' || this.bodyModel == null || this.bodyModel == '<p>&nbsp;</p>') {
            this.showToast("Please fill body message");
            return;
        }

        if (this.bodyModel != null && this.bodyModel.length > 2000) {
            this.showToast("The length of email body exceeds the max characters allowed (2000 chars)");
            return;
        }

        if (this.actionOption == "now") {
            this.sendTimeModel = this.todayDate;
        }

        if (this.actionOption == "now" && this.sendTimeModel != null) {
            this.todayDate = new Date(sendTimeModel);
            this.sendTimeLater = this.todayDate.setHours(this.todayDate.getHours()-7);
            this.sendTimeModel = this.sendTimeLater;
        }

        if (this.actionOption == "later" && this.sendTimeModel != null) {
            this.tempSendTime = new Date(sendTimeModel);
            this.sendTimeModel = this.tempSendTime.setHours(this.tempSendTime.getHours()-7);
        }

        else if (this.actionOption == "later" && this.sendTimeModel == null) {
            this.showToast('Please Fill Date');
            return;
        }

        // ->>>>>>>>>>>>>> END VALIDASI <<<<<<<<<<<<<<<<-


        let alert = this.alertCtrl.create({
            title: 'Send Email',
            message: '"Are you sure you want to send this Email?"',
            buttons: [
              {
                text: 'No',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel');
                }
              },{
                text: 'Yes',
                handler: () => {
                    //variabel buat upload file
                    // let fileBrowser = this.fileInput.nativeElement;
                    let fileBrowser = this.fileSaved;
                    const formData = new FormData();
                    if (fileBrowser && fileBrowser[0]) {
                        for(let i =0; i < fileBrowser.length; i++){
                            formData.append("attachments", fileBrowser[i]);
                        }
                        // formData.append("attachments", fileBrowser.files[0]);
                    };

                    if ( this.fileAgentCodes ) {
                        let fileAgentCodes = this.fileAgentCodes.nativeElement;
                        const formDataAgent = new FormData();
                        if (fileAgentCodes.files && fileAgentCodes.files[0]) {
                            formData.append("agentcodes", fileAgentCodes.files[0]);
                        };
                    }
                    
                    //objek email
                    let _body = ({
                        subject: this.subjectModel,
                        body: this.bodyModel,
                        sendTime: this.sendTimeModel,
                        username: this.username,
                        filter: {
                            agent: this.agent,
                            agentTypes: this.selectedAgentTypes,
                            officeCodes: this.officeCodes
                        }
                    });
                    formData.append('email', JSON.stringify(_body));

                    this.showLoading();
                    this.storage.get('Authorization').then((Authorization) => {
                        this.emailProvider.sendEmail(formData, Authorization)
                            .then((email: [any]) => {
                                this.showToast("Create Email Success");
                                this.loading.dismiss();
                                this.navCtrl.popTo('EmailListPage');
                            }, (error) => {
                                this.loading.dismiss();
                                this.showToast(error.message);
                            }
                            );
                    })
                }
            }]
        })
        alert.present();
    }

    cancel() {
        this.navCtrl.popTo('EmailListPage');
    }

    resetFile() {
        this.fileSaved = [];
        this.Names = ""; 
    }

    showToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 15000,
            position: 'top',
            showCloseButton: true,
            cssClass: 'toast'
        });
        toast.present();
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        this.loading.present();
      }


    currentCharacterCount() {
        this.bodyModel = this.editor.getData();
        let len = this.bodyModel.length;
        return len;
    }

    remainingAvailableCharacterCount() {
        this.bodyModel = this.editor.getData();
        let len = this.bodyModel.length;

        if (len <= 2000) {
            this.charactersLeft = this.totalAllowedCharacterCount - len;
        } 

        if (this.bodyModel == '<p>&nbsp;</p>') {
            this.charactersLeft = 2000;
        }
    }
}
