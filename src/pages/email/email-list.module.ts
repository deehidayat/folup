import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CKEditorModule } from 'ng2-ckeditor';
import { EmailListPage } from './email-list';
import { ComponentsModule } from '../../components/components.module';
import { SearchModalPageModule } from '../search-modal/search-modal.module';
import { SearchModalPage } from '../search-modal/search-modal';


@NgModule({
  declarations: [
    EmailListPage
  ],
  imports: [
    IonicPageModule.forChild(EmailListPage),    
    CKEditorModule,
    ComponentsModule,
    SearchModalPageModule
  ],
  entryComponents: [
    EmailListPage,
    SearchModalPage
  ]
})
export class EmailListPageModule {}
