import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { MainMenuProvider } from './../../providers/providers'; 
import { UserProvider } from './../../providers/providers'; 

@IonicPage()

@Component({
  selector: 'page-workbasket',
  templateUrl: 'workbasket.html'
})
export class WorkbasketPage {
  tab1: any;
  tab2: any;
  tab3: any;
  completedCount : number = 3;

  constructor(public navCtrl: NavController, public navParams: NavParams, public mainMenu: MainMenuProvider, private auth: UserProvider) {
    this.tab2 = 'TaskDueTodayPage';
  }

  ionViewCanEnter(): boolean{
    if(this.auth.loggedIn()){
      return true
    }else{
      this.mainMenu.activeChildPage = '';
      this.navCtrl.setRoot('LoginPage');
    }
  }

}
