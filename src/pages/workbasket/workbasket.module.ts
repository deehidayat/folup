import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkbasketPage } from './workbasket';

@NgModule({
  declarations: [
    WorkbasketPage
  ],
  imports: [
    IonicPageModule.forChild(WorkbasketPage),
  ],
  entryComponents: [
    WorkbasketPage,
  ],
  providers: [
  ]

})
export class WorkbasketPageModule { }
