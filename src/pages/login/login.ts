import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, IonicPage } from 'ionic-angular';
import { UserProvider } from '../../providers/providers';
import { MainMenuProvider } from '../../providers/providers';
import { Storage } from '@ionic/storage';

@IonicPage()

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  loading: Loading;
  registerCredentials = { username: '', password: '', channel: 'user' };
  rioleDistributionList: any[] = [];
  dummyLogin: boolean = false;
  constructor(private nav: NavController, private auth: UserProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public mainMenu: MainMenuProvider, private storage: Storage) { 
    this.mainMenu.activePage = 'Login';
    this.mainMenu.activeChildPage = '';
  }

  public createAccount() {
    this.nav.push('RegisterPage');
  }
 
  public login() {
    this.showLoading()
    this.auth.login(this.registerCredentials).subscribe(allowed => {
      if (allowed) {  
        this.storage.set('Authorization', "Bearer-"+allowed.access_token);   
        this.storage.set('username', allowed.username);
        this.storage.set('password', this.registerCredentials.password);
        this.mainMenu.username = allowed.username;
        this.auth.encrypt(allowed.username, this.registerCredentials.password, "Bearer-"+allowed.access_token)
          .subscribe(encrypted => {   
            this.storage.set('encrypted', encrypted);
            this.storage.set('isLoggedin', true);
            this.storage.set('createdTime', Date.now());
            this.auth._isLoggedin = true;
            this.auth.currentUser = {name: allowed.username, email:''};
            this.mainMenu.mainMenuInit();
            this.loadMenu("Bearer-"+allowed.access_token);
          },
          error => {
            this.showError(error);
          });

        this.auth.listRoleDistributionUserByUsername(allowed.username, "Bearer-"+allowed.access_token) 
          .subscribe(result => { 
            this.auth.roleDistributionList = result; 
            this.storage.set('roleDistributionList', result);
          }, 
          error => { 
            this.showError(error); 
          }); 
          
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }
 
  loadMenu(Authorization){
    if (this.dummyLogin == true) {
    }else{
      this.auth.role ='All';
      this.auth.getMenu(Authorization)
        .subscribe((result) => {
            for(let key in result.content){
              var item = {};
              item = result.content[key];
              this.mainMenu.mainNav[this.mainMenu.userActionIndex].children.push(item);
            }

            this.nav.setRoot('WelcomePage');
        }, (error) => {
          console.log('error: ', error);
        });
  }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Login Failed',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}