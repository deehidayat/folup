import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchTaskClaimPage } from './search-task-claim';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SearchTaskClaimPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchTaskClaimPage),
    ReactiveFormsModule
  ],
})
export class SearchTaskClaimPageModule {}
