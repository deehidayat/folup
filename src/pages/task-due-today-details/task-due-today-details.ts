import { Component, Pipe, PipeTransform, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Platform, NavController, NavParams, ToastController, LoadingController, Loading, AlertController, IonicPage, MenuController } from 'ionic-angular';
import { TaskDetails } from './task_details';
import { CurrencyPipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import { TaskProvider, UserProvider, MasterDataProvider, MainMenuProvider } from './../../providers/providers';
import { SplitDatePipe } from './../../pipes/split-date/split-date';
import { NewLifeAss, NewBenef } from './new-client';
import { TransferToRoleDecisionDesc } from './transfer-to-role-decision-desc.data';
import { Role } from './master.data';

@IonicPage()

@Component({
  selector: 'page-task-due-today-details',
  templateUrl: 'task-due-today-details.html',
  providers: [SplitDatePipe]
})

export class TaskDueTodayDetailsPage implements AfterViewInit{
  public newLifeAss : any = NewLifeAss;
  public newBenef : any = NewBenef;
  public transferToRoleDecisionDesc : any = TransferToRoleDecisionDesc;
  public selectedTask: any = {};
  public jsonSPAJ: any = {};
  public remarks: any[] = [];
  public caseHistory: any[] = [];
  public processCasesheet: any[] = [];
  public decisions: any[] = [];
  public decisionsTransferRoleName: any[] = [];
  public choosedDecision: any = {};
  public takenDecision: any = {};
  public followUpCodes: any[] = [];
  public followUpActionList: any[] = [];
  public newFollowUpActionList: any[] = [];
  public evidenceCode: any[] = [];
  public roleData: any = Role;
  items: TaskDetails[];
  tabs: Array<{id: number, active: number}>;
  searchQuery: string = '';
  itemsSearch: any;
  itemsSelected: string[] = [];
  caseHistoryItemCount: number = 3;
  tempCaseHistory: any[] = [];
  processCasesheetItemCount: number = 3;
  tempProcessCasesheet: any[] = [];
  topUpAmount: any;
  topUpFund: any[] = [];
  dataJSON: any;
  bpmRemark: string = "";
  srch_by: string = "1";
  masterData: any = {};
  @ViewChild('imageList') imageList: ElementRef;
  @ViewChild('imageView') imageView: ElementRef;
  @ViewChild('dataReview') dataReview: ElementRef;
  @ViewChild('decisionTRN') decisionTRN: ElementRef;
  loading: Loading;
  documents: any = {};
  documentView: any[] = [];
  dateEvicodes: any[] =[];
  totalTopUpFund: number = 0;
  totalProposalFund: number = 0
  sumReviewIncome: any = {};
  suspendTimeDecision: any = {suspendTime: 0, item: ['Pending Call', 'Temp Suspend', '1 Day Suspend']};
  lifeAss: any = {};
  uwQuestionnaire: any = {};
  policyNumber: string = "";
  rawJsonRequest: any;
  selectedRider:any[] = [];
  selectedSubstandard: any[] = [];
  selectedFund: any[] = [];
  docIdValidation: any[] = [];
  docItemType: any[] = ["POLDOCTEMP", "LOOSEMAILS"];
  dataReviewInputDisabled: boolean = false;
  dataReviewButtonDisabled: boolean = false;
  listUsers: any[] = [];
  decLastActionUser: string = "";
  transferRoleNameOption: any[] = [];
  selectedTRNOption: string = "";
  selectedMenu: number = 0;
  claimTask: any;
  sectionObject: any[] = [{active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}, {active: false}];
    ;
  spajNumber: number = 0;
  cmFlag: boolean = false;
  docIdValidationInvalid: any[] = [];
  BOMappingIndex: any = {};
  productType: string = "";

  constructor(private splitDatePipe: SplitDatePipe, public navCtrl: NavController, public navParams: NavParams, private _taskProvider: TaskProvider, private cp: CurrencyPipe, public storage: Storage, public toastCtrl: ToastController, public menuActive: MainMenuProvider, private _masterDataProvider: MasterDataProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController, private auth: UserProvider, public menuCtrl: MenuController) {
    let task = navParams.data;
    this.selectedTask = task;
    // Tabs Array
    this.tabs = [
      {id:0, active: 0},
      {id:1, active: 0},
      {id:2, active: 0},
      {id:3, active: 0},
      {id:4, active: 0},
      {id:5, active: 0},
      {id:6, active: 0}
    ]
    this.sumReviewIncome.lifeAss = [];
    menuActive.activeChildPage = 'Data SPAJ';
    menuActive.activePage = 'User Action';
    this.lifeAss.uwLifeAss = false;
    this.lifeAss.lists = [];
    if (task.taskId) {
      this.loadClaimTask();
      this.masterData.role = this.roleData;
    }else{
      this.navCtrl.setRoot('TaskDueTodayPage');
    }
  }

  loadDetailsMenu(){
    for (var i = this.sectionObject.length - 1; i >= 0; i--) {
      this.sectionObject[i].active = false;
    }
  }

  loadClaimTask(){
    this.storage.get('Authorization').then((Authorization) => {
      this.storage.get('encrypted').then((_encrypted) => {
        _encrypted.taskId = this.navParams.data.taskId;

        //Get Claim task
        this._taskProvider.getClaimTask(_encrypted, Authorization)
          .subscribe((result:any) => {
            this.jsonSPAJ = result.jsonRequest ? JSON.parse(result.jsonRequest):{};
            this.dataJSON = JSON.stringify(this.jsonSPAJ, null, "   ");
            this.rawJsonRequest = result.jsonRequest ? result.jsonRequest : "";
            this.policyNumber = this.selectedTask.policyNumber;
            this.storage.set('claimTask', result);
            this.storage.set('jsonSPAJ', result.jsonRequest);
            this.countProposalFund();
            this.claimTask = result;

            if (result.result[2].data.spajNumber) {
              this.spajNumber = result.result[2].data.spajNumber;
              this._taskProvider.getCMInfoAndValidationByPropNo(result.result[2].data.spajNumber, this.docItemType, Authorization)
                .subscribe((result) => {
                  this.cmFlag = result.result;
                  this.docIdValidation = result.data;
                }, (error) => {
                  console.log(error);
                });
            }

            if (this.jsonSPAJ.sqs.data.product_code) {
              let body = { "product_code": this.jsonSPAJ.sqs.data.product_code }
              this._taskProvider.getProductChannel(body, Authorization)
                .subscribe((result) => {
                  if (result.product_type) {
                    this.productType = result.product_type;
                    if (this.jsonSPAJ.flag) {
                      for (var i = 0; i < this.jsonSPAJ.flag.length; i++) {
                        if (this.jsonSPAJ.flag[i].bo_mapping == 'prod_syariah') {
                          this.jsonSPAJ.flag[i].value = result.product_type;
                        }
                        this.BOMappingIndex[this.jsonSPAJ.flag[i].bo_mapping] = i
                      }
                      this.storage.set('jsonSPAJ', JSON.stringify(this.jsonSPAJ));
                    }
                  }
                }, (error) => {
                  console.log(error);
                });
            }
          }, (error) => {
            console.log('error', error);
          },()=>{
            this.menuActive.selectedMenu = 11;
          })
      });
    });
  }

  ionViewDidLoad(){
    this.loadDetailsMenu();
  }

  ionViewWillEnter(){
    if(this.auth.loggedIn()){
      return true
    }else{
      this.auth.logout();
      this.navCtrl.setRoot('LoginPage');
    }
  }

  viewDocument(docId, e){
    let document = this.documents.lists.filter(function( obj ) {
          return obj.docId == docId;
        });
    if (this.documentView.length <= 1 && document.length >= 1) {
      this.documentView.push(document[0]);
    }
  }

  removeFromDocumentView(e, index){
    this.documentView.splice(index, 1);
  }

  removeTopUpFund(i){
    this.topUpFund.splice(i, 1);
    this.countTotalTopUpFund();
  }

  addTopUpFund(e){
    if (this.topUpFund.length<=5){
      this.topUpFund.push({type: "PREF", desc_fund: "Rupiah Equity Fund", percent: "0"});
    }
    this.countTotalTopUpFund();
  }

  countTotalTopUpFund(){
    this.totalTopUpFund = 0;
    for (var i = this.topUpFund.length - 1; i >= 0; i--) {
      this.totalTopUpFund = this.totalTopUpFund + parseInt(this.topUpFund[i].percent ? this.topUpFund[i].percent : 0);
    }
  }

  addLifeAss(e){
    if (this.jsonSPAJ.client && this.lifeAss.lists.length <= 3) {
      if (this.jsonSPAJ.client.lifeAss.length <= 4) {
        this.newLifeAss.role = 9;
        this.lifeAss.lists.push(this.newLifeAss)
        this.jsonSPAJ.client.lifeAss.push(this.newLifeAss);
        this.sortLifeAss();
      }else{
        this.showAlert('you have add the specified amount (5)');
      }
    }else{
      this.showAlert('you have add the specified amount (5)');
    }
  }

  addBenef(e){
    if (this.jsonSPAJ.client) {
      if (this.jsonSPAJ.client.benef.length <= 3) {
        this.jsonSPAJ.client.benef.push(this.newBenef);
      }else{
        this.showAlert('you have add the specified amount(4)');
      }
    }
  }

  removeLifeAss(e, index) {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Are you sure?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.jsonSPAJ.client.lifeAss.splice(index, 1);
            this.lifeAss.lists.splice(index-1, 1);
            this.sortLifeAss();
          }
        }
      ]
    });

    if (this.jsonSPAJ.client){
      if (index >= 0) {
        alert.present(prompt);
      }
    }
  }

  removeBenef(e, index) {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Are you sure?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.jsonSPAJ.client.benef.splice(index, 1);
          }
        }
      ]
    });

    if (this.jsonSPAJ.client){
      if (index >= 0) {
        alert.present(prompt);
      }
    }
  }

  saveTopUpFund(e){
    if (this.totalTopUpFund == 100) {
      if (this.jsonSPAJ.policy.topUp[0].fund) {
        let oft = JSON.stringify(this.topUpFund);
        this.jsonSPAJ.policy.topUp[0].fund = JSON.parse(oft)
        for (var i = 0; i < this.jsonSPAJ.policy.topUp[0].fund.length; i++) {
          this.jsonSPAJ.policy.topUp[0].fund[i].percent = this.jsonSPAJ.policy.topUp[0].fund[i].percent+'00';
        }
      }
      this.dataJSON = JSON.stringify(this.jsonSPAJ, null, "   ");
      this.storage.set('jsonSPAJ', JSON.stringify(this.jsonSPAJ));
      this.showToast('TOP UP was saved successfully');
    }else{
      this.showAlert('Save TOP UP failed, Total TOP UP fund must be 100%');
    }
  }

  addFollowUpCode(e, item){
    if (e.target.checked) {
      let index = this.followUpCodes.indexOf(item);
      this.followUpActionList = this.followUpActionList.concat(item);
      if(index > -1){
        this.followUpCodes.splice(index, 1);
      }
    }else{
      let index = this.followUpActionList.indexOf(item);
      if(index > -1){
        this.followUpCodes.push(item);
        this.followUpActionList.splice(index, 1);
      }
    }
    // this.setParentHeight(e);
  }

  createFollowUp(e){
    this.followUpCodes.push({
      isEdited: true,
      folupCode: '-',
      remark: '-',
      status: '-',
      folupNo: '-',
      clientCode: '-',
      reasonCode: '-'
    })
    // let length = this.followUpCodes.length + this.followUpActionList.length;
    // this.newFollowUpActionList.push(
    //       {
    //         "no": length+1,
    //         "code": "00",
    //         "desc": "Lorem Ipsum",
    //         "remark": "Lorem Ipsum",
    //         "excl_clause": "Lorem Ipsum",
    //         "stat": "Lorem Ipsum",
    //         "clnt_role": "",
    //         "added": true
    //       });
  }

  newFollowUpExists() {
    return !!this.followUpCodes.find(r => r.isEdited === true);
  }

  submitFollowUp() {
    this.showLoading();
    this.storage.get('Authorization').then((Authorization) => {
      let data = this.followUpCodes.filter(r => r.isEdited === true).map(r => {
        return {
          instance_id : r.instanceId ? r.instanceId : this.selectedTask.instanceId,
          prop_no : r.propNo ? r.propNo : this.spajNumber,
          policy_no : r.policyNo ? r.policyNo : this.policyNumber,
          activity : r.activity ? r.activity : this.selectedTask.activityName,
          role : "01",
          folup_code : r.folupCode && r.folupCode != '-' ? r.folupCode : "",
          remark: r.remark && r.remark != '-' ? r.remark : "",
          status  : r.status && r.status != '-' ? r.status : "",
          folup_no : r.folupNo && r.folupNo != '-' ? r.folupNo : "",
        };
      });
      this._taskProvider.createUpdateBrmsLogFolup({
        folup_code: data
      }, Authorization)
        .subscribe((result) => {
          this.loading.dismiss();
          this.loadFollowUp()
        }, (error) => {
          console.log('error: ', error);
        });
    });
  }

  onFolupInput(row, field, value) {
    row.isEdited = true;
    row[field] = value;
  }

  removeNewFollowUp(e, item){
    let index = this.newFollowUpActionList.indexOf(item);
    if(index > -1){
      this.newFollowUpActionList.splice(index, 1);
    }
  }

  saveDataPolicy(e){
    let index = this.jsonSPAJ.client.lifeAss.findIndex(x => x.role=="OW");
    if (index >= 0) {
      this.jsonSPAJ.client.lifeAss[index] = this.jsonSPAJ.client.ph
    }

    this.dataJSON = JSON.stringify(this.jsonSPAJ, null, "   ");
    this.storage.set('jsonSPAJ', JSON.stringify(this.jsonSPAJ));
    this.showToast('Data Review was saved successfully');
  }

  updateCurrency(e, model, model_child){
    if(!e.target){
      model[model_child] = 0;
      let getNumber = e.replace(/\D/g, "");
      setTimeout(()=>{
        model[model_child] = getNumber;
      },1);
    }
  }

  updateCurrencyWithAdd2Zero(e, model, model_child){
    if(!e.target){
      model[model_child] = 0;
      let getNumber = e.replace(/\D/g, "");
      setTimeout(()=>{
        model[model_child] = parseInt(getNumber+"00");
      },1);
    }
  }

  updateCurrencyTopUP(e){
    if(!e.target){
      this.jsonSPAJ.policy.topUp[0].amount = 0;
      let getNumber = e.replace(/\D/g, "");
      setTimeout(()=>{
        this.jsonSPAJ.policy.topUp[0].amount = parseInt(getNumber+"00");
        this.topUpAmount = getNumber;
      },1);
    }
  }

  dateToString(e, model, model_child){
    if(!e.target){
      if (e.split("-").length == 3) {
        let stringDate = e.replace(/-/g,"");
        model[model_child] = stringDate;
        console.log(this.jsonSPAJ.client.ph.dob);
      }
    }

  }

  clearChecklistDecision(e){
    if(this.itemsSearch){
      if(this.itemsSearch.length > 0){
        for (let i = 0; i < this.itemsSearch.length; i++) {
          this.itemsSearch[i].added = false;
        }
      }
    }

    if (this.followUpActionList.length >= 1) {
      for (let i = 0; i < this.followUpActionList.length; i++) {
        this.followUpCodes.push(this.followUpActionList[i]);
      }
    }

    this.followUpActionList = [];
    this.newFollowUpActionList = [];
    this.showToast('Follow Up action list cleared.');
  }

  saveFollowUp(e){
    let temFolupCode = [];
    temFolupCode = this.followUpCodes;
    if (this.followUpActionList.length >= 1 || this.newFollowUpActionList.length >= 1) {
      for (let i = 0; i < this.followUpActionList.length; i++) {
        this.followUpActionList[i].added = false;
        temFolupCode.push(this.followUpActionList[i]);
      }

      for (let i = 0; i < this.newFollowUpActionList.length; i++) {
        temFolupCode.push(this.newFollowUpActionList[i]);
      }

      for (let i = 0; i < temFolupCode.length; i++) {
        delete temFolupCode[i].added;
      }
      temFolupCode.sort(function(a, b) {
          return parseInt(a.no) - parseInt(b.no);
      });
      this.followUpActionList = [];
      this.newFollowUpActionList = [];
      this.storage.get('jsonSPAJ').then((val) => {
        let a = JSON.parse(val);
        a.policy.folup_code = temFolupCode;
        this.dataJSON = JSON.stringify(a, null, "   ");
        this.storage.set('jsonSPAJ', JSON.stringify(a));
        this.showToast('Follow Up was saved successfully');
      });
    }
  }

  // Show Toast Function
  showToast(text){
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  checkDec: boolean = false;

  chooseDecision(e, dec){
    let r = this.selectedTRNOption;
    let el = this.decisionTRN.nativeElement;
    this.transferRoleNameOption = [];
    if (dec) {
      this.choosedDecision = dec;
      if ( dec.decision_desc == '1 Day Suspend') {
        this.suspendTimeDecision.suspendTime = 24;
      }else{
        this.suspendTimeDecision.suspendTime = 0;
      }
      this.takenDecision = {};
      this.storage.get('Authorization').then((Authorization) => {
        this._taskProvider.getDecisionTransferRole(dec.transfer_role_name, Authorization)
          .subscribe((dtrn) => {
            if(dtrn.length != undefined){
              if (dec.transfer_role_name == "UW") {
                for (var i = dtrn.length - 1; i >= 0; i--) {
                  let trRN = dtrn[i].role_distribution.substr(0, 7);
                  let trnTemp = this.transferRoleNameOption.filter((itr)=>{return itr == trRN});
                  if (trnTemp.length <= 0) {
                    this.transferRoleNameOption.push(trRN);
                  }
                }

                let filtered_prod_syariah = [];

                if (this.productType == 'S') {
                  // If Product is syariah
                  filtered_prod_syariah = this.transferRoleNameOption.filter((obj)=>{
                      let length = obj.search("S");
                    return length >= 0;
                  });
                }else{
                  // If Product is NOT syariah
                  filtered_prod_syariah = this.transferRoleNameOption.filter((obj)=>{
                    let length = obj.search("K");
                    return length >= 0;
                  });
                }
                this.transferRoleNameOption = filtered_prod_syariah;

                if (r) {
                  let temp = dtrn.filter(function( obj ) {
                    return obj.role_distribution.substr(0, 7) == r;
                  });
                  if (dec.decision_desc == 'Transfer to UW Non Reguler') {
                    temp = temp.filter(function( obj ) {
                      let length = obj.role_distribution.length-2;
                      return obj.role_distribution.substr(length, 2) == '_2';
                    });
                  }else if(dec.decision_desc == 'Transfer to UW Regular') {
                    temp = temp.filter(function( obj ) {
                      let length = obj.role_distribution.length-2;
                      return obj.role_distribution.substr(length, 2) != '_2';
                    });
                  }
                  this.decisionsTransferRoleName = temp;
                  let rd_char_end = this.selectedTask.roleDistribution.substr(8, this.selectedTask.roleDistribution.length-8);
                  let auto_select_index = temp.findIndex(function( obj ) {
                    let length = rd_char_end.length;
                    return obj.role_distribution.substr(obj.role_distribution.length - length , length) == rd_char_end;
                  });
                  setTimeout(()=>{
                    let radiobtn = el.querySelector("#dtrn_"+auto_select_index);
                    if (radiobtn) {
                      radiobtn.checked = true;
                      this.takenDecision = temp[auto_select_index];
                    }
                  },1000);
                }else{
                  this.decisionsTransferRoleName = [];
                }
              }else{
                this.decisionsTransferRoleName = dtrn;
                setTimeout(()=>{
                  for (var i = this.decisionsTransferRoleName.length - 1; i >= 0; i--) {
                    let radiobtn = el.querySelector("#dtrn_"+i);
                    if (radiobtn) {
                      if (radiobtn.checked) {
                        radiobtn.checked = false;
                      }
                    }
                  }
                },1000);
              }
            }else{
              this.decisionsTransferRoleName = [];
            }
          }, (error) => {
            console.log('error: ', error);
          });
      });
    }
  }

  takeDecision(e, item){
    if (e.target.checked) {
      this.takenDecision = item;
    }
  }

  saveDataJSONSPAJ(e){
    let o = JSON.parse(this.dataJSON);
    this.jsonSPAJ = o;
    this.followUpCodes = this.jsonSPAJ.policy.folup_code;
    this.itemsSearch = [];
    this.followUpActionList = [];
    this.storage.set('jsonSPAJ', JSON.stringify(o));
    this.showToast('JSON  was saved successfully');
  }

  saveHeaderCaseReview(){
    if (this.jsonSPAJ.policy) {
      this.jsonSPAJ.policy.prop_no = this.selectedTask.spajNumber;
      this.jsonSPAJ.policy.policy_no = this.selectedTask.policyNumber;
    }
    this.storage.set('jsonSPAJ', JSON.stringify(this.jsonSPAJ));
  }

  clearBpmRemark(e){
    this.bpmRemark = "";
    this.showToast('Note  cleared successfully');
  }

  addBpmRemark(e){
    if(this.bpmRemark.length >= 1){
      let _prop_no = this.jsonSPAJ.policy.prop_no;
      let _policy_no = this.selectedTask.policyNumber;
      let note = this.bpmRemark;
      let _instance_id = this.selectedTask.instanceId;
      let _task_id = this.selectedTask.taskId;
      this.storage.get('Authorization').then((Authorization) => {
        this._taskProvider.createBpmRemark(_prop_no, _policy_no, note, _instance_id, _task_id, Authorization)
          .subscribe((result) => {
            this.showToast('Note  saved successfully');
            this.bpmRemark = "";
          }, (error) => {
            console.log('error: ', error);
          });
      });
        this.processCasesheet = [];
        this.laodProcessCasesheet();
    }
  }

  laodProcessCasesheet(){
    this.storage.get('Authorization').then((Authorization) => {
      let task = this.navParams.data;
      this._taskProvider.getRemarks(task.spajNumber, Authorization)
      .subscribe((result) => {
        this.processCasesheet = this.processCasesheet.concat(result);
        for (let i = 0; i < this.processCasesheet.length; i++) {
          if(this.processCasesheet[i].propNo) {
            this._taskProvider.getCaseHistory(this.processCasesheet[i].propNo, Authorization)
              .subscribe((_caseHistory: any) => {
                let length = _caseHistory.length
                for (var j = _caseHistory.length - 1; j >= 0; j--) {
                  if (this.processCasesheet[i] && _caseHistory[j]) {
                    if (this.processCasesheet[i].taskId && _caseHistory[j].taskId) {
                      if (this.processCasesheet[i].taskId == _caseHistory[j].taskId) {
                        this.processCasesheet[i].username = _caseHistory[j].username;
                        this.processCasesheet[i].decision = _caseHistory[j].decision;
                        this.processCasesheet[i].bpmFlowName = _caseHistory[j].bpmFlowName;
                      }
                    }
                  }
                }
              });
          }
        }
        this.processCasesheet = this.processCasesheet.filter(function( obj ) {
          return obj.remarkType.toLowerCase() == 'user';
        });
        this.processCasesheet.sort(function(a, b) {
          return parseInt(b.logId) - parseInt(a.logId);
        });
        this.tempProcessCasesheet = this.processCasesheet.slice(0, this.processCasesheetItemCount);
      }, (error) => {
        console.log('error: ', error);
      });
    });
  }

  updateFromLifeAsia(from, e){
    this.showLoading()
    if(this.jsonSPAJ.policy){
      this.storage.get('Authorization').then((Authorization) => {
        this.dataReviewButtonDisabled = true;
        let instanceId = this.selectedTask.instanceId ? this.selectedTask.instanceId : "";
        let body = {
          "bpmId":instanceId,
          "prop_no": this.jsonSPAJ.policy.prop_no,
          "policy_no": this.jsonSPAJ.policy.policy_no
        }
        this._taskProvider.getPolicyClientInquiry(body, Authorization)
          .subscribe((jsonRequest) => {
            let _jsonRequest = JSON.stringify(jsonRequest)
            this.jsonSPAJ = jsonRequest ? JSON.parse(_jsonRequest): {};
            this.dataJSON = JSON.stringify(this.jsonSPAJ, null, "   ");

            this.selectedTask.spajNumber = this.jsonSPAJ.policy.prop_no;
            this.selectedTask.policyNumber = this.jsonSPAJ.policy.policy_no;

            this.storage.set('jsonSPAJ', JSON.stringify(this.jsonSPAJ));
            if (from == 'button') {
              this.dataReviewInputDisabled = true;
              this.dataReviewButtonDisabled = false;
              let buttonLA = document.querySelectorAll('.button-la');
              if (buttonLA.length >= 1) {
                buttonLA[0].classList.add("data-loaded");
              }
              let loadedElement = document.querySelectorAll('.loaded');
              for (var i = loadedElement.length - 1; i >= 0; i--) {
                loadedElement[i].classList.remove('loaded');
                let accordionBodies = loadedElement[i].querySelectorAll('.accordion-body');
                for (var j = accordionBodies.length - 1; j >= 0; j--) {
                  accordionBodies[j].removeAttribute('style');
                  let headAcc = accordionBodies[j].previousElementSibling;
                  headAcc.children[0].removeAttribute('style');
                  headAcc.children[1].setAttribute("style", "display:none");
                }
              }
              this.showToast('Update from LifeAsia done');
            }
          }, (error) => {
            this.dataReviewButtonDisabled = false;
            console.log('error: ', error);
            this.showAlert("Error perform this opeartion");
            this.dataReviewInputDisabled = false;
          });
      });
      this.loadFollowUp();
    }else{
      this.showAlert("Can't perform this opeartion, data not is incomplete");
      this.dataReviewInputDisabled = false;
    }
    this.loading.dismiss();
  }

  loadFollowUp(){
    this.storage.get('Authorization').then((Authorization) => {
      let instanceId = this.selectedTask.instanceId ? this.selectedTask.instanceId : "";
      let body = {
        "prop_no":this.jsonSPAJ.policy.prop_no,
        "is_active":"1"
      }

      this._taskProvider.getBrmsLogFolup(body, Authorization)
        .subscribe((result) => {
          this.followUpCodes = result.length ? result : [];
        }, (error) => {
          console.log('error: ', error);
        });
    });
  }

  confirmCompleteTask(e){
    this.dataReviewButtonDisabled = true;
    let confirm = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Tombol get update from life asia belum di klik. Apakah Anda yakin untuk Complete Task?',
      buttons: [
        {
          text: 'Tidak',
          handler: () => {
            this.dataReviewButtonDisabled = false;
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.postCompleteTask(e);
            this.dataReviewButtonDisabled = false;
          }
        }
      ]
    });

    if (this.dataReviewInputDisabled) {
      this.postCompleteTask(e);
      this.dataReviewButtonDisabled = false;
    }else{
      confirm.present(prompt);
    }
  }

  postCompleteTask(e){
    this.dataReviewButtonDisabled = true;
    this.storage.get('claimTask').then((_claimTask) => {
      this.storage.get('jsonSPAJ').then((json_r) => {
        this.storage.get('encrypted').then((_encrypted) => {
          this.storage.get('username').then((_username) => {
            if(_claimTask){
              let allowed = false;
              this.dataReviewButtonDisabled = false;
              if (this.decisionsTransferRoleName.length >=1) {
                if (this.takenDecision.role_distribution) {
                  allowed = true;
                }
              }else{
                allowed = true;
              }
              if (this.choosedDecision.decision_desc && allowed == true) {
                let _completedTask = _claimTask;
                _completedTask.request = _claimTask.result;
                _completedTask.request[1].data = this.choosedDecision.decision_desc;
                _completedTask.request[2].data.sourceCallUser = _claimTask.result[2].data.lastActionUser;
                _completedTask.request[2].data.description = this.choosedDecision.desc_in_workitem ? this.choosedDecision.desc_in_workitem : "";
                _completedTask.request[2].data.sourceCallRoleName = _claimTask.result[2].data.roleName;
                _completedTask.request[2].data.sourceCallRoleDistribution = _claimTask.result[2].data.roleDistribution;
                _completedTask.request[2].data.lastActionUser = "";
                _completedTask.request[2].data.roleDistribution = "";
                _completedTask.request[2].data.policyNumber = this.selectedTask.policyNumber;
                _completedTask.request[2].data.spajNumber = this.selectedTask.spajNumber;
                _completedTask.request[2].data.roleName = "";
                _completedTask.password = _encrypted.password;
                _completedTask.username = _encrypted.username;
                _completedTask.taskId = this.selectedTask.taskId;
                _completedTask.bpmId = this.selectedTask.instanceId ? this.selectedTask.instanceId : "";

                if (this.choosedDecision.decision_desc.toLowerCase() == 'hold' || this.choosedDecision.decision_desc.toLowerCase() == 'unhold' || this.choosedDecision.decision_desc.toLowerCase() == 'pending' || this.choosedDecision.decision_desc.toLowerCase() == 'suspend' || this.choosedDecision.decision_desc.toLowerCase() == 'release') {
                  _completedTask.request[2].data.roleDistribution = _claimTask.result[2].data.roleDistribution;
                }else{
                  _completedTask.request[2].data.roleDistribution = this.takenDecision.role_distribution ? this.takenDecision.role_distribution : "";
                }

                if (this.choosedDecision.decision_desc == 'Refer to Q UW' || this.choosedDecision.decision_desc == 'Refer to Q Tele UW' || this.choosedDecision.decision_desc == 'Refer to Q PMA'){
                  if (this.decLastActionUser != "") {
                    _completedTask.request[2].data.lastActionUser = this.decLastActionUser;
                    _completedTask.request[2].data.roleName = this.choosedDecision.transfer_role_name ? this.choosedDecision.transfer_role_name : "";
                    _completedTask.request[2].data.roleDistribution = this.takenDecision.role_distribution ? this.takenDecision.role_distribution : "";
                  }else{
                    this.showAlert('Please select username.');
                    this.dataReviewButtonDisabled = false;
                    return false;
                  }
                }

                if (this.choosedDecision.decision_desc == 'Pending Call' || this.choosedDecision.decision_desc == 'Temp Suspend' || this.choosedDecision.decision_desc == '1 Day Suspend'){
                  if (this.suspendTimeDecision.suspendTime >= 1) {
                    _completedTask.request[2].data.suspendTime = this.suspendTimeDecision.suspendTime;
                  }
                }

                if (this.choosedDecision.decision_desc == 'Reindex' || this.choosedDecision.decision_desc == 'Reindex CM'){
                  let o = JSON.parse(json_r);
                  o.system.reindex.spaj_number_new = this.jsonSPAJ.system.reindex.spaj_number_new;
                  o.system.reindex.spaj_number_old = _claimTask.result[2].data.spajNumber;

                  for (var i = this.docIdValidation.length - 1; i >= 0; i--) {
                    o.system.reindex.poldoc_id = o.system.reindex.poldoc_id.concat(this.docIdValidation[i].pId)
                  }

                  json_r = JSON.stringify(o);
                }

                let ttrdd = [];

                ttrdd = this.transferToRoleDecisionDesc.filter((item)=>{
                  return this.choosedDecision.decision_desc == item;
                });

                if (ttrdd.length >= 1) {
                  _completedTask.request[2].data.roleName = this.choosedDecision.transfer_role_name ? this.choosedDecision.transfer_role_name : "";
                  _completedTask.request[2].data.roleDistribution = this.takenDecision.role_distribution ? this.takenDecision.role_distribution : "";
                }

                let arrCMIndexDecDesc = [
                  {role_name: 'COORD_UW', decision_desc: 'Already Inforced'},
                  {role_name: 'COORD_UW', decision_desc: 'Borderline Standard'},
                  {role_name: 'COORD_UW', decision_desc: 'Standard'},
                  {role_name: 'COORD_UW', decision_desc: 'Substandard'},
                  {role_name: 'EXCEPTION_UW', decision_desc: 'Already Inforced'},
                  {role_name: 'EXCEPTION_UW', decision_desc: 'Borderline Standard'},
                  {role_name: 'EXCEPTION_UW', decision_desc: 'Standard'},
                  {role_name: 'EXCEPTION_UW', decision_desc: 'Substandard'},
                  {role_name: 'SPV_UW', decision_desc: 'Already Inforced'},
                  {role_name: 'SPV_UW', decision_desc: 'Borderline Standard'},
                  {role_name: 'SPV_UW', decision_desc: 'Standard'},
                  {role_name: 'SPV_UW', decision_desc: 'Substandard'},
                  {role_name: 'TELE_UW', decision_desc: 'Already Inforced'},
                  {role_name: 'TELE_UW', decision_desc: 'Borderline Standard'},
                  {role_name: 'TELE_UW', decision_desc: 'Standard'},
                  {role_name: 'TELE_UW', decision_desc: 'Substandard'},
                  {role_name: 'UW', decision_desc: 'Already Inforced'},
                  {role_name: 'UW', decision_desc: 'Borderline Standard'},
                  {role_name: 'UW', decision_desc: 'Standard'},
                  {role_name: 'UW', decision_desc: 'Substandard'},
                  {role_name: 'UWHD_TEAM', decision_desc: 'Already Inforced'},
                  {role_name: 'UWHD_TEAM', decision_desc: 'Borderline Standard'},
                  {role_name: 'UWHD_TEAM', decision_desc: 'Standard'},
                  {role_name: 'UWHD_TEAM', decision_desc: 'Substandard'},
                  {role_name: 'TELE_PMA', decision_desc: 'Complete'},
                  {role_name: 'NB_PMA', decision_desc: 'Complete'},
                  {role_name: 'NB_PMA_COORD', decision_desc: 'Complete'}
                ]

                let _cmFlag = [];

                _cmFlag = arrCMIndexDecDesc.filter((item) => {
                  if (item.role_name == this.choosedDecision.role_name && item.decision_desc == this.choosedDecision.decision_desc) {
                    return true;
                  }else{
                    return false;
                  }
                });

                let runPostComplete = (json_r, _completedTask )=>{
                  if (json_r == this.rawJsonRequest) {
                    _completedTask.jsonRequest = "";
                  }else{
                    _completedTask.jsonRequest = json_r;
                  }

                  delete _completedTask.result;

                  this.storage.get('Authorization').then((Authorization) => {
                    this._taskProvider.completeTask(JSON.stringify(_completedTask), Authorization)
                    .subscribe((result: any) => {
                      let alert = this.alertCtrl.create({
                          title: 'Task Completed',
                          buttons: [{
                              text: 'OK',
                              handler: () => {
                                this.navCtrl.setRoot('TaskDueTodayPage');
                              }
                            }]
                        });

                        alert.present(prompt);
                    }, (error) => {
                      this.showAlert('Network error, Task is not completed');
                    });
                  });
                }
                if (_cmFlag.length >= 1 ){
                  this.storage.get('Authorization').then((Authorization) => {
                    this._taskProvider.getCMInfoAndValidationByPropNo(this.spajNumber, this.docItemType, Authorization)
                      .subscribe((result) => {
                        this.docIdValidation = result.data;
                        if (result.result == true) {
                          runPostComplete(json_r, _completedTask);
                        }else{
                          this.showAlert('Can\'t complete this task, cm flag is false.');
                        }
                      }, (error) => {
                        console.log(error);
                      });
                  });
                }else{
                  runPostComplete(json_r, _completedTask);
                }
              }else{
                this.dataReviewButtonDisabled = false;
                if (!allowed) {
                  this.showAlert('Task is not completed, you have choose decision level 2 first.');
                }else{
                  this.showAlert('Task is not completed, you have choose decision first.');
                }
              }
            }
          });
        });
      });
    });
  }

  minNum(num){
    if (this.suspendTimeDecision.suspendTime <= (num-1)) {
      setTimeout(()=>{
        if (this.suspendTimeDecision.suspendTime != "") {
          this.suspendTimeDecision.suspendTime = num;
        }
      },1);
    }
  }

  openAccordionBody(e){
    let target = e.currentTarget ? e.currentTarget : e.target;
    let element = target.nextElementSibling;
    let height = element.children[0].offsetHeight;
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      target.children[0].removeAttribute('style');
      target.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height: "+height+"px;");
      target.children[1].removeAttribute('style');
      target.children[0].setAttribute("style", "display:none");
    }
  }

  //accrodion function

  openChildAccordion(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let parentAccHeight = parentAcc.offsetHeight;

    // if (parentAcc != null) {
      if (parentAcc.className != 'accordion-body'){
        let element = e.target;
        while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
          parentAcc = element.parentElement;
        }
        if (parentAcc != null) {
        parentAccHeight = parentAcc.offsetHeight;
      }
    }

    e.stopPropagation();
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      if (parentAcc) {parentAcc.setAttribute("style", "height:"+(parentAccHeight-height)+"px");}
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px;");
      if (parentAcc) {parentAcc.setAttribute("style", "height:"+(height+parentAccHeight)+"px");}
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }
  }

  setParentHeight(e){
    let parent = e.currentTarget ? e.currentTarget : e.target;
    if (parent.className != 'accordion-body'){
      let element = e.target;
      while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
        parent = element.parentElement;
      }
    }
    setTimeout(()=>{
      let contentHeight = parent.children[0].offsetHeight;
      parent.setAttribute("style", "height:"+contentHeight+"px");
    },100);
  }

    setParentHeight2(e){
    let parent = e;
    if (parent.className != 'accordion-body'){
      let element = e;
      while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
        parent = element.parentElement;
      }
    }
    setTimeout(()=>{
      let contentHeight = parent.children[0].offsetHeight;
      parent.setAttribute("style", "height:"+contentHeight+"px");
    },100);
  }
  //end accordion function

  _stopPropagation(e){
    e.stopPropagation();
  }

  deleteRider(e){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete riders ?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            for (var i = this.selectedRider.length - 1; i >= 0; i--) {
              if (this.selectedRider[i]) {
                this.jsonSPAJ.sqs.data.coverage[0].components.splice(i, 1);
                this.selectedRider.splice(i, 1);
              }
            }
          }
        }
      ]
    });
    let index = this.selectedRider.indexOf(true);
    if (index >= 0) {
      alert.present();
    }else{
      this.showAlert('Please select rider to delete');
    }
  }

  addRider(e){
    this.jsonSPAJ.sqs.data.coverage[0].components.push({
      "mort_cls": "",
      "sar": "100000000",
      "desc_component": "PRUsaver",
      "risk_age": "99",
      "unit_prumed": "00",
      "plan_hs": "00",
      "inst_prem_rider": "",
      "component": "U1CR",
      "prod_prefix": "PRU",
      "premium": "100000000",
      "risk_term": "00",
      "prem_age": "99",
      "prem_term": "00",
      "first_premi": "",
      "inst_prem_basic": ""
    });
  }

  addSubstandard(e){
    this.jsonSPAJ.sqs.data.substandard.push({
      role: "",
      sub_class: "",
      sub_code: ""
    });
  }

  deleteSubstandard(e){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete substandard(s)?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            for (var i = this.selectedSubstandard.length - 1; i >= 0; i--) {
              if (this.selectedSubstandard[i]) {
                this.jsonSPAJ.sqs.data.substandard.splice(i, 1);
                this.selectedSubstandard.splice(i, 1);
              }
            }
          }
        }
      ]
    });
    let index = this.selectedSubstandard.indexOf(true);
    if (index >= 0) {
      alert.present();
    }else{
      this.showAlert('Please select substandard(s) to delete');
    }
  }

  addFund(e){
    this.jsonSPAJ.sqs.fund.push({
      code : "",
      desc_fund : "",
      percent : "",
      type : ""
    });
    this.countProposalFund();
  }

  deleteFund(e){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete fund(s)?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            for (var i = this.selectedFund.length - 1; i >= 0; i--) {
              if (this.selectedFund[i]) {
                this.jsonSPAJ.sqs.fund.splice(i, 1);
                this.selectedFund.splice(i, 1);
              }
            }
          }
        }
      ]
    });
    let index = this.selectedFund.indexOf(true);
    this.countProposalFund();
    if (index >= 0) {
      alert.present();
    }else{
      this.showAlert('Please select fund(s) to delete');
    }
  }

  countProposalFund(){
    this.totalProposalFund = 0;
    for (var i = this.jsonSPAJ.sqs.fund.length - 1; i >= 0; i--) {
      this.totalProposalFund = this.totalProposalFund + parseInt(this.jsonSPAJ.sqs.fund[i].percent ? this.jsonSPAJ.sqs.fund[i].percent : 0);
    }
  }

  showMoreCaseHistory(e){
    let counter = 0;
    if((this.tempCaseHistory.length+1) <= this.caseHistory.length ){
      counter = this.tempCaseHistory.length + this.caseHistoryItemCount;
      this.tempCaseHistory = this.caseHistory.slice(0, counter);
    }
  }

  showMoreProcessCaseSheet(e){
    let counter = 0;
    if((this.tempProcessCasesheet.length+1) <= this.processCasesheet.length ){
      counter = this.tempProcessCasesheet.length + this.processCasesheetItemCount;
      this.tempProcessCasesheet = this.processCasesheet.slice(0, counter);
    }
  }

  actTab(tabs, a, e){
    tabs.active = a;
  }

  getItems(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '') {
      if(this.jsonSPAJ.policy) {
        if(this.followUpCodes.length > 0) {
          if (this.srch_by == "1") {
            this.itemsSearch = this.followUpCodes.filter((item) => {
              return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
              })
          }else if(this.srch_by == "0"){
            this.itemsSearch = this.followUpCodes.filter((item) => {
              return (item.stat.toLowerCase().indexOf(val.toLowerCase()) > -1);
              })
          }
        }
      }
    }else{
      this.itemsSearch = []
    }

    if (this.itemsSearch) {
      if(this.itemsSearch.length > 0){
        for (let i = 0; i < this.itemsSearch.length; i++) {
          this.itemsSearch[i].added = false;
        }
      }
    }
  }

  ngAfterViewInit() {

  }

  slideCount: number = 0;

  imageSlide(e){
    let wrapperWidth = this.imageList.nativeElement.children[2].offsetWidth;
    let cont = this.imageList.nativeElement.children[2].children[0];
    let contWidth = cont.offsetWidth;
    let contAttr = cont.getAttribute('style');
    let itemWidth = 160;

    if (e == 'l') {
      this.slideCount = this.slideCount - 1;
      let left = (itemWidth * this.slideCount) - 20;
      if ((contWidth + (this.slideCount*itemWidth)) >= wrapperWidth) {
        cont.setAttribute("style", contAttr+"left: "+left+"px;");
      }else{
        this.slideCount = this.slideCount + 1;
      }
    }else if (e == 'r') {
      this.slideCount = this.slideCount + 1;
      let left = (itemWidth * this.slideCount);
      if (this.slideCount <= 0) {
        cont.setAttribute("style", contAttr+"left: "+left+"px;");
      }else{
        this.slideCount = this.slideCount - 1;
      }
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  callBackFn(e, o) {
   // do anything with "pdf"
   let bodyHeight = this.imageView.nativeElement.parentElement;
   let parentEl = this.imageView.nativeElement.parentElement.parentElement;
   setTimeout(()=>{
      let contentHeight = bodyHeight.offsetHeight;
      if (o == 'open') {
        parentEl.setAttribute("style", "height:"+contentHeight+"px");
      }
    },100);
  }
  onError(error: any) {
    console.log('pdf error', error)
  }

  accordionLoaded(e):boolean{
    let target = e.currentTarget ? e.currentTarget : e.target;
    let element = target.parentElement;
    let loaded = element.classList.contains('loaded');
    return loaded;
  }

  // Open Evidence Code Accordion
  openEvidenceCode() {
    this.masterData.evidenceDescripton = [];
      this.showLoading()
      this.dateEvicodes = [];

      let setDateEvCode = (evCodes)=>{
        this.dateEvicodes.push(evCodes[0].createdDate.substr(0,10));
        for (var i = evCodes.length - 1; i >= 0; i--) {
          let splitDatePipe = new SplitDatePipe();
          if (evCodes[i].modifyDate) {
            var splited = this.splitDatePipe.transform(evCodes[i].modifyDate);
            evCodes[i].modifyDate = splited;
          }
          evCodes[i].createdDate = evCodes[i].createdDate.substr(0,10)
          let index = this.dateEvicodes.indexOf(evCodes[i].createdDate.substr(0,10));
          if(index <= -1){
            this.dateEvicodes.push(evCodes[i].createdDate.substr(0,10));
          }
        }
        this.evidenceCode = evCodes;
      }

      this.storage.get('Authorization').then((Authorization) => {
        this.storage.get('claimTask').then((_claimTask) => {
          if (_claimTask) {
            if (_claimTask.result[2]) {
              this._taskProvider.getEvidenceCode(_claimTask.result[2].data.instanceId, Authorization)
                .subscribe((result) => {
                  if(result){
                    if(result.length != undefined){
                      if (result.length >= 1) {
                        setDateEvCode(result);
                      }
                    }else{
                      this.evidenceCode = [];
                    }
                  }
                }, (error) => {
                  console.log('error: ', error);
                });

                this._masterDataProvider.getEvidenceDescription(Authorization)
                  .subscribe((result) => {
                    this.masterData.evidenceDescripton = result;
                  }, (error) => {
                    console.log(error);
                  });
            }
          }
        });
      });

      this.loading.dismiss();
  }

  checkEvCode(e, item){
    if (item.logId) {
      let dateObj = new Date();
      let month = dateObj.getMonth() >= 9 ? dateObj.getMonth() + 1 :'0'+(dateObj.getMonth()+ 1);
      let day = dateObj.getDate();
      let year = dateObj.getFullYear();
      let hour = dateObj.getHours() >= 10 ? dateObj.getHours() : '0'+dateObj.getHours();
      let minutes = dateObj.getMinutes() >= 10 ? dateObj.getMinutes() : '0'+dateObj.getMinutes();
      let seconds = dateObj.getSeconds() >= 10 ? dateObj.getSeconds() : '0'+dateObj.getSeconds();
      let miliseconds = dateObj.getMilliseconds();
      let newItem = {
        prop_no: "",
        instance_id: "",
        evidence_code: "",
        role: "",
        activity: "",
        client_id: "",
        rule_name: "",
        benefit: "",
        created_by: "",
        modified_by: "",
        status: ""
      };


      item.modifyDate = year +'-'+  month +'-'+  day+' '+hour+':'+minutes+':'+seconds+'.'+miliseconds;
      newItem.prop_no = item.propNo;
      newItem.instance_id = item.instanceId;
      newItem.evidence_code = item.evidenceCode;
      newItem.role = item.role;
      newItem.activity = item.activity;
      newItem.client_id = item.clientId;
      newItem.rule_name = item.ruleName;
      newItem.benefit = item.benefit;
      newItem.created_by = item.createdBy;
      newItem.modified_by = this.auth.currentUser.name;
      newItem.status = "R";

      this.storage.get('Authorization').then((Authorization) => {
        this._taskProvider.createUpdateBrmsLogEvidence(newItem, Authorization)
          .subscribe((result) => {
            this.showToast('Operation complete, status '+item.status+', '+result.resultDescription);
          }, (error) => {
            this.showToast('Operation incomplete, '+ error);
          });
      });
    }else{
      this.showToast('Operation incomplete, no item selected');
    }
  }
  // Open Remarks accordion
  openRemarks(){
      this.showLoading()
      this.storage.get('jsonSPAJ').then((json_r) => {
        let jsonSPAJ = json_r? JSON.parse(json_r):{};
        if(jsonSPAJ.system) {
          if(jsonSPAJ.system.bpmId) {
            this.storage.get('Authorization').then((Authorization) => {
              let task = this.navParams.data;
              this._taskProvider.getRemarks(task.spajNumber, Authorization)
              .subscribe((result) => {
                this.remarks = result;
                this.remarks = this.remarks.filter(function( obj ) {
                  return obj.remarkType.toLowerCase() !== 'user';
                });
              }, (error) => {
                console.log('error: ', error);
              });
            });
          }
        }
      });
      this.docIdValidationInvalid = this.docIdValidation.filter(function( obj ) {
        return obj.validationCM == "false";
      });
      this.loading.dismiss();
  }
  // Add despatch
  addDespatch(role, e){
    this.jsonSPAJ.client.despatch.push({
      address:{
        city: "",
        country: "",
        desc_country: "",
        desc_province: "",
        kecamatan: "",
        kelurahan: "",
        km: "",
        line1: "",
        line2: "",
        line3: "",
        own:{
              home_stat: "",
              desc: "",
            },
        post_code: "",
        province: "",
        rt: "",
        rw: "",
        type:"R"
      },
      number: "",
      role: role
    });
  }
  // End Add Despatch

  // Check Dispacth
  checkDespatch(role){
    if (this.jsonSPAJ.client) {
      let index = this.jsonSPAJ.client.despatch.findIndex(x => x.role == role);
      if (index <= 0) {
        return false;
      }else{
        return true;
      }
    }
  }
  // End Check Despacth

  // Open Data Review Accordion
  openDataReview(){
      let obj = this.jsonSPAJ.policy ? this.jsonSPAJ.policy.uw_questionnaire : [];
      this.uwQuestionnaire.items = Object.keys(obj).map(function(key) {
        return [key, obj[key]];
      });

      this.uwQuestionnaire.roles = [];

      for (var i = this.uwQuestionnaire.items.length - 1; i >= 0; i--) {
        var b = this.uwQuestionnaire.items[i][1];
        var c = this.uwQuestionnaire.items[i][0];
        if (b instanceof Array) {
          for (var j = b.length - 1; j >= 0; j--) {
            let e = () => {
              let obj:any = {};
              obj.items = [];
              obj.roleCode = b[j].role;
              obj.items.push(c);
              this.uwQuestionnaire.roles.push(obj);
            }

            if (this.uwQuestionnaire.roles.length <= 0) {
              e();
            }else{
              for (var d = this.uwQuestionnaire.roles.length - 1; d >= 0; d--) {
                if(this.uwQuestionnaire.roles[d].roleCode == b[j].role){
                  let index = this.uwQuestionnaire.roles[d].items.indexOf(c);
                  if(index <= -1){
                    this.uwQuestionnaire.roles[d].items.push(c)
                  }
                }else{
                  let f = this.uwQuestionnaire.roles.filter(function( obj ) {
                    return obj.roleCode == b[j].role;
                  });

                  if (f.length <= 0) {
                    e()
                  }
                }
              }
            }
          }
        }
      }

      this.storage.get('Authorization').then((Authorization) => {
        // get all master data
        this._masterDataProvider.getMasterSex(Authorization)
          .subscribe((result) => {
            this.masterData.sex = result;
          }, (error) => {
            console.log(error);
          });

        this._masterDataProvider.getMasterMaritalStatus(Authorization)
          .subscribe((result) => {
            this.masterData.maritalStatus = result;
          }, (error) => {
            console.log(error);
          });

        this._masterDataProvider.getMasterReligion(Authorization)
          .subscribe((result) => {
            this.masterData.religion = result;
          }, (error) => {
            console.log(error);
          });

        this._masterDataProvider.getMasterCountry(Authorization)
          .subscribe((result) => {
            this.masterData.country = result;
          }, (error) => {
            console.log(error);
          });

        this.showLoading();
        this._masterDataProvider.getMasterCountryCodes(Authorization)
          .subscribe((result) => {
            this.masterData.countryCodes = result;
            for (var i = this.masterData.countryCodes.length - 1; i >= 0; i--) {
              let arr_name = this.masterData.country.filter(function(item){
                return item.id == result[i].id;
              });
              if (arr_name[0]) {
                this.masterData.countryCodes[i].name = arr_name[0].description+' ('+'+'+this.masterData.countryCodes[i].description+')';
                this.masterData.countryCodes[i].desc_country = '+'+this.masterData.countryCodes[i].description+' ('+arr_name[0].description+')';
              }
            }
          }, (error) => {
            console.log(error);
          });
        this.loading.dismiss();

        this._masterDataProvider.getMasterEducation(Authorization)
          .subscribe((result) => {
            this.masterData.education = result;
          }, (error) => {
            console.log(error);
          });

        this._masterDataProvider.getMasterOccupation(Authorization)
          .subscribe((result) => {
            this.masterData.occupation = result;
          }, (error) => {
            console.log(error);
          });

        this._masterDataProvider.getMasterProvince(Authorization)
          .subscribe((result) => {
            this.masterData.province = result;
          }, (error) => {
            console.log(error);
          });
      });

      //get sum review income
      if (this.jsonSPAJ.client) {
        if (this.jsonSPAJ.client.ph.income.length >= 1) {
          let total = 0;
          for (var i = this.jsonSPAJ.client.ph.income.length - 1; i >= 0; i--) {
            total = total + parseInt(this.jsonSPAJ.client.ph.income[i].amount);
          }
          this.sumReviewIncome.ph = total;
        }

        for (var p = 0; p < this.jsonSPAJ.client.lifeAss.length; p++) {
          if (this.jsonSPAJ.client.lifeAss[p].income.length >= 1) {
            let total = 0;
            for (var i = this.jsonSPAJ.client.lifeAss[p].income.length - 1; i >= 0; i--) {
              total = total + parseInt(this.jsonSPAJ.client.lifeAss[p].income[i].amount);
            }
            this.sumReviewIncome.lifeAss[p] = total;
          }
        }

        if (this.jsonSPAJ.client.despatch) {
          let despatchFilter = this.jsonSPAJ.client.despatch.filter(item => item.role == 'OW');
          if (despatchFilter.length <= 0) {
            this.jsonSPAJ.client.despatch.push({"number":"","role":"OW","address":{"country":"","rt":"","km":"","rw":"","city":"","own":{"home_stat":"","desc":""},"type":"R","kelurahan":"","desc_province":"","desc_country":"","province":"","post_code":"","kecamatan":"","line3":"","line2":"","line1":""}})
          }
        }
      }

      if (this.jsonSPAJ.policy) {
        if (this.jsonSPAJ.policy.payor.income.length >= 1) {
          let total = 0;
          for (var i = this.jsonSPAJ.policy.payor.income.length - 1; i >= 0; i--) {
            total = total + parseInt(this.jsonSPAJ.policy.payor.income[i].amount);
          }
          this.sumReviewIncome.payor = total;
        }
      }

      this.lifeAss.uwLifeAss = this.getOW();
      if (!this.getOW()) {
        this.lifeAss.lists.splice(0, 1);
      }
  }

  selectCountryCodes(id, master_data, model){
    let obj_country_codes = master_data.filter(function(obj){
      return obj.id == id;
    })

    if (obj_country_codes[0]) {
      model.desc_country = obj_country_codes[0].desc_country;
    }

  }

  changeMaritalDescription(e, model,  id){
    let maritalStatus = this.masterData.maritalStatus.filter(item => item.id == e);
    model.desc_marryd = maritalStatus[0].description;
  }

  sortLifeAss(){
    if (this.jsonSPAJ.client) {
      this.jsonSPAJ.client.lifeAss.sort(function(a, b) {
        return a.role - b.role;
      });

      for (var i = 0; i < this.jsonSPAJ.client.lifeAss.length; i++) {
        if (parseInt(this.jsonSPAJ.client.lifeAss[i].role) != NaN) {
          this.jsonSPAJ.client.lifeAss[i].role = '0'+(i+1);
        }
      }
    }
  }

  updateDescription(event, master_data, model, model_desc){
    let selected_item = master_data.filter(function(obj){
      return obj.id == event;
    })

    if (selected_item[0]) {
      model[model_desc] = selected_item[0].description;
    }
  }

  getOW():boolean{
    if (this.jsonSPAJ.client) {
      let index = this.jsonSPAJ.client.lifeAss.findIndex(x => x.role=="OW");

      this.lifeAss.lists = this.jsonSPAJ.client.lifeAss.filter(function( obj ) {
        return obj.role !== "OW";
      });

      if (index >= 0) {
        if (index >= 1) {
          let temp = this.jsonSPAJ.client.lifeAss[0];
          this.jsonSPAJ.client.lifeAss.splice(0, 1, this.jsonSPAJ.client.lifeAss[index]);
          this.jsonSPAJ.client.lifeAss.splice(index, 1, temp);
          this.sortLifeAss();
        }
        return true
      }
      this.sortLifeAss();
    }
    return false;
  }

  // Open Other documents
  openOtherDocuments() {
      this.masterData.productCodes = [];
      this.showLoading();
      this.storage.get('claimTask').then((_claimTask) => {
        if (_claimTask) {
          if (_claimTask.result[2]) {
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.getDocuments(_claimTask.result[2].data.spajNumber, Authorization)
                .subscribe((docs) => {
                  this.documents = docs;
                }, (error) => {
                  console.log(error);
                  this.showAlert('Network error, while load documents');
                }, ()=>{
                  if (this.imageList) {
                    let imageListContainer = this.imageList.nativeElement.children[2].children[0];
                    let docLength = this.documents.lists.length;
                    let width = docLength * 185;
                    imageListContainer.setAttribute("style", "width:"+width+"px;");
                  }
                });

              this._masterDataProvider.getMasterProductCodes(Authorization)
                .subscribe((result) => {
                  this.masterData.productCodes = result;
                }, (error) => {
                  console.log(error);
                });
            });
          }
        }
      });

      this.loading.dismiss();
  }

  saveDocuments(){
    if (this.docIdValidation.length >= 1) {
      this.storage.get('Authorization').then((Authorization) => {
        for (var i = this.docIdValidation.length - 1; i >= 0; i--) {
          this.docIdValidation[i].itemType = "POLDOCTEMP"
          this._taskProvider.updateCmInfoById(this.docIdValidation[i], Authorization)
            .subscribe((result) => {
              this.showToast('Doc ID '+this.docIdValidation[i].docId+', change status '+result.resultCode);
            }, (error) => {
              console.log(error);
            });
        }
      });
    }
  }
  // Open topUp
  openTopUp(){
    this.showLoading();
    this.storage.get('Authorization').then((Authorization) => {
      this._masterDataProvider.getMasterFund(Authorization)
      .subscribe((result) => {
        this.masterData.fund = result;
      }, (error) => {
        console.log(error);
      });
    });

    if (this.jsonSPAJ.policy.topUp) {
      let tof = JSON.stringify(this.jsonSPAJ.policy.topUp[0].fund)
      let toa = JSON.stringify(this.jsonSPAJ.policy.topUp[0].amount)
      this.topUpFund = JSON.parse(tof);
      this.topUpAmount = JSON.parse(toa).slice(0, -2);
      for (var i = 0 ; i < this.topUpFund.length; i++) {
        let str = this.topUpFund[i].percent.toString();
        this.topUpFund[i].percent = str.substring(0, str.length-2);
      }
    }
    this.countTotalTopUpFund();
    this.loading.dismiss();
  }

  // Open Decision
  openDecision(){
      this.showLoading();
      if (this.claimTask) {
        if (this.claimTask.result[2]) {
          this.storage.get('Authorization').then((Authorization) => {
            this._taskProvider.getDecisions(this.claimTask.result[2].data.roleName, this.claimTask.result[2].data.activityName, Authorization)
              .subscribe((result) => {
                if(result.length != undefined){
                  this.decisions = result;
                }
              }, (error) => {
                console.log('error: ', error);
              });

            this._taskProvider.listUsers(Authorization)
              .subscribe((result: any[]) => {
                if (result.length != undefined) {
                  result.sort();
                  this.listUsers = result;
                }
              }, (error) => {
                console.log('error: ', error);
              });
          });
        }
      }
      this.loading.dismiss();
  }

  // Open Case History
  openCaseHistory(){
    this.showLoading();
    let task = this.navParams.data;
    this.storage.get('Authorization').then((Authorization) => {
      this._taskProvider.getCaseHistory(task.spajNumber, Authorization)
      .subscribe((result: any) => {
        this.caseHistory = result.filter(function( obj ) {
            return obj.username !== "SYSTEM";
          }).filter(function( obj ) {
            return obj.username !== null;
          }).filter(function( obj ) {
            return obj.username !== "";
          });;
        if (!this.caseHistory){
          console.log('no case history');
        }else{
          this.tempCaseHistory = this.caseHistory.slice(0, this.caseHistoryItemCount)
        }
      }, (error) => {
        console.log('error', error);
      });
    });
    this.loading.dismiss();
  }

  openDataProposal(){
    this.storage.get('Authorization').then((Authorization) => {
      this._masterDataProvider.getMasterFrequencies(Authorization)
        .subscribe((result) => {
          this.masterData.frequencies = result;
        }, (error) => {
          console.log(error);
        });
    });
  }

  changeSection(){
    if (this.menuActive.selectedMenu == 0 && !this.sectionObject[0].active){
      this.openDataReview();
    }else if(this.menuActive.selectedMenu == 1 && !this.sectionObject[1].active){
      this.openDataProposal();
    }else if(this.menuActive.selectedMenu == 2 && !this.sectionObject[2].active){
      this.openOtherDocuments();
    }else if(this.menuActive.selectedMenu == 3){
      this.openEvidenceCode();
    }else if(this.menuActive.selectedMenu == 4 && !this.sectionObject[4].active){
    }else if(this.menuActive.selectedMenu == 5 && !this.sectionObject[5].active){
      this.openRemarks();
    }else if(this.menuActive.selectedMenu == 6 && !this.sectionObject[6].active){
      this.loadFollowUp();
    }else if(this.menuActive.selectedMenu == 7 && !this.sectionObject[7].active){
      this.openTopUp();
    }else if(this.menuActive.selectedMenu == 8 && !this.sectionObject[8].active){
      this.laodProcessCasesheet();
    }else if(this.menuActive.selectedMenu == 9 && !this.sectionObject[9].active){
      this.openDecision();
    }else if(this.menuActive.selectedMenu == 10 && !this.sectionObject[10].active){
      this.openCaseHistory();
    }else if(this.menuActive.selectedMenu == 11 && !this.sectionObject[11].active){
    }

    if (this.menuActive.selectedMenu != undefined) {
      if (!this.sectionObject[this.menuActive.selectedMenu].active) {
        this.sectionObject[this.menuActive.selectedMenu].active = true;
      }
    }
  }

  changeRiskProfile(event, model){
    let desc = "";
    if (event) {
      if (event == "M") {
        desc = "Moderate"
      }else if (event == "L") {
        desc = "Konservatif"
      }else if (event == "H") {
        desc = "Agresif"
      }
      model.desc_result = desc;
    }
  }
}
