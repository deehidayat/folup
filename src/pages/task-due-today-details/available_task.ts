export class AvailableTask {
  activityName: string;
    spajNumber: string;
    policyNumber: string;
    lastActionUser: string ;
    totalSAR: string;
    createdDate: any;
    agingDays: string;
    componentCode: string;
    scanDate: any;
    sourceOfSubmission: string;
    sourceOfBusiness: string;
    premiumIndicator: string;
    bankChannel: string;
    format: string;
    productCode: string;
    description: string;
    followUpCode: string;
    sourceCallUser: string;
    logId: string;
    instanceId: string;
    taskId: string;
    taskName: string;
    taskDue: any;
    roleDistribution: string;
}