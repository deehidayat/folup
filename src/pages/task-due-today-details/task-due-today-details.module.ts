import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrencyPipe } from '@angular/common';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';

import { TaskDueTodayDetailsPage } from './task-due-today-details';

@NgModule({
  declarations: [
    TaskDueTodayDetailsPage
  ],
  imports: [
    IonicPageModule.forChild(TaskDueTodayDetailsPage),    
    ComponentsModule,
    PdfViewerModule, 
    PipesModule
  ],
  entryComponents: [
    TaskDueTodayDetailsPage,
  ],
  providers: [ CurrencyPipe ]

})
export class TaskDueTodayDetailsPageModule { }
