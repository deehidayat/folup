export const NewLifeAss = {
                "role": "99",
                "type": "",
                "number": "",
                "relation": "",
                "desc_relation": "",
                "salut": "",
                "name": "",
                "pob": "",
                "dob": "",
                "next_age": "",
                "cob": "",
                "desc_cob": "",
                "id": {
                    "type": "",
                    "desc_type": "",
                    "number": "",
                    "flag": "",
                    "exp_date": ""
                },
                "natlty": "",
                "desc_natlty": "",
                "sex": "",
                "desc_sex": "",
                "marryd": "",
                "desc_marryd": "",
                "religion": "",
                "desc_religion": "",
                "edu": "",
                "desc_edu": "",
                "npwp": "",
                "npwp_flag": "",
                "address": [{
                        "type": "",
                        "own": {
                            "home_stat": "",
                            "desc": ""
                        },
                        "line1": "",
                        "line2": "",
                        "line3": "",
                        "rt": "",
                        "rw": "",
                        "km": "",
                        "kelurahan": "",
                        "kecamatan": "",
                        "city": "",
                        "province": "",
                        "desc_province": "",
                        "post_code": "",
                        "country": "",
                        "desc_country": ""
                    }
                ],
                "occp": {
                    "code": "",
                    "desc_occp": "",
                    "id": "",
                    "cat": "",
                    "desc_cat": "",
                    "title": "",
                    "desc_title": "",
                    "comp_name": "",
                    "address": {
                        "line1": "",
                        "line2": "",
                        "line3": "",
                        "rt": "",
                        "rw": "",
                        "km": "",
                        "kelurahan": "",
                        "kecamatan": "",
                        "city": "",
                        "province": "",
                        "desc_province": "",
                        "post_code": "",
                        "country": "",
                        "desc_country": ""
                    },
                    "mort_cls": "",
                    "work_stat": ""
                },
                "xIns": {
                    "flag_xIns": "",
                    "data": [{
                            "name": "",
                            "substandard": "",
                            "ces_date": "",
                            "curr": "",
                            "sar": "",
                            "benefit": [{
                                    "type": "",
                                    "desc_ben": ""
                                }
                            ]
                        }
                    ]
                },
                "contact": {
                    "phone": [{
                            "type": "",
                            "number": "",
                            "country": "",
                            "desc_country": ""
                        }, {
                            "type": "",
                            "number": "",
                            "country": "",
                            "desc_country": ""
                        }
                    ],
                    "fax": "",
                    "email": ""
                },
                "income": [{
                        "type": "",
                        "freq": "",
                        "src_value": "",
                        "desc_src": "",
                        "amount": "",
                        "desc_amount": ""
                    }
                ],
                "flag_role": "",
                "desc_flag_role": "",
                "flag_temp1": "",
                "sum_assured": {
                    "life": "",
                    "cc": ""
                },
                "medical_sar": {
                    "sar_exist": [{
                            "role": "",
                            "data": [{
                                    "tsaType": "",
                                    "total": "",
                                    "detail": [{
                                            "riskClass": "",
                                            "amount": ""
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "sar_current": [{
                            "role": "",
                            "data": [{
                                    "tsaType": "",
                                    "total": "",
                                    "detail": [{
                                            "riskClass": "",
                                            "amount": ""
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            };

export const NewBenef = {
                "type": "",
                "number": "",
                "salut": "",
                "name": "",
                "dob": "",
                "sex": "",
                "desc_sex": "",
                "marryd": "",
                "desc_marryd": "",
                "relation": "",
                "desc_relation": "",
                "percent": "",
                "flag_role": "",
                "desc_flag_role": "",
                "address": [{
                        "type": "",
                        "own": {
                            "home_stat": "",
                            "desc": ""
                        },
                        "line1": "",
                        "line2": "",
                        "line3": "",
                        "rt": "",
                        "rw": "",
                        "km": "",
                        "kelurahan": "",
                        "kecamatan": "",
                        "city": "",
                        "province": "",
                        "desc_province": "",
                        "post_code": "",
                        "country": "",
                        "desc_country": ""
                    }
                ]
            }