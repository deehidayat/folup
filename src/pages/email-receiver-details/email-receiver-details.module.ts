import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailReceiverDetailsPage } from './email-receiver-details';

@NgModule({
  declarations: [
    EmailReceiverDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(EmailReceiverDetailsPage),
  ],
})
export class EmailReceiverDetailsPageModule {}
