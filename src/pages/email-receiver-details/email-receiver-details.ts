import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Storage } from '@ionic/storage';
import { EmailProvider } from '../../providers/email/email';


@IonicPage()
@Component({
  selector: 'page-email-receiver-details',
  templateUrl: 'email-receiver-details.html',
})
export class EmailReceiverDetailsPage {

  id :any;
  email:any;
  emailReceivers: any;
  loading:Loading;
  agentInvolved:any;
  page:0;
  size:10;
  lengthData:0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public emailProvider : EmailProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    this.id = navParams.get('id');
    this.agentInvolved = navParams.get('agentInvolved');
    // this.getEmail(this.id);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmailReceiverDetailsPage');
    this.search();
  }

  search() {
    this.emailReceivers = [];
    this.page = 0;
    this.size = 10;
    this.lengthData = 0;
    this.showLoading();
    this.getEmail();
    this.getData(null);
  }

  getEmail() {
    this.storage.get('Authorization').then((Authorization) => {
        this.emailProvider.getEmailById(this.id, Authorization)
        .subscribe(
        result => {
            this.email = result;
        },
        err => {
          this.showToast(err.message);
        });
    });
  }

  getData(infiniteScroll) {
    this.storage.get('Authorization').then((Authorization) => {
        this.emailProvider.getReceivers(this.id, this.page, this.size, Authorization)
        .subscribe(
        result => {
          
            for (let i = 0; i < result.length; i++) {
                this.emailReceivers.push(result[i]);        
            }
            this.lengthData = result.length;
            this.loading.dismiss();
            if (null != infiniteScroll)
            infiniteScroll.complete();
        },
        err => {
            // this.showToast(err.message);
            this.loading.dismiss();
        });
    });
  }

    //infinite scroll
    doInfinite(infiniteScroll) {
        this.page++;
        this.getData(infiniteScroll);
    }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true,
        cssClass: 'toast'
    });
    toast.present();
}

showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
}
