import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopExclusionDetailPage } from './pop-exclusion-detail';

@NgModule({
  declarations: [
    PopExclusionDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PopExclusionDetailPage),
  ],
})
export class PopExclusionDetailPageModule {}
