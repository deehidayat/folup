import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageTaskClaimPage } from './manage-task-claim';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ManageTaskClaimPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageTaskClaimPage),
    ReactiveFormsModule
  ],
})
export class ManageTaskClaimPageModule {}
