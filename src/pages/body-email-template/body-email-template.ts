import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController,ViewController } from 'ionic-angular';
import { AddTemplatePage } from '../../pages/add-template/add-template';
import { EditTemplatePage } from '../../pages/edit-template/edit-template';

import { InfiniteScroll } from 'ionic-angular/components/infinite-scroll/infinite-scroll';
import { EmailTemplateProvider } from '../../providers/email-template/email-template';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';


@IonicPage()
@Component({
  selector: 'page-body-email-template',
  templateUrl: 'body-email-template.html'
})
export class BodyEmailTemplatePage {

  public id: any;
  public emails: any;
  public body: String = "";
  public subject: String = "";
  public pBody: any;
  public pSubject: any;
  public selected = [];
  public AddTemplatePage: AddTemplatePage;
  public EditTemplatePage: EditTemplatePage;
  public pageNumber = 0;
  public lengthData = 0;
  public size = 10;
  public loading:any;
  public username:string;
  public searchOption:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public emailTemplateProvider: EmailTemplateProvider,  public events: Events,
    private alertCtrl: AlertController, public viewCtrl: ViewController, private storage: Storage,
    public toastCtrl:ToastController, public loadingCtrl:LoadingController) {
      // load data awal buka page

      this.subject = "";
      this.body = "";
      this.search();
      this.searchOption = {subject:false, body:false};
  }

  ionViewDidLoad() {
    this.storage.get('username').then((username) => {
      this.username = username;
    })
    console.log('ionViewDidLoad BodyEmailTemplatePage');
  }

  //fungsi search emails
  search() {
    this.emails = [];
    this.pageNumber = 0;
    this.showLoading();
    this.pSubject = this.subject;
    this.pBody = this.body;
    this.getData(null);
  }

  //fungsi get data untuk ambil data dengan rest dan infinite scroll
  getData(infiniteScroll) {
    this.storage.get('Authorization').then((Authorization) => {
    this.emailTemplateProvider.getFilterEmails(this.pBody, this.pSubject, this.pageNumber, this.size, Authorization).subscribe(
      result => {                 
        for (let i = 0; i < result.length; i++) {
          this.emails.push(result[i]);        
        }
        this.loading.dismiss();
        this.lengthData = result.length;
        if (null != infiniteScroll)
          infiniteScroll.complete();
      },
      err => {
        this.loading.dismiss();
        this.showToast(err.message);
      });
    });
  }

  //infinite scroll
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.pageNumber++;
    this.getData(infiniteScroll);
  }

  //buka page Add Template untuk Add template email
  openAdd() {
    this.navCtrl.push('AddTemplatePage');
  }

  //idcheck untuk ambil objek dengan memilih checkbox
  idCheck(select) {
    for (let i = select; i < this.emails.length; i++) {
      this.selected.push(this.emails[i]);
      // this.agents[i].push(checked);
      console.log(select);
      console.log(this.selected);
      console.log(this.emails[i]);
      return;
    }
  }

  // kirim objek ke page edit dan buka page edit template -passing var
  openEdit(event, id, subject, body) {
    for (let i = event; i < this.emails.length; i++) {
      id = this.emails[i]['id'];
      subject = this.emails[i]['subject'];
      body = this.emails[i]['body'];

      // Sharing data using NavController
      this.navCtrl.push('EditTemplatePage', {
        id: id,
        subject: subject,
        body: body
      });

      console.log(event);
      console.log(this.selected);
      console.log(i);
      return;
    }
  }

  //fungsi delete dengan popup konfirmasi
  delete() {
    let alert = this.alertCtrl.create({
      title: 'Delete Email',
      message: '"Are you sure to delete this data?"',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            //push id dari objek emails ke variabel ids 
            let ids = [];
            for(let i = 0; i < this.emails.length;i++) {
            
            if (this.emails[i].checked)
                ids.push(this.emails[i]['id']);
            }    
          
            this.storage.get('Authorization').then((Authorization) => {
              this.emailTemplateProvider.deleteEmailTemplate(ids, this.username, Authorization)
                .subscribe(data => { 
                  if (ids == null){             
                    return;
                  }
                  this.showToast("Email Template has been deleted")
                  this.search();
                }, err => {
                  this.showToast(err.message);
                  return;
                });
          }
        )}
        }
      ]
    });
    alert.present();
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }
  
  openDetails(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    
    if (element.hasAttribute('style')){
      element.removeAttribute('style');
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px");
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }
  }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true
    });
    toast.present();
  }
  
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

}
