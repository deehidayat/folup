import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BodyEmailTemplatePage } from './body-email-template';
import { ComponentsModule } from './../../components/components.module';

@NgModule({
  declarations: [
    BodyEmailTemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(BodyEmailTemplatePage),
    ComponentsModule
  ],
})
export class BodyEmailTemplatePageModule {}
