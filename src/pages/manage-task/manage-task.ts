import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, Loading, } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MainMenuProvider, UserProvider, TaskProvider } from './../../providers/providers';
import { NewTask } from './new-task';
/**
 * Generated class for the ManageTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manage-task',
  templateUrl: 'manage-task.html',
})
export class ManageTaskPage {
  public newTask : any = NewTask;
  searchOption: any;
  actionOption: string= 'ac_terminate';
  _encrypted: any = {};
  selectedTaskName: string = '1';
  searchTaskResult: any[] = [];
  searchInput: any;
  itemList: any[] = [];
  transferUsername: string = "";
  suspendedType: string = "All";
  loading: Loading;
  listUsers: any[] = [];
  numberOfPages: any[] = [];
  activePage: number = 0;
  searchBody: any = {};
  availableTasks: any[] = [];
  perPage: number = 20;
  loadedPage: any[] = [0];
  counter: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _taskProvider: TaskProvider, private storage: Storage, private mainMenu: MainMenuProvider, private user: UserProvider, public toastCtrl: ToastController, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.storage.get('Authorization').then((Authorization) => {
      this._taskProvider.listUsers(Authorization)
        .subscribe((result: any[]) => {
          if (result.length != undefined) {
            result.sort();
            this.listUsers = result;
          }
        }, (error) => {
          console.log('error: ', error);
        });
    });

    this.mainMenu.activePage = 'User Action';
    this.mainMenu.activeChildPage = 'Manage Task';

    this.searchOption = {role_distribution: false, policy_number: false, username: false, spaj_number:false};
    this.searchInput = {role_distribution: "", policy_number: "", username: "", spaj_number:""};
  }

  ionViewWillEnter(){ 
    if(this.user.loggedIn()){
      return true
    }else{
      this.user.logout();
      this.navCtrl.setRoot('LoginPage');
    }
  } 

  openDetails(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    
    if (element.hasAttribute('style')){
      element.removeAttribute('style');
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px");
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }
  }

  openPage(page){
    let findPage = this.loadedPage.findIndex((item) => {return item == page});
    if (findPage < 0) {
      this.showLoading();
      this.storage.get('Authorization').then((Authorization) => {
        this._taskProvider.getLogBpmTrackByStatus(this.searchBody, Authorization)
          .subscribe((_searchTaskResult: any[]) => {
            if (_searchTaskResult.length != undefined) {
              for (let i = 0; i <= _searchTaskResult.length-1 ; i++) {
                _searchTaskResult[i]._worklistData = _searchTaskResult[i].worklistData ? JSON.parse(_searchTaskResult[i].worklistData):{};
                this.searchTaskResult.splice(((page*this.perPage)+i), 1, _searchTaskResult[i]);
              }
            }
            this.loading.dismiss();
          }, (error) => {
            this.loading.dismiss();
            console.log('error: ', error);
          });
      });
      this.loadedPage.push(page);
    }   
    this.activePage = page;
    this.searchBody.pageNumber = page+1;   
  }

  searchTask(){
    let bpmId = "";
    let prop_no = this.searchOption.spaj_number? this.searchInput.spaj_number : "";
    let policy_no = this.searchOption.policy_number? this.searchInput.policy_number : "";
    let username = this.searchOption.username? this.searchInput.username : "";
    let taskName = "";
    let role = "";
    let role_distribution = this.searchOption.role_distribution? this.searchInput.role_distribution : "";
    this.searchTaskResult = [];
    this.activePage = 0;
    this.searchBody.pageNumber = 1; 
    this.loadedPage = [0];

    let searchOpt = {
      bpm_id : "",
      prop_no : prop_no,
      policy_no : policy_no,
      username: username,
      role : "",
      role_distribution : role_distribution,
      taskName: "",
      bpm_status : ["OPEN", "CLAIMED"],
      pageNumber: '1',
      pageSize: '20',
      counter:''
    }

    this.searchBody = {
      bpm_id : "",
      prop_no : prop_no,
      policy_no : policy_no,
      username: username,
      role : "",
      role_distribution : role_distribution,
      taskName: "",
      bpm_status : ["OPEN", "CLAIMED"],
      pageNumber: '1',
      pageSize: '20'
    }

    if (this.searchInput.spaj_number || this.searchInput.policy_number || this.searchInput.username || this.searchInput.role_distribution) {
      this.showLoading();
      this.storage.get('Authorization').then((Authorization) => {
        this._taskProvider.getLogBpmTrackByStatus(searchOpt, Authorization)
          .subscribe((_searchTaskResult: any[]) => {
            if (_searchTaskResult.length != undefined) {
              searchOpt.counter = "Y";
              delete searchOpt.pageNumber;
              delete searchOpt.pageSize;

              this._taskProvider.getLogBpmTrackByStatus(searchOpt, Authorization)
                .subscribe((result) => {
                    this.counter = result.counter;
                    this.pagesCounter();
                  }, (error) => {
                    console.log('error: ', error);
                  });

              for (let i = 0; i <= _searchTaskResult.length - 1; i++) {
                _searchTaskResult[i]._worklistData = _searchTaskResult[i].worklistData ? JSON.parse(_searchTaskResult[i].worklistData):{};
                this.searchTaskResult.push(_searchTaskResult[i])
              }
            }
              this.loading.dismiss();
            }, (error) => {
              this.loading.dismiss();
              console.log('error: ', error);
            });

        this.storage.get('encrypted').then((val) => {
          this._taskProvider.getAvailableTasks(val, Authorization)
            .subscribe((availableTasks: any[]) => {
              if (availableTasks) {
                if (availableTasks.length != undefined) {
                  let endIndex = this.counter;
                  let filteredAvailableTaks = availableTasks.filter((obj)=>{
                    let bool = false;

                    if (obj.activityName == "Exception Block") {
                      bool = true;
                    }else{
                      return false
                    }

                    if (prop_no) {
                      if (obj.spajNumber == prop_no) {
                        bool = true;
                      }else{
                        return false;
                      }
                    }

                    if (policy_no) {
                      if (obj.policyNumber == policy_no) {
                        bool = true;
                      }else{
                        return false;
                      }
                    }

                    if (username) {
                      if (obj.username == username) {
                        bool = true;
                      }else{
                        return false;
                      }
                    }

                    if (role_distribution) {
                      if (obj.roleDistribution == role_distribution) {
                        bool = true;
                      }else{
                        return false;
                      }
                    }

                    return bool;
                  });
                  for (let i = 0; i <= filteredAvailableTaks.length-1; i++) {
                    val.taskId = filteredAvailableTaks[i].taskId;
                    this._taskProvider.getClaimTask(val, Authorization)
                      .subscribe((resultClaimTask:any) => {
                        if (resultClaimTask.result[2].data) {
                          filteredAvailableTaks[i]._worklistData = resultClaimTask.result[2].data ? resultClaimTask.result[2].data:{};
                          this.searchTaskResult[i+endIndex] = filteredAvailableTaks[i];
                          this.availableTasks.push(filteredAvailableTaks[i]);
                        }
                        this.pagesCounter();
                      }, (error) => {
                        console.log('error', error);
                      }) 
                  }
                }
              } 
            }, (error) => {
              this.showAlert(error); 
            });
        });

      });
    }else{
      this.showAlert('Please input search option first to do this action!');
    }
  }

  pagesCounter(){
    let m = Math.ceil((this.counter+this.availableTasks.length)/this.perPage);
    if (m <= 1) {
      m = 1;
    }
    this.numberOfPages = Array(m).fill(0,0).map((x,i)=>i);
  }
  checkAvailabilityOfEB(){
    let d = []
    for (var i = this.itemList.length - 1; i >= 0; i--) {
      if (this.itemList[i]) {
        d.push(this.searchTaskResult[i]);
      }
    }

    let filterEB = d.filter((obj)=>{return obj._worklistData.activityName != "Exception Block"});

    return filterEB.length;
  }
  takeAction(){
    if (this.itemList.length >= 1) {
      if (this.actionOption == 'ac_transfer_to_pool') {
        for (var i = this.itemList.length - 1; i >= 0; i--) {
          if (this.itemList[i]) {
            let body = {
              bpm_id: this.searchTaskResult[i].bpmId,
              username: ""
            }
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.transferCaseByBpmId(body, Authorization)
                .subscribe((result) => {
                  this.showToast('Bpm id '+body.bpm_id+', transfer '+result.resultDescription);
                }, (error) => {
                  console.log('error: ', error);
                });
            });
          }
        }
      }else if(this.actionOption == 'ac_transfer_to_user' && this.transferUsername != "") {
        for (var i = this.itemList.length - 1; i >= 0; i--) {
          if (this.itemList[i]) {
            let body = {
              bpm_id: this.searchTaskResult[i].bpmId,
              username: this.transferUsername
            }
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.transferCaseByBpmId(body, Authorization)
                .subscribe((result) => {
                  this.showToast('Bpm id '+body.bpm_id+', transfer '+result.resultDescription);
                }, (error) => {
                  console.log('error: ', error);
                });
            });
          }
        }
      }else if(this.actionOption == 'ac_resume_suspend_case') {
        for (var i = this.itemList.length - 1; i >= 0; i--) {
          if (this.itemList[i]) {
            let _suspend_type = "";
            let body = {
              suspend_type : _suspend_type,
              prop_no : this.searchTaskResult[i].propNo,
              policy_no : this.searchTaskResult[i].policyNo
            }
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.getLogBpmSuspend(body, Authorization)
                .subscribe((result) => {
                  let body2 = {
                    log_id : result.logId, 
                    flag_bpm : "BPM_RESUME"
                  }
                  this._taskProvider.updateLogBpmSuspend(body2, Authorization)
                    .subscribe((result2) => {
                      this.showToast('Proposal Number '+body.prop_no+', resume suspend request '+result2.resultDescription);
                    }, (error) => {
                      console.log('error: ', error);
                    });
                }, (error) => {
                  console.log('error: ', error);
                });
            });
          }
        }
      }else if(this.actionOption == 'ac_retrigger') {
        for (var i = this.itemList.length - 1; i >= 0; i--) {
          if (this.itemList[i]) {
            let body = {
              prop_no: this.searchTaskResult[i].propNo
            } 
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.retriggerWF(body, Authorization)
                .subscribe((result) => {
                  this.showToast('Retrigger Proposal number '+body.prop_no+', status '+result.resultCode);
                }, (error) => {
                  console.log('error: ', error);
                });
            });
          }
        }
      }else if(this.actionOption == 'ac_terminate') {
        for (var i = this.itemList.length - 1; i >= 0; i--) {
          if (this.itemList[i]) {
            let body = {
              bpm_id: this.searchTaskResult[i].bpmId
            } 
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.administratorTerminate(body, Authorization)
                .subscribe((result) => {
                  this.showToast('Bpm id '+body.bpm_id+', terminate '+result.resultDescription);
                }, (error) => {
                  console.log('error: ', error);
                });
            });
          }
        }
      }else if(this.actionOption == 'ac_force_finish' || this.actionOption == 'ac_force_restart') {
        if (this.checkAvailabilityOfEB() >= 1) {
          this.showAlert('Action tidak dapat dilakukan, Action ini hanya untuk case Exception Block');
          return false
        }
        for (var i = this.itemList.length - 1; i >= 0; i--) {
          if (this.itemList[i]) {

            if (this.searchTaskResult[i]) {
              let bpmDecisionData = "Force Restart";
              let wkDescription = "ADMINISTRATOR : Force Restart";

              if (this.actionOption == 'ac_force_finish') {
                bpmDecisionData = "Force Finish";
                wkDescription =  "ADMINISTRATOR : Force Finish";
              }

              this.newTask.request[1].data = bpmDecisionData;
              this.newTask.request[2].data = this.searchTaskResult[i]._worklistData;
              this.newTask.request[2].data.description = wkDescription;
              this.newTask.taskId = this.searchTaskResult[i].taskId;
              this.newTask.bpmId = this.searchTaskResult[i].instanceId;
              
              this.storage.get('encrypted').then((_encrypted) => {
                this.storage.get('Authorization').then((Authorization) => {
                  this.newTask.username = _encrypted.username;
                  this.newTask.password = _encrypted.password;
                  this._taskProvider.completeTask(JSON.stringify(this.newTask), Authorization)
                    .subscribe((result) => {
                        this.showToast(bpmDecisionData+' Bpm id '+this.newTask.bpmId+' completed');
                    }, (error) => {
                      this.showAlert('Network error, not completed');
                    });
                });
              });
            }
          }
        }
      }
    }else{
      this.showAlert('Please select task first to do this action!')
    }
  }

  showToast(text){
    let toast = this.toastCtrl.create({
      message: text,
      duration: 150000,
      position: 'top',
      showCloseButton: true
    });
    toast.present();
  }

  showAlert(text) { 
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  resumeAllErrorCase(){
    let confirm = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Anda yakin untuk melakuan "Resume All Error Case"?',
      buttons: [
        {
          text: 'Tidak',
          handler: () => {
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.showLoading();
            this.storage.get('Authorization').then((Authorization) => {
              this._taskProvider.getAutoStartExceptionBlock(Authorization)
                .subscribe((result) => {
                  this.showAlert('"Resume All Error Case" telah selesai');
                  this.loading.dismiss();
                }, (error) => {
                  console.log('error: ', error);
                  this.showAlert('"Resume All Error Case" error');
                  this.loading.dismiss();
                });
            });
          }
        }
      ]
    });

    confirm.present(prompt);
  }
}
