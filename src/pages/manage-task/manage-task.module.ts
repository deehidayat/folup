import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageTaskPage } from './manage-task';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ManageTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageTaskPage),
    ReactiveFormsModule
  ],
})
export class ManageTaskPageModule {}
