import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuditTrailPage } from './audit-trail';

@NgModule({
  declarations: [
    AuditTrailPage,
  ],
  imports: [
    IonicPageModule.forChild(AuditTrailPage),
  ],
})
export class AuditTrailPageModule {}
