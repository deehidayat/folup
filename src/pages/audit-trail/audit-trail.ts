import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuditTrailProvider } from '../../providers/audit-trail/audit-trail';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the AuditTrailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-audit-trail',
  templateUrl: 'audit-trail.html',
})
export class AuditTrailPage {

  public menu:any;
  public sendTime: any;
  public channelTypeModel : any;
  public sendto : any;
  public auditTrails : any;
  public startDate :any;
  public endDate : any;
  public loading: any;
  public pMenu:any;
  public pStartDate:any;
  public pEndDate:any;
  public size:10;
  searchOption:any;
  Daa:any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public auditTrailProvider: AuditTrailProvider, public toastCtrl: ToastController, public loadingCtrl: 
    LoadingController, private storage: Storage) {
      this.searchOption = { menu:false };
      
  }

  ionViewDidLoad() {
        this.pageNumber = 0;
        this.size = 10;
        this.lengthData = 0;
        this.menu="All";
        this.search();
  }

  pageNumber:0;
  lengthData:0;

  search() {
        this.auditTrails = [];
        this.pageNumber = 0;
        this.showLoading();
        this.pMenu= this.menu;
        this.pStartDate = this.startDate;
        this.pEndDate = this.endDate;
        this.getData(null);
  }

  getData(infiniteScroll) {
      if (this.pMenu === "All") {
        this.pMenu = "";
      }
      this.storage.get('Authorization').then((Authorization) => {
          this.auditTrailProvider.getAuditTrail(this.pMenu, this.pStartDate, this.pEndDate, this.pageNumber, this.size, Authorization)
          .subscribe(
          result => {                 
              for (let i = 0; i < result.length; i++) {
                  this.auditTrails.push(result[i]);        
              }
              this.loading.dismiss();
              this.lengthData = result.length;
              if (null != infiniteScroll)
                  infiniteScroll.complete();
          },
          err => {
              this.loading.dismiss();
              // this.showToast(err.message)
              console.log('error', err);
          });
        });
      }

  reset(){
  }

  // Open Detail
  openDetails(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    
    if (element.hasAttribute('style')){
      element.removeAttribute('style');
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    } else {
      element.setAttribute("style", "height:"+height+"px");
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }
  }
  // end

  //infinite scroll
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.pageNumber++;
    this.getData(infiniteScroll);
    }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true
    });
    toast.present();
  }
  
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
}
