import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrencyPipe } from '@angular/common';

import { TaskDueTodayPage } from './task-due-today';

@NgModule({
  declarations: [
    TaskDueTodayPage
  ],
  imports: [
    IonicPageModule.forChild(TaskDueTodayPage)
  ],
  entryComponents: [
    TaskDueTodayPage
  ],
  providers: [
    CurrencyPipe
  ]

})
export class TaskDueTodayPageModule { }
