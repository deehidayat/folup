import { Component } from '@angular/core';
import { Nav, Platform, MenuController, NavController, NavParams, LoadingController, Loading, IonicPage, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TaskProvider, UserProvider, MainMenuProvider } from './../../providers/providers';

@IonicPage()

@Component({
  selector: 'page-task-due-today',
  templateUrl: 'task-due-today.html'
})

export class TaskDueTodayPage {
  public availableTasks: any[] = [];
  item: any;
  tabs: Array<{id: number, active: number}>;
  items: any[] = [];
  detailsOpened: string = 'off';
  filter : string = '';
  sorting : string = '-';
  username: string;
  loading: Loading;
  _encrypted: any = {};
  sortByItem: any[] = [{value:'', text:'Default Sort', sort:''}, {value:'activityName', text:'Activity Name A-Z', sort:'asc'}, {value:'activityName', text:'Activity Name Z-A', sort:'desc'}, {value:'spajNumber', text:'SPAJ Number A-Z', sort:'asc'}, {value:'spajNumber', text:'SPAJ Number Z-A', sort:'desc'}, {value:'createdDate', text:'Created Date', sort:'asc'}];
  filterByItem: any[] = [{value:'', text:'Default Filter'},{value:'premiumIndicator-N', text:'Premium Indicator = N'},{value:'premiumIndicator-Abc1234567', text:'Premium Indicator = Abc1234567'}];
  tempItems: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private _taskProvider: TaskProvider, private storage: Storage, public mainMenu: MainMenuProvider, private loadingCtrl: LoadingController, public auth: UserProvider, private alertCtrl: AlertController) {
    storage.get('username').then((val) => {
      this.username = val;
    });

    mainMenu.activePage = 'User Action';
    mainMenu.activeChildPage = 'New Business';

    storage.get('Authorization').then((Authorization) => {
      storage.get('encrypted').then((val) => {
        this._encrypted = val;
        this.showLoading()
        this._taskProvider.getAvailableTasks(val, Authorization)
          .subscribe((availableTasks: any[]) => {
            if (availableTasks) {
              if (availableTasks.length != undefined) {
                this.availableTasks = availableTasks;
                this.items = this.availableTasks;
                this.mainMenu.taskDueCount = this.items.length;
                this.tempItems = this.availableTasks.slice(0);
              }
            }
            this.loading.dismiss();
          }, (error) => {
            this.showError(error); 
          });
      });
    });
  }

  ionViewWillEnter(){ 
    if(this.auth.loggedIn()){
      return true
    }else{
      this.auth.logout();
      this.navCtrl.setRoot('LoginPage');
    }
  } 

  openDetails(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    
    if (element.hasAttribute('style')){
      element.removeAttribute('style');
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px");
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }
  }

  openTaskDueTodayDetails(item) {
    this.navCtrl.push('TaskDueTodayDetailsPage', item);
    this.showLoading();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  sortBy(model){
    if (model != '') {
      let sortItem = model.split('-');

      if (sortItem[1] == 'asc') {
        this.items = this.tempItems.sort(function(a, b) {
          if (a[sortItem[0]] < b[sortItem[0]]) {
            return -1
          }
          if (a[sortItem[0]] > b[sortItem[0]]) {
            return 1
          }
          return 0;
        });
      }else{
        this.items = this.tempItems.sort(function(a, b) {
          if (a[sortItem[0]] < b[sortItem[0]]) {
            return 1
          }
          if (a[sortItem[0]] > b[sortItem[0]]) {
            return -1
          }
          return 0;
        });
      }
        
    }else{
      if (this.filter == '') {
        this.items = this.availableTasks;
      }else{
        this.tempItems = this.availableTasks.slice(0);
        this.filterBy(this.filter);
        this.items = this.tempItems;
      }
      
    }
  }

  filterBy(model){
    if (model != '') {
      let prop = model.split('-')[1];
      let attr = model.split('-')[0]
      this.tempItems = this.availableTasks.slice(0);
      this.items = this.tempItems.filter(function( obj ) {
        return obj[attr] == prop;
      });
      this.tempItems = this.tempItems.filter(function( obj ) {
        return obj[attr] == prop;
      });
    }else{
      this.items = this.availableTasks;
      this.tempItems = this.availableTasks.slice(0);
    }
  }

  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Error load tasks',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
