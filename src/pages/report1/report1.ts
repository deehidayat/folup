import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController,Loading, ToastController } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response, URLSearchParams, ResponseContentType } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Report1Provider } from '../../providers/report1/report1';
import { URLServices } from '../../providers/url-services';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../providers/file-download-helper';


@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report1.html',
})
export class ReportPage {

  reports: any;
  detail: any;
  sendTimeModel: Date;
  channelType: String="";
  loading: any;

  messageId: any;
  message: any;
  sendTime: any;
  agentNumber: any;
  delivered: any;
  notDelivered: any;
  sendto: any;

  reportsFaileds: any;
  status: any;
  agentInvolved: any;

  public searchOption:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public report1Provider: Report1Provider,
    public modalCtrl: ModalController, public toastCtrl: ToastController, private storage: Storage, private loadingCtrl: LoadingController,
    public urlServices: URLServices, public http:Http) {
      this.searchOption = {sendTime:false, channel:false};
      this.channelType = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }


  export(startDate, endDate, typeOfChannel) {
    const url = this.urlServices.diskomApi+"/report1api/export?startDate="+startDate+"&endDate="+endDate+"&typeOfChannel="+typeOfChannel;
    const options = new RequestOptions({responseType: ResponseContentType.Blob });

    // Process the file downloaded
    this.http.get(url, options).subscribe(res => {
        const fileName = getFileNameFromResponseContentDisposition(res);
        saveFile(res.blob(), fileName);
    });
  }

  // end

	getReportsDetailAndSummary(startDate, endDate, channelType){
		this.showLoading();
		this.storage.get('Authorization').then((Authorization) => {
			this.report1Provider.getReportsFailed(startDate, endDate, channelType, Authorization)
				.subscribe((data: [any]) => {
					this.reports = data;
					this.loading.dismiss();
				}, (error) => {
					console.log('error', error);
					this.loading.dismiss();               
				});
		})
  }
  
  // Open Detail
  openDetails(e){
  let element = e.currentTarget.nextElementSibling;
  let height = element.children[0].offsetHeight;
  
  if (element.hasAttribute('style')){
    element.removeAttribute('style');
    e.currentTarget.children[1].removeAttribute('style');
    e.currentTarget.children[0].setAttribute("style", "display:none");
  } else {
    element.setAttribute("style", "height:"+height+"px");
    e.currentTarget.children[0].removeAttribute('style');
    e.currentTarget.children[1].setAttribute("style", "display:none");
    }
  }
  // end
	
	showToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 15000,
			position: 'top',
			showCloseButton: true
		});
		toast.present();
	}

	showLoading() {
	  this.loading = this.loadingCtrl.create({
		content: 'Please wait...'
	  });
	  this.loading.present();
	}


}
