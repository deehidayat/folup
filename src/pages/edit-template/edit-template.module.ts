import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditTemplatePage } from './edit-template';
import { ComponentsModule } from './../../components/components.module';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  declarations: [
    EditTemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(EditTemplatePage),
    ComponentsModule,
    CKEditorModule
  ],
  entryComponents: [
    EditTemplatePage,
  ],
})
export class EditTemplatePageModule {}
