import { Component, AfterViewInit, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { BodyEmailTemplatePage } from '../../pages/body-email-template/body-email-template';
import { EmailTemplateProvider } from '../../providers/email-template/email-template';
import { Storage } from '@ionic/storage';
import * as CKEditor from '@ckeditor/ckeditor5-build-decoupled-document';


/**
 * Generated class for the EditTemplatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-template',
  templateUrl: 'edit-template.html',
})
export class EditTemplatePage implements AfterViewInit, OnInit {

  public totalAllowedCharacterCount = 2000;
  public charactersLeft = 2000;

  config: any;
  BodyEmailTemplatePage: BodyEmailTemplatePage;
  editForm: FormGroup;
  id: any;
  subject: any;
  body: any;
  serviceData: any;
  emailtemplates: string[] = [];
  loading:any;
  username:string;

  listError: any[];
  valid: boolean = false;
  editor: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public emailTemplateProvider: EmailTemplateProvider, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, private storage: Storage) {

    //lemparan variabel yang dipassing dari body-email-template -passing var
    this.id = navParams.get('id');
    this.subject = navParams.get('subject');
    this.body = navParams.get('body');

    this.config = {
      uiColor: '#FFFFFF',
      allowedContent: true,
      extraAllowedContent: '[id]',
      protectedSource: '/<i[^>]></i>*id/g',
      height: 200,
      // extraPlugins : 'wordcount',
      removeButtons: "Subscript,Superscript,SpecialChar,"
        + "Source,Styles,Clipboard,Format,Maximize,Strike,RemoveFormat,About,Print,"
        + "Preview,Save,NewPage,Templates,IncreaseIndent,Image,Flash,Table,-,DocProps,Cut,"
        + "Copy,Paste,PasteText,PasteFromWord,-,Undo,Redo,Image,Flash,able,HorizontalRule,"
        + "Smiley,SpecialChar,PageBreak,Iframe,Outdent,ShowBlocks,-,NumberedList,BulletedList-,"
        + "Outdent,Indent,-,Blockquote,CreateDiv,-,-,BidiLtr,BidiRtl,Find,Replace,-,SelectAll,-,"
        + "SpellChecker,Scayt,Form,Unlink,Checkbox,Radio,TextField,Textarea,Select,Button,"
        + "ImageButton,HiddenField,Anchor,BulletedList,-,Language,CopyFormatting",
      wordcount: {
        showCharCount: true,
        showWordCount: false,
        countHTML: false,
        maxCharCount: 2000
      }
    };
  }

  ngOnInit() {
    CKEditor
    .create(document.querySelector('#document-editor-editable'))
    .then(editor => {
        const toolbarContainer = document.querySelector('.document-editor__toolbar');

        toolbarContainer.appendChild( editor.ui.view.toolbar.element );
        this.editor = editor;
        this.editor.setData(this.body);
    }) 
  }

  ionViewDidLoad() {
    this.storage.get('username').then((username) => {
      this.username = username;
    })
    this.charactersLeft = this.totalAllowedCharacterCount - this.body.length;
  }

  ngAfterViewInit() {
  }

  insertAgentName(event) {
    event.insertText("[[agentName]]")
  }
  
  //fungsi edit
  edit(id, body, subject) {
    //validasi
    if ( this.subject == '' && this.body == '' || this.body == '<p>&nbsp;</p>' ){
      this.showToast("Subject dan Body Email Kosong");
      return;
    }
    if ( this.subject == '' || this.subject == null){
      alert ('Subject Email is Required');
      return;
    }
    if ( this.body == '' || this.body == null || this.body == '<p>&nbsp;</p>'){
      alert ('Body Email is Required');
      return;
    }
    if (this.body != null && this.body.length > 2000) {
      this.showToast("The length of email body exceeds the max characters allowed (2000 chars)");
      return;
    }
    
    //end validasi

    let emailedit = ({
      id: this.id,
      subject: this.subject,
      body: this.body,
      username: this.username
    });

    this.storage.get('Authorization').then((Authorization) => {
    this.emailTemplateProvider.editEmailTemplate(id, emailedit, Authorization)
    .subscribe(data => {
      this.showToast("Email Template has been succesfully edited") 
      //passing data ke body email template
      this.navCtrl.push(BodyEmailTemplatePage, {
        id: id,
        subject: subject,
        body: body
      });

      //pindah page setelah klik edit
      this.navCtrl.setRoot(BodyEmailTemplatePage); 
    }, err => {
      this.showToast(err.message);
      return;
    });
  })
  }

  //kembali ke bodyemailtemplate pas klik cancel
  cancel() {
    this.navCtrl.popTo('BodyEmailTemplatePage'); 
  }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true
    });
    toast.present();
  }

  showLoading() {
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      this.loading.present();
    }

  currentCharacterCount() {
      this.body = this.editor.getData();
      let len = this.body.length;
      return len;
  }

  remainingAvailableCharacterCount() {
      this.body = this.editor.getData();
      let len = this.body.length;
      if (len <= 2000) {
        this.charactersLeft = this.totalAllowedCharacterCount - len;
    } 

      if (this.body == '<p>&nbsp;</p>') {
        this.charactersLeft = 2000;
      }
  }

  }
