import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClaimPage } from './claim';
import { CurrencyPipe } from '@angular/common';

@NgModule({
  declarations: [
    ClaimPage,
  ],
  imports: [
    IonicPageModule.forChild(ClaimPage),
  ],
  providers: [
    CurrencyPipe
  ]
})
export class ClaimPageModule {}
