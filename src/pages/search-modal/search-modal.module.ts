import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchModalPage } from './search-modal';
import { ComponentsModule } from './../../components/components.module';

@NgModule({
  declarations: [
    SearchModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchModalPage),
    ComponentsModule
  ],
  // entryComponents: [
  //   SearchModalPage
  // ]
})
export class SearchModalPageModule {}
