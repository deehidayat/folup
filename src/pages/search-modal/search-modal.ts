import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController,Loading } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
// import { ToasterServiceProvider } from '../../providers/toaster-service/toaster-service'
// import { EmailTemplateProvider } from '../../providers/email-template/email-template';
import { EmailTemplateProvider } from '../../providers/email-template/email-template';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SearchModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-modal',
  templateUrl: 'search-modal.html',
})
export class SearchModalPage {

  public emails: any;
  public subject: String = "";
  public body: String = "";
  public subjectbody: any;
  public id: any;
  public data: any;
  public loading: any;
  public pageNumber = 0;
  public size = 10;
  public lengthData = 0;
  public pSubject:any;
  public pBody:any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public emailTemplateProvider: EmailTemplateProvider, public toastCtrl: ToastController, private storage: Storage,
    private loadingCtrl: LoadingController) {

      this.subject = "";
      this.body = "";

      this.search();
  }

  ionViewDidLoad() {
    
  }

  search() {
    this.emails = [];
    this.pageNumber = 0;
    this.pSubject = this.subject;
    this.pBody = this.body;    
    this.getData(null);
    this.showLoading();
  }

  //  fungsi get data untuk ambil data dengan rest dan infinite scroll
   getData(infiniteScroll) {
    this.storage.get('Authorization').then((Authorization) => {
    this.emailTemplateProvider.getFilterEmails(this.pBody, this.pSubject, this.pageNumber, this.size, Authorization).subscribe(
      result => {                 
        for (let i = 0; i < result.length; i++) {
          this.emails.push(result[i]);        
        }
        this.lengthData = result.length;
        if (null != infiniteScroll)
          infiniteScroll.complete();
        this.loading.dismiss();
      },
      err => {
        this.showToast(err.message);
        this.loading.dismiss();
      },
    );
  });
  }

   //infinite scroll
   doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.pageNumber++;
    this.getData(infiniteScroll);
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }
  //lempar objek ke email
  idRadio(subject, body) {
    this.subjectbody = {
      subject: subject,
      body: body
    }
  }
  useEmails() {
    this.viewCtrl.dismiss(this.subjectbody);
  }
  //lempar objek ke email


  cancel() {
    this.viewCtrl.dismiss();
  }

  reset(){
    this.search();
  }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true
    });
    toast.present();
  }   

  showLoading() {
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      this.loading.present();
  }
}
