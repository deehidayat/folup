
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,Loading } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response, URLSearchParams, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { URLServices } from '../../providers/url-services';
import { PruforceProvider } from '../../providers/pruforce/pruforce';
import { SelectAgentProvider } from '../../providers/select-agent/select-agent';
import { Storage } from '@ionic/storage';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../providers/file-download-helper';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';


@IonicPage()
@Component({
    selector: 'page-pruforce',
    templateUrl: 'pruforce.html',
})
export class PruforcePage {
    @ViewChild('fileAgentCodes') fileAgentCodes;

    // public agentTypes = ['AG', 'MA', 'AA', 'AD', 'FC', 'MF', 'AS', 'NS', 'NA', 'GAO'];
    public agency = ['AA','AD','AG', 'GAO', 'MA'];
    public fd = ['AS','FC', 'MF', 'NA' , 'NS'];


    public agent: any;
    public selectedAgentTypes: any;
    public officeCodes: any;
    public availableOfficeCodes: any;
    public loading:any;
    public body:any;
    public sendTime: any;
    public username:string;
    actionOption: string= 'now';
    actionOption1: string= 'manual';
    tempSendTime:any;

    public currentDate = new Date();
    public nowDate = new Date(this.currentDate.setHours(this.currentDate.getHours()+7)).toISOString();
    public maxDate = new Date(this.currentDate.setHours(this.currentDate.getHours()+239)).toISOString();


    public toggleButton:boolean;
    public todayDate: Date;

    constructor(private http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
      public urlServices: URLServices, public pruforceProvider: PruforceProvider, private storage: Storage,
      public agentProvider:SelectAgentProvider, public navCtrl: NavController, private alertCtrl:AlertController) {  
        this.getOfficeCodes();  
    }

    ionViewDidLoad() {
      this.storage.get('username').then((username) => {
          this.username = username;
        });
      this.agent="";
  }

  nowTime() {
    this.sendTime = this.nowDate;
  }

  download() {
    const url = this.urlServices.diskomApi+"/agentapi/downloadTemplate";
    const options = new RequestOptions({responseType: ResponseContentType.Blob });
  
    // Process the file downloaded
    this.http.get(url, options).subscribe(res => {
        const fileName = getFileNameFromResponseContentDisposition(res);
        saveFile(res.blob(), fileName);
    });
  }

    //rest untuk post pruforce
    postPru() {

    //validasi kalo subjek/body. Tanggal kosong=send now, Tanggal
    if (this.body == '' || this.body == null) {
      this.showToast('Please fill body message');
      return;
    }

    if (this.actionOption == 'now') {
      this.sendTime = this.todayDate;
    }

    if (this.actionOption == null) {
      this.showToast('Please choose time to send');
    
      return;   
    }

    if(this.actionOption == 'now' && this.sendTime != null){
      this.todayDate = new Date(this.sendTime);
      this.sendTime = this.todayDate.setHours(this.todayDate.getHours()-7);
    }

    if (this.actionOption == "later" && this.sendTime != null) {
      this.tempSendTime = new Date(this.sendTime);
      this.sendTime = this.tempSendTime.setHours(this.tempSendTime.getHours()-7);
    }

    else if(this.actionOption == 'later' && this.sendTime == null){
      this.showToast('Please Fill Date');
      return;
    }

    let alert = this.alertCtrl.create({
      title: 'Send Pruforce',
      message: '"Are you sure you want to send this Pruforce Notification?"',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            const formData = new FormData();
            if ( this.fileAgentCodes ) {
              let fileAgentCodes = this.fileAgentCodes.nativeElement;
              const formDataAgent = new FormData();
              if (fileAgentCodes.files && fileAgentCodes.files[0]) {
                  formData.append("agentcodes", fileAgentCodes.files[0]);
                };
            }

            let body = {
              body: this.body,
              sendTime: this.sendTime,
              username: this.username,
              filter: {
                agentTypes: this.selectedAgentTypes,
                officeCodes: this.officeCodes,
                agent: this.agent
                }
              };
            
              formData.append('pruforceNotification', JSON.stringify(body));
                  this.showLoading();
                  this.storage.get('Authorization').then((Authorization) => {
                    this.pruforceProvider.createPru(formData, Authorization)
                      .then((pruforceNotification:[any]) => {
                        this.showToast("Send Pruforce Notification Success");
                        this.loading.dismiss();
                        this.navCtrl.popTo('PruforceListPage');

                      }, (error) => {
                        this.showToast(error.message);
                        this.loading.dismiss();
                      });
                })
            }
          }
        ] 
      })
      alert.present();
  }

  openAccordionBody(e){
    let target = e.currentTarget ? e.currentTarget : e.target;
    let element = target.nextElementSibling;
    let height = element.children[0].offsetHeight;
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      target.children[0].removeAttribute('style');
      target.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height: "+height+"px;");
      target.children[1].removeAttribute('style');
      target.children[0].setAttribute("style", "display:none");
    }     
  }
  
  //accrodion function
  
  openChildAccordion(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let parentAccHeight = parentAcc.offsetHeight;
  
    if (parentAcc.className != 'accordion-body'){
      let element = e.target;
      while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
        parentAcc = element.parentElement;
      }
      parentAccHeight = parentAcc.offsetHeight;
    }
  
    e.stopPropagation();
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      parentAcc.setAttribute("style", "height:"+(parentAccHeight-height)+"px");
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px;");
      parentAcc.setAttribute("style", "height:"+(height+parentAccHeight)+"px");
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }
  }
  
  setParentHeight(e){
    let parent = e.currentTarget ? e.currentTarget : e.target;
    if (parent.className != 'accordion-body'){
      let element = e.target;
      while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
        parent = element.parentElement;
      }
    }
    setTimeout(()=>{
      let contentHeight = parent.children[0].offsetHeight;
      parent.setAttribute("style", "height:"+contentHeight+"px");
    },100);
  }
  //end accordion function
  
  
  _stopPropagation(e){
    e.stopPropagation();
  }
  
  accordionLoaded(e):boolean{
    let target = e.currentTarget ? e.currentTarget : e.target;
    let element = target.parentElement;
    let loaded = element.classList.contains('loaded');
    return loaded;
  }
  
  openSendTo(e) {
    if (!this.accordionLoaded(e)) {
        this.showLoading();
        this.loading.dismiss().then(()=>{this.openAccordionBody(e);});
        e.target.parentElement.classList.add('loaded');
      }else{
        this.openAccordionBody(e);
      }
    }
  
  getOfficeCodes() {
    this.storage.get('Authorization').then((Authorization) => {
        this.agentProvider.getFilterOfficeCode(Authorization).
        subscribe(data => {
            this.availableOfficeCodes = data;
            console.log(data);
        }, err => {
            return;
        });
    })
  }
  
  selectAll() {
    this.reset();
    for(let i = 0;i < this.availableOfficeCodes.length;i++)
        this.officeCodes.push(this.availableOfficeCodes[i]);
  }
  
  reset() {
    this.officeCodes = [];
  }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true
    });
    toast.present();
  }
  
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

}