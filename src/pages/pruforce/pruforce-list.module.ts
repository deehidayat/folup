import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CKEditorModule } from 'ng2-ckeditor';
import { PruforceListPage } from './pruforce-list';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    PruforceListPage
  ],
  imports: [
    IonicPageModule.forChild(PruforceListPage),    
    CKEditorModule,
    ComponentsModule
  ],
  entryComponents: [
    PruforceListPage
  ]
})
export class PruforceListPageModule {}
