import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PruforcePage } from './pruforce';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    PruforcePage
  ],
  imports: [
    IonicPageModule.forChild(PruforcePage),
    SelectSearchableModule
  ],
})
export class PruforcePageModule {}
