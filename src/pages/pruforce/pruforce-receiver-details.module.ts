import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PruforceReceiverDetailsPage } from './pruforce-receiver-details';

@NgModule({
  declarations: [
    PruforceReceiverDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PruforceReceiverDetailsPage),
  ],
})
export class PruforceReceiverDetailsPageModule {}