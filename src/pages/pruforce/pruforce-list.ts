import { Component, ViewChild } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,Loading } from 'ionic-angular';
import { SearchModalPage } from '../search-modal/search-modal';
import { Storage } from '@ionic/storage';

import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { PruforceProvider } from '../../providers/pruforce/pruforce';
import { InfiniteScroll } from 'ionic-angular/components/infinite-scroll/infinite-scroll';

@IonicPage()
@Component({
    selector: 'page-pruforce-list',
    templateUrl: 'pruforce-list.html',
})
export class PruforceListPage {

    body:String = "";
    startDate:any;
    endDate:any;
    messages:any;
    page:0;
	size:10;
    lengthData:0;
    public loading:Loading;
    searchOption:any;
    
    constructor(public http: HttpClient, public navCtrl: NavController, public navParams: NavParams, public pruforceProvider: PruforceProvider,
        public modalCtrl: ModalController, public toastCtrl: ToastController, private storage: Storage,
        private loadingCtrl: LoadingController) {
            this.body="";
            this.search();
            // passing
            
            this.navCtrl.popToRoot();
            this.searchOption = {body:false, sendTime:false};
    }

    ionViewDidLoad() {
        this.body="";    
    }
    
    search() {
        this.messages = [];
        this.page = 0;
        this.size = 10;
        this.lengthData = 0;
        this.showLoading();
        this.getData(null);
	}

    getData(infiniteScroll) {
        this.storage.get('Authorization').then((Authorization) => {
            this.pruforceProvider.getFilterPruforceNotification(this.body, this.startDate, this.endDate, this.page, this.size, Authorization)
            .subscribe(
            result => {
                for (let i = 0; i < result.length; i++) {
					this.messages.push(result[i]);        
                }
                this.lengthData = result.length;
                this.loading.dismiss();
                if (null != infiniteScroll)
                infiniteScroll.complete();
            },
            err => {
                // this.showToast(err.message);
                this.loading.dismiss();
            });
        });
    }

    //infinite scroll
    doInfinite(infiniteScroll) {
		this.page++;
		this.getData(infiniteScroll);
    }

    openEdit(i, id, agentInvolved) {
        // Sharing data using NavController
      this.navCtrl.push('PruforceReceiverDetailsPage', {
          id : id,
          agentInvolved : this.messages[i].agentInvolved
      });  
    }

    openCompose() {
        this.navCtrl.push('PruforcePage');
    }

    showToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 15000,
            position: 'top',
            showCloseButton: true,
            cssClass: 'toast'
        });
        toast.present();
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        this.loading.present();
      }
}