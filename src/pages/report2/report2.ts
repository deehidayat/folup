import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Report2Provider } from '../../providers/report2/report2';
import { URLServices } from '../../providers/url-services';
import { Http, RequestOptions, ResponseContentType } from '@angular/http';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../providers/file-download-helper';

@IonicPage()
@Component({
  selector: 'page-report2',
  templateUrl: 'report2.html',
})
export class Report2Page {

	public reports2: any;
	public startDate: any;
	public endDate: any;
	public channel: any;
	loading: Loading;
	responseType:any;
	public searchOption:any;

	constructor(public navCtrl: NavController, public navParams: NavParams, public report2Provider: Report2Provider,
		private storage: Storage, public toastCtrl: ToastController, private alertCtrl: AlertController, 
		private loadingCtrl: LoadingController, public urlServices:URLServices, private http:Http) {
			this.searchOption = { sendTime:false , channel:false };
	}

	export(startDate, endDate, channel) {
		const url = this.urlServices.diskomApi+"/report2api/export?channel="+ channel +"&startDate="+startDate+"&endDate="+endDate;
		const options = new RequestOptions({responseType: ResponseContentType.Blob });
	
		// Process the file downloaded
		this.http.get(url, options).subscribe(res => {
			const fileName = getFileNameFromResponseContentDisposition(res);
			saveFile(res.blob(), fileName);
		});
	  }

	getReports2(startDate, endDate, channel){
		this.showLoading();
		
		this.storage.get('Authorization').then((Authorization) => {
			this.report2Provider.getReports2(startDate, endDate, channel, Authorization)
				.subscribe((data: [any]) => {
					this.reports2 = data;
					this.loading.dismiss();
				}, (error) => {
					this.loading.dismiss();
					console.log('error: ', error);
				});
			});
	}
	
	openDetails(e){
		let element = e.currentTarget.nextElementSibling;
		let height = element.children[0].offsetHeight;

		if (element.hasAttribute('style')){
			element.removeAttribute('style');
			e.currentTarget.children[1].removeAttribute('style');
			e.currentTarget.children[0].setAttribute("style", "display:none");
		} else{
			element.setAttribute("style", "height:"+height+"px");
			e.currentTarget.children[0].removeAttribute('style');
			e.currentTarget.children[1].setAttribute("style", "display:none");
		}
	}

	showToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 15000,
			position: 'top',
			showCloseButton: true
		});
		toast.present();
	}
	
	showAlert(text) { 
		let alert = this.alertCtrl.create({
		  title: '',
		  subTitle: text,
		  buttons: ['OK']
		});
	}

	showLoading() {
		this.loading = this.loadingCtrl.create({
			content: 'Please wait...'
		});
		this.loading.present();
	}
}
