import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiscommPage } from './discomm';

@NgModule({
  declarations: [
    DiscommPage,
  ],
  imports: [
    IonicPageModule.forChild(DiscommPage),
  ],
})
export class DiscommPageModule {}
