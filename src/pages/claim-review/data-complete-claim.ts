export const DataCompleteClaim = {
  "request": [{
      "data": "38",
      "name": "name"
    }, {
      "data": [{
          "riderCode": "H1BR",
          "claimType":"HB",
          "decision": "approved",
          "time" : "",
          "role":""
        }, {
          "riderCode": "H1QR",
          "claimType":"HS",
          "decision": "suspend",
          "time" : "",
          "role":""
        }, {
          "riderCode": "H1ZR",
          "claimType":"HS",
          "decision": "timer",
          "time" : "4",
          "role":""
        }
        , {
          "riderCode": "H1ZR",
          "claimType":"HS",
          "decision": "other",
          "time" : "",
          "role":"CC"
        }
      ],
      "name": "bpmDecision"
    }, {
      "data": {
        "createdDate": "2018-05-01T21:41:24Z",
        "roleDistribution": "AV_MINOR",
        "activityName": "Analyst Verifier",
        "roleName": "AV",
        "policyNumber": "70809090",
        "y2logId": "38",
        "lastActionUser": "fahly"
      },
      "name": "worklistData"
    }
  ],
  "password": "6zoGH1mwKxPGhgalrMqbIG3YRMa0qlJ6",
  "username": "n3EcQ51LPitnWwf4uihjjA==",
  "taskId": "2078.607",
  "bpmId": "2072.657",
  "claimUnique": "CLAIM_201804281708851",
  "jsonRequest": {
    "payload": [{
        "policyNo": "19055420",
        "claimType": "HS",
        "riderCode": "H1QR",
        "generalClaimLifeAsia": {
          "symptonDate1": null,
          "accident": "X",
          "appealcase": "X",
          "admissionDate": "20171024",
          "inpatientDate": null,
          "claref": null,
          "relGuaranteedFlag": "X",
          "totalClaim": "0.00",
          "outpatient": "X",
          "cob": "XXX     ",
          "cobAmmount": "0.00",
          "dischargeDate": "20171024",
          "rate": 0,
          "hospitalName": "                                                            ",
          "hospitaladdrs": "                                                           ",
          "hospitalcntry": "   ",
          "hospitalCode": "#G      ",
          "registerDate": "99999999",
          "networkIndicator": " "
        },
        "claimNumberLifeAsia": {
          "policy": "19055420",
          "policyName": "SIM SIMI                      ",
          "lifeAssuredCode": "50077700",
          "lifeAssuredName": "SIM SIMI                      ",
          "product": "U1B",
          "claimSeq": "1",
          "alterationNum": "1",
          "riderClaimType": "HS",
          "riderCode": "H1QR",
          "riderDesc": null,
          "dupClaimFlag": " ",
          "dupAdmin": "          ",
          "dupInfo": "        ",
          "scanTimeStamp": null
        },
        "claimNumberNew": {
          "overrideBy": "          ",
          "overrideDate": "99999999",
          "ovrDeductReason": " ",
          "ovrApproveReason": "        ",
          "startDate": "H1QR"
        },
        "brmsResult": {
          "generatedBy": null,
          "processDate": null,
          "distributedBy": null,
          "brmsDetail": null
        },
        "policyProfilling_LifeAsia": {
          "policy": "19055420",
          "policyStat": "IF",
          "rider": "01",
          "riderDesc": null,
          "riderStat": "IF",
          "policyHolder": "50077700",
          "policyHolderName": "SIM SIMI                      ",
          "lifeAssured": "50077700",
          "lifeAssuredName": "SIM SIMI                      ",
          "riderRCD": "20141016"
        },
        "policyProfilling_HistoryOfUnderwriting": [{
            "decisionCode": " ",
            "trxDate": " ",
            "reminderDate": " ",
            "remark": " ",
            "user": " ",
            "exclusionDetail": {
              "followUpCode": " ",
              "reason": " ",
              "reasonDesc": " ",
              "detail": [{
                  "seqNo": " ",
                  "letterType": " ",
                  "exclusionNote1": " ",
                  "exclusionNote2": " ",
                  "exclusionNote3": " ",
                  "exclusionNote4": " ",
                  "exclusionNote5": " ",
                  "exclusionNote6": " "
                }
              ]
            }
          }
        ],
        "policyProfilling_AlterationNumber": {
          "transactinNo": null,
          "riderCode": "H1QR",
          "riderDesc": null,
          "plan": "C",
          "dateFrom": "20141116",
          "dateEnd": "99999999",
          "prorateFlag": "N",
          "agentCode": null
        },
        "policyProfilling_AnotherRiderCodePolicyLevel": {
          "riderCode": "H1QR",
          "riderDesc": null,
          "riderRCD": "20141016",
          "riderStat": "IF"
        },
        "policyProfilling_RiderOnClientLevel": {
          "riderCode": null,
          "riderRCD": null,
          "ridderStat": null,
          "policyNo": "19055420"
        },
        "policyProfilling_New": {
          "agentCode": "00001158",
          "crtdate": "20171024",
          "crrcd": "20141016",
          "riderRCD": null,
          "riderAgePeriod": null,
          "reinstatemenDate": null,
          "insurancePeriod1": null,
          "insurancePeriod2": null
        },
        "popUp_ClaimHistory": {
          "riderCode": "H1QR",
          "riderName": "HS",
          "riderStat": "IF",
          "riderRCD": "20141016",
          "lifeAssuredNo": "50077700",
          "lifeAssuredName": null,
          "claimHistoryTableClaimSeq": "1",
          "claimHistoryTableStat": "PN",
          "claimHistoryTableAmntPaid": "500000.00"
        },
        "popUp_UncoverStatement": {
          "uncoverReasonCode": null,
          "uncoverParam01": null,
          "uncoverParam02": null,
          "uncoverParam03": null,
          "uncoverParam04": null,
          "uncoverParam05": null
        },
        "alterationFollowUp": {
          "alterationNo": null,
          "policyStat": "IF",
          "receivedDate": null,
          "alterationTypeHeaderCode": "FXN",
          "alterationTypeHeaderDesc": "FXN",
          "tableLifeNoCode": "FXN",
          "tableLifeNoDesc": "FXN",
          "tableStatusCode": "FXN",
          "tableStatusDesc": "FXN",
          "riderRCDPolLevel": "20141016",
          "riderDescPolLevel": null,
          "riderStatPolLevel": "IF"
        },
        "relatedDocumentList": {
          "policyNo": "19055420",
          "policyOwnerNo": "50077700",
          "mainLifeAssuredNo": "19055420",
          "dob": null
        },
        "riderComponent_LifeAsia": {
          "lifeAssured": "50077700",
          "coverageNo": "01",
          "riderNo": "01",
          "riderCode": "H1QR",
          "riderName": "HS",
          "riderStat": "IF",
          "roomBoardPlan": "C",
          "sumAssured": "20141016",
          "totalUnit": null,
          "coverageRCD": null,
          "benefitBills": "99999999",
          "premCessDate": "20451114",
          "riskCessationDate": "20451114",
          "rerateDate": "20451114",
          "rerateFromDate": "20451114",
          "lapseDate": null,
          "reinstateDate": null,
          "mortalityClass": "C",
          "wpFromExistingHS": null,
          "wpStartDate": null,
          "riderAnniv": "99999999"
        },
        "hospitalisationDetail": {
          "policyNo": "19055420",
          "riderCode": "H1QR",
          "riderVersion": null,
          "claimType": "HS",
          "messages": null,
          "admissionDate": "20171024",
          "symptonDate1": "99999999",
          "subCurrencyAmount": null,
          "rate": 0,
          "remainingAnnualLimit": null,
          "remainingRoomAndBoard": "",
          "remainingICU": "",
          "remainingPhysiotheraphy": null,
          "surgeryCLaimSubmit": null,
          "personalFee": null,
          "qty": null,
          "claimpay": null,
          "doctorLincense01": "               ",
          "claimPaid": {
            "claimSubmit": null,
            "personalFee": null,
            "claimDecline": null,
            "claimPaid": null
          },
          "diagnose": [{
              "icdCode": "",
              "diagnosis": "",
              "diagnosisType": "",
              "dokter": {
                "dokterCode": "",
                "dokterName": "",
                "dokterLicense": ""
              },
              "additionalDiagnosis": "",
              "surgery": [{
                  "surgeryCode": null,
                  "surgeryRemark": null
                }
              ],
              "benefitClaim": [{
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "RBCR",
                  "benefitDescription": "Biaya Kamar & Akomodasinya    ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "ICCR",
                  "benefitDescription": "Biaya Unit Perawatan Intensif ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "GPCR",
                  "benefitDescription": "General Practitioner          ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "SPCR",
                  "benefitDescription": "Biaya Kunjungan dr. Spesialis ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "OCCR",
                  "benefitDescription": "Biaya Tindakan Bedah          ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "T4CR",
                  "benefitDescription": "Surgical Type IV              ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "T3CR",
                  "benefitDescription": "Surgical Type III             ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "T2CR",
                  "benefitDescription": "Surgical Type II              ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "T1CR",
                  "benefitDescription": "Surgical Type I               ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "MECR",
                  "benefitDescription": "Miscellaneous Expense         ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "HNCR",
                  "benefitDescription": "Home Nursing                  ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "ABCR",
                  "benefitDescription": "Local Ambulance Service       ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": "Y",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "OPCR",
                  "benefitDescription": "Out Patient Treatment         ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": " ",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "PHCR",
                  "benefitDescription": "Pre-hospitalisation           ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": " ",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "POCR",
                  "benefitDescription": "Post-hospitalisation          ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": " ",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "ETCR",
                  "benefitDescription": "Rwt Inap Darurat Diluar Area  ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": " ",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "CTCR",
                  "benefitDescription": "Biaya Perawatan Kanker        ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": " ",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }, {
                  "policyNo": "19055420",
                  "claimType": "HS",
                  "benefitCode": "KDCR",
                  "benefitDescription": "Kidney Dialysis               ",
                  "calculationCode": "",
                  "deductibleStatus": false,
                  "prorationFactorStatus": false,
                  "sequenceIterationHsAnnualLimit": 0,
                  "sequenceIterationHaAnnualLimit": 0,
                  "sequenceIterationHaPrimeLimitBooster": 0,
                  "sequenceIterationDeductible": 0,
                  "benefitLevel": " ",
                  "inpatientFlag": " ",
                  "multiInvoiceFlag": " ",
                  "labCodeFlag": " ",
                  "admissionDate": "20171024",
                  "invoice": [{
                      "kurs": "",
                      "rate": 0,
                      "invoiceDate": "",
                      "benefitAmount": 0,
                      "amountSubmitted": 0,
                      "personalFee": 0,
                      "qty": 0,
                      "claimPaid": 0,
                      "claimDecline": 0,
                      "seqnum": ""
                    }
                  ],
                  "labCode": [{
                      "seqnum": 0,
                      "dteiss": "",
                      "labcde": ""
                    }
                  ],
                  "currentClaimHistory": [],
                  "uncoverStatement": [{
                      "reasonCode": "",
                      "reasonSeq": "",
                      "param01": "",
                      "param02": "",
                      "param03": "",
                      "param04": "",
                      "param05": ""
                    }
                  ]
                }
              ],
              "diagCalculation": {
                "totalClaimSubmit": 0,
                "personalFee": 0,
                "claimDecline": 0,
                "claimPaid": 0
              }
            }
          ]
        },
        "lifeAssuredDataLifeAsia": {
          "lifeAssured": "50077700",
          "life": "SIM SIMI                                                    ",
          "dateOfBirth": "19801114",
          "sex": "F",
          "crtdate": "20171024",
          "age": null,
          "nik": "55132464654             "
        },
        "manualRegister": {
          "policyOwnerNumber": "19055420",
          "policyNumber": "19055420",
          "mainLifeAssuredNumber": "19055420",
          "mainLifeAssuredName": "50077700",
          "dobMainLifeAssured": null
        },
        "bankAccountDetail": {
          "policyHolderNumber": null,
          "policyHolderName": null,
          "claimSequence": "1",
          "lifeAssuredName": "01",
          "lifeAssuredCode": "50077700",
          "paymentApproveDate": "99999999",
          "paymentMethod": "C",
          "paymentFrequent": "00",
          "bankDescription": null,
          "bankAccount": null,
          "accountName": null,
          "bankBranch": null,
          "bankCity": null
        },
        "fullDeclineStatement": [{
            "uncoverStatementBenefit": null,
            "uncoverReasonCode": " ",
            "uncoverParameterP1": " ",
            "uncoverParameterP2": " ",
            "uncoverParameterP3": " ",
            "uncoverParameterP4": " ",
            "uncoverParameterP5": " ",
            "fullDeclineReason": null
          }
        ],
        "claimSummary": {
          "riderCode": "H1BR",
          "claimType":"HB",
          "decision": "approved",
          "time" : "",
          "role":""
        },
        "caseSummary": {
          "caseSummary_CaseLog": {
            "userclaim": " ",
            "starttime": " ",
            "taskname": " ",
            "caseLogList": [{
                "taskname": null,
                "trxin": null,
                "starttime": null,
                "userclaim": null,
                "decision": null
              }
            ]
          },
          "caseSummary_ClaimHistories": [{
              "riderCode": "H1QR",
              "claimHistoryDetail": [{
                  "totalClaimPaid": 0,
                  "admissionDate": null,
                  "claimStatus": "PN",
                  "claimAmountPaid": 0
                }, {
                  "totalClaimPaid": 0,
                  "admissionDate": null,
                  "claimStatus": "PN",
                  "claimAmountPaid": 0
                }
              ]
            }
          ]
        }
      }
    ]
  }
}