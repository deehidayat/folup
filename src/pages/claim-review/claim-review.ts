import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController,ModalController } from 'ionic-angular';
import { MainMenuProvider, UserProvider, TaskProvider } from './../../providers/providers';
import { Storage } from '@ionic/storage';

import { HistoryOfUnderwriting,
  AlterationProcess,
  AnotherRiderCode,
  RiderOnClientLevel } from '../../providers/mock-data/policy-profiling-table';

import { DaysRemains,
    BoardTypes,
    Doctors,
    ICUs, Visits, PreHospitalisations, Surgeries, Miscs } from '../../providers/mock-data/hospitalisations-data';

import { DataCompleteClaim } from './data-complete-claim';
  // Guarantors
  const GUARANTORS: any = [
    { name: 'Fullerton', value: 'fullerton', caseno: 'JKT-1234567' },
    { name: 'Raffles', value:'raffles', caseno: 'RKD-1234567'}
  ];

@IonicPage()
@Component({
  selector: 'page-claim-review',
  templateUrl: 'claim-review.html',
})
export class ClaimReviewPage {

  public dataCompleteClaim : any = DataCompleteClaim;
  tabs: Array<{id: number, active: number}>;
  data: any[] = [];
  selectedData: any = {};
  selectedPolicy: number = 0;
  loading: Loading;
  uidata: any;
  clientworksheet: any;
  dupworksheet: any;
  loose: any;
  incoming: any;
  claimunique;
  listUsers: any;
  decisions: any;
  selectedTRNOption: string = "";
  @ViewChild('decisionTRN') decisionTRN: ElementRef;
  choosedDecision: any = {};
  suspendTimeDecision: any = {suspendTime: 0, item: ['Pending Call', 'Temp Suspend', '1 Day Suspend']};
  takenDecision: any;
  transferRoleNameOption: any[] = [];
  decisionsTransferRoleName: any[] = [];
  newUncoverStatement: any = {};
  newTermsOfCoverage: any = {};
  hosTable: any[] = [" ", "Y", "N" ];
  selectedTask: any = {};
  rawJsonRequest: any;
  policyNumber: any;
  claimTask: any = {};
  dataReviewButtonDisabled: boolean = false;
  decLastActionUser: any;
  decisionMode: any;
  policyDecision: any[] = [];
  singlePolicyDecision: any = {"decision": "", "time" : "", "role":"" };
  items: any[] = [];
  selectedItems: any[] = [];
  masterData: any = {};
  showList: any[] = [false, false, false, false, false];
  data1: any[] = [];
  totalDiagnose : any[] = [];
  totalAll: any = {};
  selectedMenu: number = 0;
  additionalData: any = {};
  insurancePeriod: any;
  longDesc: any;
  longDescDiagnosis: any;
  codeDiagnosis: any;
  fullDeclineStatement: any[] = ['', ''];
  _benefitClaim: any[] = [];
  tempObject: any = [];
  reasonCodeDesc: any = [''];
  mandatoryField: any = {};

  TABS:any = [
    {id: 2, active: 0, name: 'Policy Profiling', allowed: ['Admin', 'All']},
    {id: 3, active: 0, name: 'Rider Component', allowed: ['CA', 'AV', 'All']},
    {id: 9, active: 0, name: 'BRMS Result', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 0, active: 0, name: 'General Information', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 8, active: 0, name: 'Hospitalization', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 7, active: 0, name: 'Document', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 18, active: 0, name: 'Bank Account', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 10, active: 0, name: 'Full Decline Statement', allowed: ['CA', 'All']},
    {id: 1, active: 0, name: 'Claim Details', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 4, active: 0, name: 'Life Assured', allowed: ['CA', 'AV', 'OR', 'All']},
    {id: 5, active: 0, name: 'Beneficiary', allowed: ['All']},
    {id: 6, active: 0, name: 'Claim Summary', allowed: ['CA', 'All']}
  ]

  

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: UserProvider, public mainMenu: MainMenuProvider, public task: TaskProvider, private loadingCtrl: LoadingController, public storage: Storage, private alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.tabs = [
      {id:0, active: 0},
      {id:1, active: 0},
      {id:2, active: 0},
      {id:3, active: 0},
      {id:4, active: 0},
      {id:5, active: 0}
    ]

    this.claimunique = navParams.data.claimUnique;
    this.selectedTask = navParams.data;

    setTimeout(() => {
      if(this.selectedTask.activityName == 'Incoming Handler'){
        this.tabs[2].active = 5;
      }else if(this.selectedTask.activityName == 'Client Identifier'){
        this.tabs[2].active = 3;
      }else if(this.selectedTask.activityName == 'Duplicate Handler'){
        this.tabs[2].active = 4;
      }else if(this.selectedTask.activityName == 'Loosemail Handler'){
        this.tabs[2].active = 6;
      }
    }, 1000);

    if (navParams.data != undefined && navParams.data != null) {
      this.loadData();
    }

    let myArray = ['HA', 'HS', 'HB', 'Other'];
    for (var i = 20 ; i >= 0; i--) {
      let policyno = Math.floor((Math.random() * 20000000) + 1000000);
      var rand = myArray[Math.floor(Math.random() * myArray.length)];
      this.data1.push({policy: policyno, riderClaimType:  rand, claimSeq: i});
    }
  }

  //Added by Huda for fixing CDS-265
  duplicateFormTickBtnState : {
    processedPolicyNumber : boolean,
    admissionDate : boolean,
    amountClaim : boolean,
    appealCase: boolean,
    prePostCase : boolean,
    outpatientDate: boolean
  } = {
    processedPolicyNumber : true,
    admissionDate :  true,
    amountClaim :  true,
    appealCase:  true,
    prePostCase :  true,
    outpatientDate:  true
  }

  clientHandlerFormTickBtnState : {
    processedPolicyNumber : boolean,
    processedLifeAssuredSequence : boolean,
  } = {
    processedPolicyNumber : true,
    processedLifeAssuredSequence : true,

  }
  //End Added

  draw() {
    this.loading.dismiss().then(() => {
      let arr = [];
      arr = this.claimTask.jsonRequest.payload;
      this.clientworksheet = this.claimTask.jsonRequest.payload;
      this.dupworksheet = this.claimTask.jsonRequest.payload;
      this.loose = this.claimTask.jsonRequest.payload;
      this.incoming = this.claimTask.jsonRequest.payload;
      this.masterData = this.claimTask.jsonRequest.dataMaster;

      /* Validation Reason Code with master zcrnpf */
      for (var x = 0; x < this.claimTask.jsonRequest.payload[0].hospitalisationDetail.diagnose[0].benefitClaim.length; x++) {
        for (var y = 0; y < this.masterData.zcrnpf.length; y++) {
          if (this.claimTask.jsonRequest.payload[0].hospitalisationDetail.diagnose[0].benefitClaim[x].benefitCode == this.masterData.zcrnpf[y].bencode) {
            for (var z = 0; z < this.masterData.reasonCode.length; z++) {
              if (this.masterData.zcrnpf[y].rsncde == this.masterData.reasonCode[z].itemitem) {
                this.tempObject[x].push(this.masterData.reasonCode[z]);
              }
            }
          }
          this.masterData.reasonCode = this.tempObject;
        }
      }
      let masterDataArr = ["hospital", "additionalDiagnose", "surgeryType", "doctorName", "labcode"]
      for (let i = 0; i < masterDataArr.length ; i++) {
        this.initializeItems(this.masterData[masterDataArr[i]], i+1);
      }

      for (let i = 0; i < arr.length; i++) {
        let tmp = arr[i];
        this.data.push(tmp);
        this.policyDecision.push(tmp.claimSummary);
      }
      this.selectData(0);
    });
  }

  loadData(){
    this.storage.get('Authorization').then((Authorization) => {
      this.storage.get('encrypted').then((_encrypted) => {
        if (_encrypted) {
          _encrypted.taskId = this.selectedTask.taskId;
          _encrypted.claimUnique = this.selectedTask.claimUnique;
          this.showLoading();
          this.task.getClaimTask(_encrypted, Authorization)
            .subscribe((result:any) => {
              if (!result.jsonRequest) {
                this.loading.dismiss();
                return false;
              }else{
                if (!result.jsonRequest.payload) {
                  this.loading.dismiss();
                  return false;
                }
              }
              this.rawJsonRequest = result.jsonRequest ? result.jsonRequest : "";
              this.policyNumber = this.selectedTask.policyNumber;
              this.storage.set('claimTask', result);
              this.storage.set('jsonReq', result.jsonRequest);
              this.claimTask = result;
              this.claimTask.jsonRequest.payload[0].riderComponent_LifeAsia.riderCode = this.claimTask.jsonRequest.payload[0].riderComponent_LifeAsia.riderCode + " - " + this.claimTask.jsonRequest.payload[0].riderComponent_LifeAsia.riderName;
              this.uidata = result.jsonRequest ? result.jsonRequest : {};
              this.draw();
          }, (error) => {
            this.showAlert('Error load data.');
            console.log('error', error);
          },()=>{
            this.mainMenu.selectedMenu = 0;
          });

          this.auth.getMandatoryField(Authorization)
            .subscribe((result:any) => {
              if (result.status == 200) {
                for (var i = result.content.length - 1; i >= 0; i--) {                  
                  this.mandatoryField['field_'+result.content[i].fieldId] = result.content[i];
                }
              }
            }, (error) => {
              console.log('error', error);
            });
        }
      });
    });
  }

  loadClient() {
    this.task.getClientWorksheet(this.claimunique)
    .subscribe(datax => {
      this.clientworksheet = datax;
      this.loadWorksheet();
    })
  }

  loadWorksheet(){
    this.task.getDupWorksheet(this.claimunique)
    .subscribe(datax => {
      this.dupworksheet = datax;
      this.loadLoose();
    })
  }

  loadLoose(){
    this.task.getLooseHandler(this.claimunique)
    .subscribe(datax => {
      this.loose = datax;
      this.loadIncoming()
    })
  }

  loadIncoming(){
    this.task.getIncomingHandler(this.claimunique)
    .subscribe(datax => {
      this.incoming = datax;
    })
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter(){
    if(this.auth.loggedIn()){
      return true
    }else{
      this.auth.logout();
      this.navCtrl.setRoot('LoginPage');
    }
  }

  actTab(tabs, a, e){
    tabs.active = a;
    if (this.tabs[2].active == 13){
      this.openDecision();
    }
    if (this.tabs[1].active == 8){

    }
  }

  splitDate(string){
    if(string.length <= 8){
      let _string = string.toString();
      let segment1 = _string.substr(0,4);
      let segment2 = _string.substr(4,2);
      let segment3 = _string.substr(6,2);
      return segment1+'-'+segment2+'-'+segment3;
    }else{
      return string;
    }
  }

  calculateDays(date1, date2, model, model_child){
    let dateone = new Date(this.splitDate(date1)).getTime();
    let datetwo = new Date(this.splitDate(date2)).getTime();
    model[model_child] = (dateone-datetwo) /60/60/24/1000;
  }

  calculateAge(birthday, model, model_child) {
    if (birthday.target) {
      birthday = birthday.target.value
    }

    let dateone =  Date.now();
    let datetwo = new Date(this.splitDate(birthday)).getTime();
    model[model_child] = Math.round((dateone-datetwo) /60/60/24/365/1000);
  }

  validateDate(admissionDate, dischargeDate, model, model_child){
    let dateNow = Date.now();
    admissionDate = new Date(this.splitDate(admissionDate)).getTime();
    dischargeDate = new Date(this.splitDate(dischargeDate)).getTime();

    if(admissionDate <= dateNow){
      this.showAlert('Admission Date not valid! must be larger than the current date ');
    }else if(admissionDate <= dischargeDate){
      this.showAlert('Admission Date not valid! must be larger than the Discharge Date');
    }
  }

  selectData(index){
    var currentDate1 = this.claimTask.jsonRequest.payload[0].policyProfilling_LifeAsia.riderRCD;
    var currentDate2 = this.claimTask.jsonRequest.payload[0].generalClaimLifeAsia.admissionDate;

    var year1 = currentDate1.substr(0, 4);
    var year2 = currentDate2.substr(0, 4);

    var month1 = currentDate1.substr(4, 2);
    var month2 = currentDate2.substr(4, 2);

    var day1 = currentDate1.substr(6, 2);
    var day2 = currentDate2.substr(6, 2);

    var date2 = +new Date(year1, month1, day1);
    var date1 = +new Date(year2, month2, day2);

    var diff = date1 - date2;

    var years = Math.floor(diff/31536000000);
    var months = Math.floor((diff % 31536000000)/2628000000);
    var days = Math.floor(((diff % 31536000000) % 2628000000)/86400000);

    this.insurancePeriod = years + " Year(s) " + months + " Month(s) " + days + " Day(s)";

    this.storage.get('claimTask').then((claimTask) => {
      if (claimTask != undefined) {
        let new_invoice = {
          "kurs": "Not Available",
          "rate": 0,
          "invoiceDate": "Not Available",
          "benefitAmount": 0,
          "amountSubmitted": 0,
          "personalFee": 0,
          "qty": 0,
          "claimPaid": 0,
          "claimDecline": 0,
          "seqnum": "Not Available",
          "seqnumb": "Not Available"
        }
        let arr = claimTask.jsonRequest.payload;
        this.selectedData = arr[index];
        this.selectedData.index = index;
        this.selectedData.uncoverStatement = []
        if (this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia) {
          this.additionalData.generalClaimLifeAsia = {};
          this.calculateDays(this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.dischargeDate, this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.admissionDate, this.additionalData.generalClaimLifeAsia, 'los');
        }

        if (this.claimTask.jsonRequest.payload[this.selectedData.index].lifeAssuredDataLifeAsia) {
          this.additionalData.lifeAssuredDataLifeAsia = {};
          this.calculateAge(this.claimTask.jsonRequest.payload[this.selectedData.index].lifeAssuredDataLifeAsia.dateOfBirth, this.claimTask.jsonRequest.payload[this.selectedData.index].lifeAssuredDataLifeAsia, 'age');
        }
        this.diagnoses = [];
        this.diagnoseSections = [];
        this.benefitClaim = [];
        this._benefitClaim = [];
        if (this.selectedData) {
          let tmp = this.selectedData.hospitalisationDetail.diagnose;
          for (var i = 0; i < tmp.length; i++) {
            this.diagnoseSections.push({ id: i, name: "SSS", display: false, icon: 'md-add-circle' });
            this.diagnoses.push({ id: tmp[i].icdCode, name: tmp[i].diagnosis, display: true, icon: 'md-remove-circle' });
            let tmp1 = tmp[i].benefitClaim;
            let cnt = 0;
            let pcnt = 0;
            this._benefitClaim.push(tmp[i]);
            this.totalDiagnose[i] = {
                    "amountSubmitted": 0,
                    "personalFee": 0,
                    "claimPaid": 0,
                    "claimDecline": 0
                  }

            for (var y = 0; y < tmp1.length; y++) {
              let bf = tmp1[y];
              if (bf.benefitLevel == 'Y') {
                this.benefitClaim[cnt] = bf;
                this.benefitClaim[cnt].child = [];
                this.benefitClaim[cnt].index = y;
                this.benefitClaim[cnt].totalChild = {
                  "amountSubmitted": 0,
                  "personalFee": 0,
                  "claimPaid": 0,
                  "claimDecline": 0,
                  "qty": 0,
                  "benefitAmount": 0
                }

                let objc = bf.uncoverStatement;
                this.selectedData.uncoverStatement[y] =[];
                for (var i = 0; i < objc.length ; i++) {
                  if (objc[i]) {
                    var us = Object.keys(objc[i]).map(function(key) {
                      return [key, objc[i][key]];
                    });
                    this.selectedData.uncoverStatement[y].push(us);
                  }
                }

                if (this.benefitClaim[cnt].invoice.length  <= 0) {
                  this.benefitClaim[cnt].invoice.push(new_invoice);
                  this.claimTask.jsonRequest.payload[index].hospitalisationDetail.diagnose[i].benefitClaim[y].invoice.push(new_invoice);
                }
                cnt++;
                pcnt = 0;
              } else {
                if (this.benefitClaim[cnt - 1].child) {
                  this.benefitClaim[cnt - 1].child[pcnt]=bf;
                  this.benefitClaim[cnt - 1].child[pcnt].index = y;
                  if (this.benefitClaim[cnt - 1].child[pcnt].invoice.length  <= 0) {
                    this.benefitClaim[cnt - 1].child[pcnt].invoice.push(new_invoice);
                    this.claimTask.jsonRequest.payload[index].hospitalisationDetail.diagnose[i].benefitClaim[y].invoice.push(new_invoice);
                  }
                  pcnt++;
                }
              }
            }
          }

          this.showLoading();
          for (var d = this.selectedData.hospitalisationDetail.diagnose.length - 1; d >= 0; d--) {
            for (var i = this.benefitClaim.length - 1; i >= 0; i--) {
              this.totalHeaderBenefit(i, d);
            }
            this.countTotalDiagnose(d);
          }
          this.loading.dismiss();

          let tmph = this.selectedData.policyProfilling_HistoryOfUnderwriting;
          for (var y = 0; y < tmph.length; y++) {
            let hu = tmph[y];
            let hitem:any[] = [];
            let hiarr = { col:1, field: 'Decision Code', value: hu.decisionCode, btnPopup: true};
            hitem[0] = hiarr;
      hiarr = { col:2, field: 'Reminder Date', value: hu.reminderDate, btnPopup: false};
            hitem[1] = hiarr;
      hiarr = { col:3, field: 'Remark', value: hu.remark, btnPopup: false};
            hitem[2] = hiarr;
      hiarr = { col:4, field: 'User Profile', value: hu.user, btnPopup: false};
            hitem[3] = hiarr;
      let harr = {id: y+1, items: hitem}
            this.table01[y] = harr;
          }

      let tmpa = this.selectedData.policyProfilling_AlterationNumber;
          let hu_a = tmpa;
          let hitem_a:any[] = [];
          let hiarr_a = { col:1, field: 'Transaction No.', value: hu_a.transactinNo, btnPopup: true};
          hitem_a[0] = hiarr_a;
      hiarr_a = { col:2, field: 'Rider Code', value: hu_a.riderCode, btnPopup: false};
          hitem_a[1] = hiarr_a;
      hiarr_a = { col:3, field: 'Plan', value: hu_a.plan, btnPopup: false};
          hitem_a[2] = hiarr_a;
      hiarr_a = { col:4, field: 'Start Date', value: hu_a.dateFrom, btnPopup: false};
          hitem_a[3] = hiarr_a;
      hiarr_a = { col:5, field: 'End Date', value: hu_a.dateEnd, btnPopup: false};
          hitem_a[4] = hiarr_a;
      hiarr_a = { col:6, field: 'Prorate Flag', value: hu_a.prorateFlag, btnPopup: false};
          hitem_a[5] = hiarr_a;
        let harr_a = {id: y+1, items: hitem_a}
          this.table02[0] = harr_a;

      let tmpl = this.selectedData.policyProfilling_AnotherRiderCodePolicyLevel;
          let hu_l = tmpl;
          let hitem_l:any[] = [];
          let hiarr_l = { col:1, field: 'Rider Code', value: hu_l.riderCode, btnPopup: false};
          hitem_l[0] = hiarr_l;
      hiarr_l = { col:2, field: 'RCD Rider', value: hu_l.riderRCD, btnPopup: false};
          hitem_l[1] = hiarr_l;
      hiarr_l = { col:3, field: 'Rider Description', value: hu_l.riderDesc, btnPopup: false};
          hitem_l[2] = hiarr_l;
      hiarr_l = { col:4, field: 'Rider Status', value: hu_l.riderStat, btnPopup: false};
          hitem_l[3] = hiarr_l;
        let harr_l = {id: y+1, items: hitem_l}
          this.table03[0] = harr_l;

      let tmpc = this.selectedData.policyProfilling_RiderOnClientLevel;
          let hu_c = tmpc;
          let hitem_c:any[] = [];
          let hiarr_c = { col:1, field: 'Policy No', value: hu_c.policyNo, btnPopup: false};
          hitem_c[0] = hiarr_c;
          hiarr_c = { col:2, field: 'Rider Code', value: hu_c.riderCode, btnPopup: false};
          hitem_c[1] = hiarr_c;
      hiarr_c = { col:3, field: 'RCD Rider', value: hu_c.riderRCD, btnPopup: false};
          hitem_c[2] = hiarr_c;
      hiarr_c = { col:4, field: 'Rider Description', value: hu_c.riderDesc, btnPopup: false};
          hitem_c[3] = hiarr_c;
      hiarr_c = { col:5, field: 'Rider Status', value: hu_c.riderStat, btnPopup: false};
          hitem_c[4] = hiarr_c;
        let harr_c = {id: y+1, items: hitem_c}
          this.table04[0] = harr_c;

        }
      }
    });
  }

  showExcl() {
    let exclModal = this.modalCtrl.create('PopExclusionDetailPage');
    exclModal.present();
  }

  addInvoice(e, index, idx, i){
    if (this.benefitClaim[idx] && this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[i].benefitClaim[index]) {
      let today = new Date();
      let dd = today.getDate();
      let mm = today.getMonth()+1;
      let dds = dd.toString();
      let mms = mm.toString();

      let yyyy = today.getFullYear();
      if(dd<10){
          dds='0'+dd;
      }
      if(mm<10){
          mms='0'+mm;
      }
      let invoiceDate = yyyy+''+mms+''+dds;

      let newBenefInvoice = {
        "kurs": "",
        "rate": 0,
        "invoiceDate": invoiceDate,
        "benefitAmount": 0,
        "amountSubmitted": 0,
        "personalFee": 0,
        "qty": 1,
        "claimPaid": 0,
        "claimDecline": 0,
        "seqnum": "",
        "seqnumb": ""
      }

      this.benefitClaim[idx].invoice.push(newBenefInvoice);
      this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[i].benefitClaim[index].invoice.push(newBenefInvoice);

    }
  }
  addDiagnoses(e){
    var counter = this.diagnoses.length;
    this.diagnoses.push({ id: (counter + 1), name: "AAA", display: true, icon: 'md-remove-circle' });
  }

  countTotalDiagnose(index){
    this.totalDiagnose[index] = {
      "amountSubmitted": 0,
      "personalFee": 0,
      "claimPaid": 0,
      "claimDecline": 0
    }
    for (var i = 0; i < this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].benefitClaim.length; i++) {
      for (var bf_invoice = 0; bf_invoice < this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].benefitClaim[i].invoice.length; bf_invoice++) {
        this.totalDiagnose[index].amountSubmitted = (parseFloat(this.totalDiagnose[index].amountSubmitted) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].benefitClaim[i].invoice[bf_invoice].amountSubmitted)).toFixed(2);
        this.totalDiagnose[index].personalFee = (parseFloat(this.totalDiagnose[index].personalFee) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].benefitClaim[i].invoice[bf_invoice].personalFee)).toFixed(2);
        this.totalDiagnose[index].claimPaid = (parseFloat(this.totalDiagnose[index].claimPaid) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].benefitClaim[i].invoice[bf_invoice].claimPaid)).toFixed(2);
        this.totalDiagnose[index].claimDecline = (parseFloat(this.totalDiagnose[index].claimDecline) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].benefitClaim[i].invoice[bf_invoice].claimDecline)).toFixed(2);
      }
    }
    this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].diagCalculation.totalClaimSubmit = this.totalDiagnose[index].amountSubmitted;
    this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].diagCalculation.personalFee = this.totalDiagnose[index].personalFee;
    this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].diagCalculation.claimDecline = this.totalDiagnose[index].claimDecline;
    this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[index].diagCalculation.claimPaid = this.totalDiagnose[index].claimPaid;

    this.countTotalAll();
  }

  countTotalAll(){
    this.totalAll = {
      "amountSubmitted": 0,
      "personalFee": 0,
      "claimPaid": 0,
      "claimDecline": 0
    }

    for (var i = this.totalDiagnose.length - 1; i >= 0; i--) {
      this.totalAll.amountSubmitted = this.totalAll.amountSubmitted + this.totalDiagnose[i].amountSubmitted;
      this.totalAll.personalFee = this.totalAll.personalFee + this.totalDiagnose[i].personalFee;
      this.totalAll.claimPaid = this.totalAll.claimPaid + this.totalDiagnose[i].claimPaid;
      this.totalAll.claimDecline = this.totalAll.claimDecline + this.totalDiagnose[i].claimDecline;
    }
  }
  /**
   * Case Review Section
   */

  // General Information
  claimTypes:any[] = [
    {id: 1, name: 'Outpatient', checked: false},
    {id: 2, name: 'One Day Care', checked: false},
    {id: 3, name: 'Kidney Dialysis', checked: false},
    {id: 4, name: 'Death Claim', checked: false},
    {id: 5, name: 'Appeal Case', checked: false},
    {id: 6, name: 'Guaranteed', checked: false},
    {id: 7, name: 'Accident', checked: false},
    {id: 8, name: 'CA Treatment', checked: false}
  ];

  // Get Guarantee checkbox status
  displayGuarantee:any = this.claimTypes[5].checked;

  updateClaimType(id) {
    let obj = this.claimTypes.find(data => {
      return data.id === id;
    });
    if(obj.checked) {
      if(obj.id == 6) {
        this.displayGuarantee = false;
      }
      obj.checked = false;
    } else {
      obj.checked = true;
      if(obj.id == 6) {
        this.displayGuarantee = true;
      }
    }
  }

  guaranteeOptions:any[] = GUARANTORS;

  //Guarantor Init
  selectedGuarantor = this.guaranteeOptions[0].value;
  guarantorCaseno = this.guaranteeOptions[0].caseno;

  // Updates Guarantor Case Number
  onSelectGuarantor(item:string) {
    let obj:any = this.guaranteeOptions.find(data => {
      return data.value === item;
    });
    this.selectedGuarantor = obj.value;
    this.guarantorCaseno = obj.caseno;
  }

  // Policy Profiling
  table01:any[] = [];
  table02:any[] = [];
  table03:any[] = [];
  table04:any[] = [];

  tables: any[] = [
    {
      id: 'table01',
      name: 'History of Underwriting',
      fields: ['Decision Code','Reminder Date','Remark','User Profile'],
      data: this.table01,
      icon: 'md-remove-circle',
      display: true,
      allowed: ['HA', 'HS', 'HB', 'Other']
    },
    {
      id: 'table02',
      name: 'Alteration Process',
      fields: ['Transaction No.', 'Rider Code', 'Plan', 'Start Date', 'End Date', 'Prorate Flag'],
      data: this.table02,
      icon: 'md-add-circle',
      display: false,
      allowed: ['HA', 'HS', 'HB', 'Other']
     },
    {
      id: 'table03',
      name: 'Another Rider Code of Policy Level',
      fields: ['Rider Code', 'RCD Rider', 'Rider Description', 'Rider Status'],
      data: this.table03,
      icon: 'md-add-circle',
      display: false,
      allowed: ['HA', 'HS', 'HB', 'Other']
    },
    {
      id: 'table04',
      name: 'Rider on Client Level',
      fields: ['Policy Number', 'Rider Code', 'Rider Description', 'RCD Rider', 'Rider Status'],
      data: this.table04,
      icon: 'md-add-circle',
      display: false,
      allowed: ['HA', 'HS', 'HB', 'Other']
    },
  ]

  toggleDetails(id:string) {
    let table:any = this.tables.find(data => {
      return data.id === id;
    })
    if(table.display) {
      table.display = false;
      table.icon = 'md-add-circle';
    } else {
      table.display = true;
      table.icon = 'md-remove-circle';
    }
  }

  // Beneficiary
  beneficiaries:any[] = [
    {
      id: 1,
      name: 'Arya Stark',
      percentage: 70,
      amount: 12345566,
      relationship: 'Sister',
      display: true,
      icon: 'md-remove-circle'
    },
    {
      id: 2,
      name: 'Bran Stark',
      percentage: 60,
      amount: 1234566,
      relationship: 'Brother',
      display: true,
      icon: 'md-remove-circle'
    }
  ];

  toggleDisplay(id:any) {
    let item = this.beneficiaries.find(data => {
      return data.id === id;
    });
    if(item.display) {
      item.display = false;
      item.icon = 'md-add-circle';

    } else {
      item.display = true;
      item.icon = 'md-remove-circle';
    }
  }


  // Client Worksheet
  cwtable:any = [
    {policyNo: 12345678, phNo: 10001, phName: 'BRIAN AUDITA PA', laNo: 10001, laName: 'BRIAN AUDITA PA', laSequence: 100001, code: 'HS', checked: true},
    {policyNo: 11111112, phNo: 10002, phName: 'TONY STARKS', laNo: 10002, laName: 'IRON MAN', laSequence: 100002, code: 'HS', checked: true},
    {policyNo: 11111113, phNo: 10003, phName: 'STEVE ROGERS', laNo: 10003, laName: 'CAPTAIN AMERICA', laSequence: 100003, code: 'HS', checked: true},
    {policyNo: 11111114, phNo: 10004, phName: 'BRUCE BANNER', laNo: 10004, laName: 'HULK', laSequence: 100004, code: 'HS', checked: true},  {policyNo: 11111115, phNo: 10005, phName: 'THOR', laNo: 10005, laName: 'THOR', laSequence: 100005, code: 'HS', checked: true},
  ]

  // Duplication Worksheet
  dwtable:any = [
    {policyNo: 1000001, admissionDate: 'Apr 20 2018', amount: 100200, prepostCase: 'Yes', outpatientDate: 'Apr 24 2018'},
    {policyNo: 1000002, admissionDate: 'Apr 20 2018', amount: 100200, prepostCase: 'Yes', outpatientDate: 'Apr 24 2018'},
    {policyNo: 1000003, admissionDate: 'Apr 20 2018', amount: 100200, prepostCase: 'Yes', outpatientDate: 'Apr 24 2018'},
    {policyNo: 1000004, admissionDate: 'Apr 20 2018', amount: 100200, prepostCase: 'Yes', outpatientDate: 'Apr 24 2018'},
  ]

  // Claim Summary
  decision:any = 'done';

  // Document
  docSubmitted = 'doc1';
  receipt = 'receipt1';
  status = 1;
  requestor = 1;
  description = 1;

  // Hospitalisation
  diagnosis = 'a01';
  diagnoseType = 'd01';
  doctor = 'doctor01';
  doctorLicense = 'l01';
  surgeryType = 'minor';
  action = 1;

  dayRemains:any[] = DaysRemains;
  boardTypes:any[] = BoardTypes;
  doctors:any[] = Doctors;
  icus:any[] = ICUs;
  visits:any = Visits;
  preHospitalisations:any = PreHospitalisations;
  // surgeries:any[] = Surgeries;
  miscs:any = Miscs;

  diagnoseSections:any[] = [];
  diagnoses:any[] = [];
  benefitClaim:any[] = [];
  benefitclaimchild:any[] = [];

  surgeries:any[] = [
    {
      benefit: 'Surgery Type II',
      limit: '49,800,000',
      qty: 0,
      submit: 0,
      fee: 0,
      decline: 0,
      paid: '9.800.000',
      surgeryDetails: [
        {benefit: "Biaya Obat-obatan", limit: 0, qty:0, submit: '100000', fee:0, decline:0, paid:0},
        {benefit: "Biaya Pemeriksaan Penunjang", limit: 0, qty:0, submit: '230000', fee:0, decline:0, paid:0},
        {benefit: "Biaya Test Lab", limit: 0, qty:0, submit: '60000', fee:0, decline:0, paid:0},
        {benefit: "Biaya Aneka Perawatan Lain", limit: 0, qty:0, submit: '130000', fee:0, decline:0, paid:0}
      ]
    }
  ]

  addSurgery(e, i) {
    var newSurgery = {
      "surgeryCode": "NA",
      "surgeryRemark": "",
      "surgeryType": "",
      "detail": [
        {
          "benefitCode": "MC",
          "benefitDesc": "Biaya Rawat Operasi",
          "amountSubmitted": 0,
          "personalFee": 0
        },
        {
          "benefitCode": "TC",
          "benefitDesc": "Biaya Peralatan",
          "amountSubmitted": 0,
          "personalFee": 0
        },
        {
          "benefitCode": "SC",
          "benefitDesc": "Biaya Ahli Bedah",
          "amountSubmitted": 0,
          "personalFee": 0
        },
        {
          "benefitCode": "OO",
          "benefitDesc": "BiayaBedah Lain-lain",
          "amountSubmitted": 0,
          "personalFee": 0
        }
      ]
    }

    this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[i].surgery.push(newSurgery);
  }

  benefitSurgeries:any[] = [
    {
      benefit: 'Surgery Type II',
      limit: '49,800,000',
      qty: 0,
      submit: 0,
      fee: 0,
      decline: 0,
      paid: '9.800.000',
      surgeryDetails: [
        {benefit: "Biaya Obat-obatan", limit: 0, qty:0, submit: '100000', fee:0, decline:0, paid:0},
        {benefit: "Biaya Pemeriksaan Penunjang", limit: 0, qty:0, submit: '230000', fee:0, decline:0, paid:0},
        {benefit: "Biaya Test Lab", limit: 0, qty:0, submit: '60000', fee:0, decline:0, paid:0},
        {benefit: "Biaya Aneka Perawatan Lain", limit: 0, qty:0, submit: '130000', fee:0, decline:0, paid:0}
      ]
    }
  ]

  surgeryCounter(arr){
    var counter = 0;
    for (var i = 0; i < arr.length; i++) {
      counter = counter + parseFloat(arr[i].submit || 0);
    }

    return counter;
  }

  displayToggle(id) {
    let obj = this.diagnoseSections.find(data => {
      return data.id === id;
    });
    if(obj.display) {
      obj.display = false;
      obj.icon = 'md-add-circle';
    } else {
      obj.display = true;
      obj.icon = 'md-remove-circle';
    }
  }

  displayDiagnoses(id) {
    let obj = this.diagnoses.find(data => {
      return data.id === id;
    });
    if(obj.display) {
      obj.display = false;
      obj.icon = 'md-add-circle';
    } else {
      obj.display = true;
      obj.icon = 'md-remove-circle';
    }
  }

  gotoDetails() {
    console.log('passed')
  }

  //


/**
 * Case Summary Section
 */

  // Case Summary Tabs
  claimHostoryData:any[] = [
    {
      rider: 'H1QR - PRUhospital and Surgery Cover Plus',
      riderStatus: 'In force',
      rcdRider: '5678945 - ong agus sutina taslim'
    }
  ]
  // End Case Summary Tabs

  openAccordionBody(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px;");
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }
  }

  openChildAccordion(e){
    let element = e.currentTarget.nextElementSibling;
    let height = element.children[0].offsetHeight;
    let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let parentAccHeight = parentAcc.offsetHeight;

    // if (parentAcc != null) {
      if (parentAcc.className != 'accordion-body'){
        let element = e.target;
        while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
          parentAcc = element.parentElement;
        }
        if (parentAcc != null) {
        parentAccHeight = parentAcc.offsetHeight;
      }
    }

    e.stopPropagation();
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      if (parentAcc) {parentAcc.setAttribute("style", "height:auto");}
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height:"+height+"px;");
      if (parentAcc) {parentAcc.setAttribute("style", "height:auto");}
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }
  }
  openDetailsListTable(e){
    let element = e.currentTarget.nextElementSibling.nextElementSibling;
    let height = element.children[0].offsetHeight;
    let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let parentAccHeight = parentAcc.offsetHeight;

    // if (parentAcc != null) {
    if (parentAcc.className != 'accordion-body'){
      let element = e.target;
      while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
        parentAcc = element.parentElement;
      }
      if (parentAcc != null) {
        parentAccHeight = parentAcc.offsetHeight;
      }
    }

    e.stopPropagation();
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      if (parentAcc) {parentAcc.setAttribute("style", "height:auto");}
      e.currentTarget.children[0].removeAttribute('style');
      e.currentTarget.children[1].setAttribute("style", "display:none");
    }else{
      element.setAttribute("style", "height: auto");
      if (parentAcc) {parentAcc.setAttribute("style", "height:auto");}
      e.currentTarget.children[1].removeAttribute('style');
      e.currentTarget.children[0].setAttribute("style", "display:none");
    }
  }

  openChildAccordionHospitalization(e){
    let element = e.currentTarget.parentElement.nextElementSibling;
    let height = element.children[0].offsetHeight;
    let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let parentAccHeight = parentAcc.offsetHeight;

    // if (parentAcc != null) {
      if (parentAcc.className != 'accordion-body'){
        let element = e.target;
        while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
          parentAcc = element.parentElement;
        }
        if (parentAcc != null) {
        parentAccHeight = parentAcc.offsetHeight;
      }
    }

    e.stopPropagation();
    if (height > 1000){element.setAttribute("transition-duration", "1000");}
    else{element.setAttribute("transition-duration", "300");}
    if (element.hasAttribute('style')){
      element.removeAttribute("style");
      if (parentAcc) {parentAcc.setAttribute("style", "height:"+(parentAccHeight-height)+"px");}
      if (e.currentTarget.children[0]) {
        e.currentTarget.children[0].removeAttribute('style');
        e.currentTarget.children[1].setAttribute("style", "display:none");
      }
    }else{
      element.setAttribute("style", "height: auto;");
      if (parentAcc) {parentAcc.setAttribute("style", "height:"+(height+parentAccHeight)+"px");}
      if (e.currentTarget.children[1]) {
        e.currentTarget.children[1].removeAttribute('style');
        e.currentTarget.children[0].setAttribute("style", "display:none");
      }
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  triggerReason(event, model) {
    this.longDesc = model.longdesc;
  }

  getReasonCodeDesc(event, model, index){
    this.reasonCodeDesc[index] = model.longdesc;
  }

  triggerCheckboxChange(event, index){
    if (event.checked) {
      this.claimTask.jsonRequest.payload[index].generalClaimLifeAsia.accident = 'Y';
    }else {
      this.claimTask.jsonRequest.payload[index].generalClaimLifeAsia.accident = 'X';
    }
  }

  openDecision(){
    this.showLoading();
    let roleName = this.navParams.data.roleName;
    this.storage.get('Authorization').then((Authorization) => {
      this.task.getDecisions(roleName, this.navParams.data.activityName, Authorization)
        .subscribe((result) => {
          if(result.length != undefined){
            this.decisions = result;
          }
        }, (error) => {
          console.log('error: ', error);
        });

      this.task.listUsers(Authorization)
        .subscribe((result: any[]) => {
          if (result.length != undefined) {
            result.sort();
            this.listUsers = result;
          }
        }, (error) => {
          console.log('error: ', error);
        });
    });
    this.loading.dismiss();
  }

  chooseDecision(e, dec){
    this.choosedDecision.decision_desc = dec;
    this.choosedDecision.transfer_role_name = "";
    this.decisionsTransferRoleName = [];
    if ( this.choosedDecision.decision_desc == '1 Day Suspend') {
      this.suspendTimeDecision.suspendTime = 24;
    }else{
      this.suspendTimeDecision.suspendTime = 0;
    }

    let transfer = this.choosedDecision.decision_desc.search("Send to Other Roles");

    if (this.choosedDecision.decision_desc == 'Transfer To UW' || transfer >= 0) {
      this.storage.get('Authorization').then((Authorization) => {
        let get_desc = this.decisions.filter((obj)=>{
          return obj.decision_desc == this.choosedDecision.decision_desc;
        });
        if (get_desc[0]) {
          this.choosedDecision.transfer_role_name = get_desc[0].transfer_role_name;
        }

        this.task.getDecisionTransferRole(this.choosedDecision.transfer_role_name, Authorization)
          .subscribe((dtrn) => {
            if(dtrn.length != undefined){
              this.decisionsTransferRoleName = dtrn;
            }
          }, (error) => {
            console.log('error: ', error);
          });
      });
    }
    // if (dec) {
    //   this.choosedDecision = dec;
    //   if ( dec.decision_desc == '1 Day Suspend') {
    //     this.suspendTimeDecision.suspendTime = 24;
    //   }else{
    //     this.suspendTimeDecision.suspendTime = 0;
    //   }
    //   this.takenDecision = {};
    //   this.storage.get('Authorization').then((Authorization) => {
    //     this.task.getDecisionTransferRole(dec.transfer_role_name, Authorization)
    //       .subscribe((dtrn) => {
    //         if(dtrn.length != undefined){
    //           if (dec.transfer_role_name == "UW") {
    //             for (var i = dtrn.length - 1; i >= 0; i--) {
    //               let trRN = dtrn[i].role_distribution.substr(0, 7);
    //               let trnTemp = this.transferRoleNameOption.filter((itr)=>{return itr == trRN});
    //               if (trnTemp.length <= 0) {
    //                 this.transferRoleNameOption.push(trRN);
    //               }
    //             }
    //             if (r) {
    //               let temp = dtrn.filter(function( obj ) {
    //                 return obj.role_distribution.substr(0, 7) == r;
    //               });
    //               if (dec.decision_desc == 'Transfer to UW Non Reguler') {
    //                 temp = temp.filter(function( obj ) {
    //                   let length = obj.role_distribution.length-2;
    //                   return obj.role_distribution.substr(length, 2) == '_2';
    //                 });
    //               }else if(dec.decision_desc == 'Transfer to UW Regular') {
    //                 temp = temp.filter(function( obj ) {
    //                   let length = obj.role_distribution.length-2;
    //                   return obj.role_distribution.substr(length, 2) != '_2';
    //                 });
    //               }
    //               this.decisionsTransferRoleName = temp;
    //             }else{
    //               this.decisionsTransferRoleName = [];
    //             }
    //           }else{
    //             this.decisionsTransferRoleName = dtrn;
    //           }

    //           // if (e) {
    //             // this.setParentHeight2(el);
    //           // }

    //         }else{
    //           this.decisionsTransferRoleName = [];
    //           // if (e) {
    //             // this.setParentHeight2(el);
    //           // }
    //         }
    //       }, (error) => {
    //         console.log('error: ', error);
    //       });
    //   });
    // }
  }

  takeDecision(e, item){
    if (e.target.checked) {
      this.takenDecision = item;
    }
  }

  minNum(num){
    if (this.suspendTimeDecision.suspendTime <= (num-1)) {
      setTimeout(()=>{
        if (this.suspendTimeDecision.suspendTime != "") {
          this.suspendTimeDecision.suspendTime = num;
        }
      },1);
    }
  }

  createUncoverStatment(index){
    this.newUncoverStatement[index] = [];
    this.newUncoverStatement[index].push({"reasonCode": null,"reasonSeq": null,"param01": null});
  }

  addToUncoverStatement(index, diagnose_index){
    this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index].uncoverStatement.push(this.newUncoverStatement[index][0]);
    this.selectedData.uncoverStatement[index].push([["param01", this.newUncoverStatement[index][0].param01]]);
    this.newUncoverStatement = {};
  }

  removeUncoverStatement(index_benef, diagnose_index, index){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement.splice(index, 1);
          }
        }
      ]
    });
    alert.present();
  }

  addNewTermsOfCoverage(index_benef, diagnose_index, index){
    this.newTermsOfCoverage[index_benef] = [];
    this.newTermsOfCoverage[index_benef][index] = [];
    let objc = this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement[index];
    var result = Object.keys(objc).map(function(key) {
      return [key, objc[key]];
    });
    let o = 1
    for (var i = 0; i < result.length ; i++) {
      if (!(result[i][0] == "seqnumb" || result[i][0] == "reasonSeq" || result[i][0] == "reasonCode")) {
        o++;
      }
    }

    let param_text = "param"+o;
    if (o<=9) {
      param_text = "param0"+o;
    }
    this.newTermsOfCoverage[index_benef][index] = {name: param_text, value: ""}
  }

  addToTermsOfCoverage(index_benef, diagnose_index, index, idx){
    let new_param = this.newTermsOfCoverage[index_benef][index];
    if (new_param.value) {
      this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement[index][new_param.name] = new_param.value;
      this.selectedData.uncoverStatement[idx][index].push([new_param.name, new_param.value])
    }
    this.newTermsOfCoverage = {};
  }

  removeTermsOfCoverage(index_benef, diagnose_index, index, param_name, idx, key_item_index){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let sp = parseInt(param_name.replace("param"));
            delete this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement[index][param_name];
            this.selectedData.uncoverStatement[idx][index].splice(key_item_index, 1);
            let objc = this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement[index];
            var result = Object.keys(objc).map(function(key) {
              return [key, objc[key]];
            });

            for (var i = result.length - 1; i >= 0; i--) {
              if (!(result[i][0] == "seqnumb" || result[i][0] == "reasonSeq" || result[i][0] == "reasonCode")) {
                delete this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement[index][result[i][0]];
              }
            }

            let o = 1
            for (var i = 0; i < result.length; i++) {
              let param_text = "param"+o;
              if (o<=9) {
                param_text = "param0"+o;
              }
              if (!(result[i][0] == "seqnumb" || result[i][0] == "reasonSeq" || result[i][0] == "reasonCode")) {
                this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].uncoverStatement[index][param_text] = result[i][1];
                this.selectedData.uncoverStatement[idx][index][i] = [param_text, result[i][1]];
                o++;
              }
            }
          }
        }
      ]
    });
    alert.present();
  }

  addClauseDetails(e){
    this.selectedData.caseSummary.caseSummary_CaseLog.caseLogList.push({

    });
  }

  deleteClauseDetails(e, index){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.selectedData.caseSummary.caseSummary_CaseLog.caseLogList.splice(index, 1);
            console.log('yes clicked');
          }
        }
      ]
    });
    alert.present();
  }

  numAttrs(obj) {
    var count = 0;
    for(var key in obj) {
      if (obj.hasOwnProperty(key)) {
        ++count;
      }
    }
    return count;
  }

  postCompleteTask(e){
    this.showLoading();
    this.dataReviewButtonDisabled = true;
    this.storage.get('claimTask').then((raw_claimTask) => {
      this.storage.get('jsonReq').then((raw_json) => {
        this.storage.get('encrypted').then((_encrypted) => {
          this.storage.get('username').then((_username) => {
            let a = JSON.stringify(this.claimTask);
            let _claimTask = JSON.parse(a);
            let b = JSON.stringify(this.claimTask.jsonRequest);
            let json_r = JSON.parse(b)

            if(_claimTask){
              let allowed = false;
              this.dataReviewButtonDisabled = false;
              if (this.decisionsTransferRoleName.length >=1) {
                if (this.takenDecision.role_distribution) {
                  allowed = true;
                }
              }else{
                allowed = true;
              }
              if (this.choosedDecision.decision_desc && allowed == true) {
                let _completedTask = _claimTask;
                _completedTask.request = _claimTask.result;

                _completedTask.request[1].data = new Array();

                if (this.decisionMode == 0) {
                  for (var i = 0 ;i < this.policyDecision.length; i++){
                    _completedTask.request[1].data[i] = this.policyDecision[i];
                    json_r.payload[i].claimSummary.decision=this.policyDecision[i].decision;
                    json_r.payload[i].claimSummary.note = this.policyDecision[i].note ? this.policyDecision[i].note : "";
                    json_r.payload[i].claimSummary.time = this.policyDecision[i].time ? this.policyDecision[i].time : "";
                    _completedTask.request[1].data[i].role = this.navParams.data.roleName;
                    json_r.payload[i].claimSummary.role = this.navParams.data.roleName;
                  }
                }else if (this.decisionMode == 1) {
                  for (var i = 0 ;i < this.policyDecision.length; i++){
                    _completedTask.request[1].data[i] = this.policyDecision[i];
                    _completedTask.request[1].data[i].decision = this.singlePolicyDecision.decision;
                    _completedTask.request[1].data[i].note = this.singlePolicyDecision.note ? this.singlePolicyDecision.note : "";
                    _completedTask.request[1].data[i].time = this.singlePolicyDecision.time ? this.singlePolicyDecision.time : "";
                    json_r.payload[i].claimSummary.decision=this.singlePolicyDecision.decision;
                    json_r.payload[i].claimSummary.note = this.singlePolicyDecision.note ? this.singlePolicyDecision.note : "";
                    json_r.payload[i].claimSummary.time = this.singlePolicyDecision.time ? this.singlePolicyDecision.time : "";
                    _completedTask.request[1].data[i].role = this.navParams.data.roleName;
                    json_r.payload[i].claimSummary.role = this.navParams.data.roleName;
                  }
                }

                _completedTask.request[2].data.sourceCallUser = _claimTask.result[2].data.lastActionUser;
                _completedTask.request[2].data.description = this.choosedDecision.desc_in_workitem ? this.choosedDecision.desc_in_workitem : "";
                _completedTask.request[2].data.sourceCallRoleName = _claimTask.result[2].data.roleName;
                _completedTask.request[2].data.sourceCallRoleDistribution = _claimTask.result[2].data.roleDistribution;
                _completedTask.request[2].data.lastActionUser = _username;
                _completedTask.request[2].data.policyNumber = this.selectedTask.policyNumber;
                _completedTask.request[2].data.spajNumber = this.selectedTask.spajNumber;
                _completedTask.request[2].data.roleName = this.choosedDecision.transfer_role_name ? this.choosedDecision.transfer_role_name : "";
                _completedTask.password = _encrypted.password;
                _completedTask.username = _encrypted.username;
                _completedTask.claimUnique = this.selectedTask.claimUnique;
                _completedTask.taskId = this.selectedTask.taskId;
                _completedTask.bpmId = this.selectedTask.instanceId ? this.selectedTask.instanceId : "";

                if (this.choosedDecision.decision_desc.toLowerCase() == 'hold' || this.choosedDecision.decision_desc.toLowerCase() == 'unhold' || this.choosedDecision.decision_desc.toLowerCase() == 'pending' || this.choosedDecision.decision_desc.toLowerCase() == 'suspend' || this.choosedDecision.decision_desc.toLowerCase() == 'release') {
                  _completedTask.request[2].data.roleDistribution = _claimTask.result[2].data.roleDistribution;
                }else{
                  if (this.takenDecision) {
                    _completedTask.request[2].data.roleDistribution = this.takenDecision.role_distribution ? this.takenDecision.role_distribution : "";
                  }
                }

                if (this.choosedDecision.decision_desc == 'Refer to Q UW' || this.choosedDecision.decision_desc == 'Refer to Q Tele UW' || this.choosedDecision.decision_desc == 'Refer to Q PMA'){
                  if (this.decLastActionUser != "") {
                    _completedTask.request[2].data.lastActionUser = this.decLastActionUser;
                  }else{
                    this.showAlert('Please select username.');
                    this.dataReviewButtonDisabled = false;
                    return false;
                  }
                }

                if (json_r == raw_json) {
                  _completedTask.jsonRequest = "";
                }else{
                  _completedTask.jsonRequest = json_r;
                }

                delete _completedTask.result;
                delete _completedTask.jsonRequest.dataMaster;

                this.storage.get('Authorization').then((Authorization) => {
                  this.task.completeTask(JSON.stringify(_completedTask), Authorization)
                  .subscribe((result: any) => {
                    if(result.result == false){
                      var title= 'Take Decision Failed : ' + result.resultDescription;
                    }
                    else{
                      var title = 'Task Completed';
                    }
                    let alert = this.alertCtrl.create({
                      title: title,
                      buttons: [{
                          text: 'OK',
                          handler: () => {
                            this.navCtrl.setRoot('ClaimPage');
                          }
                        }]
                    });
                    alert.present(prompt);
                  }, (error) => {
                    this.showAlert('Network error, Task is not completed');
                  });
                });
              }else{
                this.dataReviewButtonDisabled = false;
                if (!allowed) {
                  this.showAlert('Task is not completed, you have choose decision level 2 first.');
                }else{
                  this.showAlert('Task is not completed, you have choose decision first.');
                }
              }
            }
          });
        });
      });
    });
    this.loading.dismiss();
  }

  showAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  dateToString(e, model){
    if(!e.target){
      if (e.split("-").length == 3) {
        let stringDate = e.replace(/-/g,"");
        let split = model.split('.');
        this[split[0]][split[1]][split[2]] = stringDate;
      }
    }
  }

  initializeItems(arr, index) {
    this.items[index] = [];
    this.items[index] = arr;
  }

  retransformToString(e, model, model_end){
    let _string = e.target.value.toString();
    let segment1 = _string.substr(0,4);
    let segment2 = _string.substr(4,2);
    let segment3 = _string.substr(6,2);
    let d = segment1+'-'+segment2+'-'+segment3;
    if (this.checkIfDateNotValid(d)) {
      model[model_end] = model[model_end]
    }else{
      model[model_end] = e.target.value;
    }
  }

  checkIfDateNotValid(a) {
    try{
        var d = new Date(a);
        return !(d.getTime() === d.getTime()); //NAN is the only type which is not equal to itself.
    }catch (e){
        return true;
    }
  }

  getItems(ev: any, arr, attrName, index, sub_index) {
    this.selectedItems[index] = [];
    let val = ev.target.value;
    if (val) {
      if (val.length >= 3) {
        if (val && val.trim() != '') {

          this.selectedItems[index] = this.items[index].filter((item) => {
            if (index == 1) {
              if (item[attrName] && item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1) {
                return (item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1)
              } else if (item.hospitalCode) {
                return (item.hospitalCode.toLowerCase().indexOf(val.toLowerCase()) > -1)
              }
            } else if (index == 2) {
              if (item[attrName] && item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1) {
                return (item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1)
              } else if (item.descitem) {
                return (item.descitem.toLowerCase().indexOf(val.toLowerCase()) > -1)
              }
            } else if (index == 3) {
              if (item[attrName] && item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1) {
                return (item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1)
              } else if (item.longdesc) {
                return (item.longdesc.toLowerCase().indexOf(val.toLowerCase()) > -1)
              }
            }else if (index == 4) {
              if (item[attrName] && item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1
                  && (item.hospcd == this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.hospitalCode)) {
                return (item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1)
              } else if (item.zdoctcd
                  && (item.hospcd == this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.hospitalCode)) {
                return (item.zdoctcd.toLowerCase().indexOf(val.toLowerCase()) > -1)
              }
            }else if (index == 5) {
              if (item[attrName] && item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1) {
                return (item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1)
              } else if (item.shortdesc) {
                return (item.shortdesc.toLowerCase().indexOf(val.toLowerCase()) > -1)
              }
            } else if (item[attrName]) {
              return (item[attrName].toLowerCase().indexOf(val.toLowerCase()) > -1);
            }
          });
          if (sub_index) {
            this.showList[index] = sub_index;
          }else{
            this.showList[index] = true;
          }          
        }
      } else {
        this.showList[index] = false;
      }
    } else {
      this.showList[index] = false;
    }
  }

  selectItem(e, item, model, attr, index){
    let split = model.split('.');
    if (split.length >= 2) {
      if (split.length == 4) {
        if(split[0] == 'labCode'){
          this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[1]].benefitClaim[split[2]].labCode[split[3]].labcde = item.descitem;
          this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[1]].benefitClaim[split[2]].labCode[split[3]].desc = item.longdesc;
        }else{
          this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]][split[3]] = item.descitem+' - '+item.longdesc;
          this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]]['icdCode'] = item.descitem;
          this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]]['diagnosisType'] = item.longdesc;
        }          
      }else if (split.length == 3){        
        if (split[2] == 'hospitalName') {
          this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.hospitaladdrs = item.hospitalAddrs;
          this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.hospitalcntry = item.hospitalCntry;
          this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.hospitalCode = item.hospitalCode;
          this.claimTask.jsonRequest.payload[this.selectedData.index].generalClaimLifeAsia.networkIndicator = item.networkIndicator;
        }else if(split[0] == 'surgeryType'){
          this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[1]].surgery[split[2]].surgeryType = item.shortdesc+' - '+ item.longdesc;
        }else{
          this.claimTask.jsonRequest.payload[this.selectedData.index][split[1]][split[2]] = item[attr]
        }
      }else if(split.length == 6 && split[4] == 'dokter'){
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]][split[4]]['dokterName'] = item.zdoctcd+' - '+item.zdoctnm;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]][split[4]]['dokterCode'] = item.zdoctcd;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]][split[4]]['dokterSpeciality'] = item.dokterSpeciality;
        // this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[split[2]][split[4]]['dokterLicense'] = item.dokterLicense;
      }
    }else if (split.length == 1){
      this[model] = item[attr]
    }
    this.showList[index] = false;
    // this.longDescDiagnosis = item.longdesc;
    // this.codeDiagnosis = item.id;
  }

  updateCurrency(event, src_model, model){
    let e = event.target.value;
    let get_int = e.split('.');
    let after_comma = '00';
    if (get_int[1] != undefined) {
      after_comma = get_int[1].replace(/\D/g, "");
    }

    let before_comma = get_int[0].replace(/\D/g, "");
    let _float = parseFloat(before_comma+'.'+after_comma).toFixed(2);
    let model_split = model.split('.');
    if (src_model == 'hospitalisation') {
      this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[model_split[1]].benefitClaim[model_split[2]].invoice[model_split[3]][model_split[0]] = 0
      setTimeout(()=>{
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[model_split[1]].benefitClaim[model_split[2]].invoice[model_split[3]][model_split[0]] = _float;

        let z = JSON.stringify(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[model_split[1]]);
        this._benefitClaim[this.selectedData.index] = JSON.parse(z);
      },100, model_split);

      setTimeout(()=>{
        this.countTotalDiagnose(model_split[1]);
      },150, model_split);
    }else{
      src_model[model] = 0
      setTimeout(()=>{
        src_model[model] = _float;
      },100, model_split);
    }
  }

  updateCurrencyAmount(event, currency, diagnose_index, multi_invoice, index_benef, index_invoice){
    this.showLoading();
    if (multi_invoice == 'N') {
      let e = 0;
      if (event.target) {
        e = event.target.value;
      }else{
        e = event;
      }

      if (currency == this.selectedData.hospitalisationDetail.policyCurrency) {
        e = 1;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.rate = 1;
      }

      if (currency) {
        let benefits = this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim;
        if (benefits) {
          for (var i = 0; i < benefits.length ; i++) {
            if (this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].multiInvoiceFlag != 'Y') {
              for (var j = 0; j < this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice.length ; j++) {
                if (this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j]) {
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].benefitAmount = this._benefitClaim[diagnose_index].benefitClaim[i].invoice[j].benefitAmount * e;
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].personalFee = this._benefitClaim[diagnose_index].benefitClaim[i].invoice[j].personalFee * e;
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].claimPaid = this._benefitClaim[diagnose_index].benefitClaim[i].invoice[j].claimPaid * e;
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].claimDecline = this._benefitClaim[diagnose_index].benefitClaim[i].invoice[j].claimDecline * e;
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].amountSubmitted = this._benefitClaim[diagnose_index].benefitClaim[i].invoice[j].amountSubmitted * e;
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].rate = e;
                  this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[i].invoice[j].kurs = currency;
                }
              }
            }
          }
        }
      }
    }else if (multi_invoice == 'Y') {
      let e = this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.rate;

      if (currency == this.selectedData.hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].kurs) {
        e = 1;
      }

      if (this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice]) {
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].benefitAmount = this._benefitClaim[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].benefitAmount * e;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].personalFee = this._benefitClaim[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].personalFee * e;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].claimPaid = this._benefitClaim[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].claimPaid * e;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].claimDecline = this._benefitClaim[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].claimDecline * e;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].amountSubmitted = this._benefitClaim[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].amountSubmitted * e;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].rate = e;
        this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[index_benef].invoice[index_invoice].kurs = currency;
      }
    }

    this.countTotalDiagnose(diagnose_index);
    this.loading.dismiss();
  }

  totalHeaderBenefit(index, diagnose_index){
    setTimeout(()=>{
      this.benefitClaim[index].totalChild = {
        "amountSubmitted": 0,
        "personalFee": 0,
        "claimPaid": 0,
        "claimDecline": 0,
        "qty": 0,
        "benefitAmount": 0
      }
      if (this.benefitClaim[index].child.length >= 1) {
        for (var i = this.benefitClaim[index].child.length - 1; i >= 0; i--) {
          for (var j = this.benefitClaim[index].child[i].invoice.length - 1; j >= 0; j--) {
            this.benefitClaim[index].totalChild.amountSubmitted = (parseFloat(this.benefitClaim[index].totalChild.amountSubmitted) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].child[i].index].invoice[j].amountSubmitted)).toFixed(2);
            this.benefitClaim[index].totalChild.personalFee = (parseFloat(this.benefitClaim[index].totalChild.personalFee) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].child[i].index].invoice[j].personalFee)).toFixed(2);
            this.benefitClaim[index].totalChild.claimPaid = (parseFloat(this.benefitClaim[index].totalChild.claimPaid) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].child[i].index].invoice[j].claimPaid)).toFixed(2);
            this.benefitClaim[index].totalChild.claimDecline = (parseFloat(this.benefitClaim[index].totalChild.claimDecline) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].child[i].index].invoice[j].claimDecline)).toFixed(2);
            this.benefitClaim[index].totalChild.qty = (parseInt(this.benefitClaim[index].totalChild.qty) + parseInt(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].child[i].index].invoice[j].qty));
            this.benefitClaim[index].totalChild.benefitAmount = (parseFloat(this.benefitClaim[index].totalChild.benefitAmount) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].child[i].index].invoice[j].benefitAmount)).toFixed(2);
          }
        }
      }else if (this.benefitClaim[index].multiInvoiceFlag == 'Y'){
        for (var j = this.benefitClaim[index].invoice.length - 1; j >= 0; j--) {
          this.benefitClaim[index].totalChild.amountSubmitted = (parseFloat(this.benefitClaim[index].totalChild.amountSubmitted) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].index].invoice[j].amountSubmitted)).toFixed(2);
          this.benefitClaim[index].totalChild.personalFee = (parseFloat(this.benefitClaim[index].totalChild.personalFee) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].index].invoice[j].personalFee)).toFixed(2);
          this.benefitClaim[index].totalChild.claimPaid = (parseFloat(this.benefitClaim[index].totalChild.claimPaid) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].index].invoice[j].claimPaid)).toFixed(2);
          this.benefitClaim[index].totalChild.claimDecline = (parseFloat(this.benefitClaim[index].totalChild.claimDecline) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].index].invoice[j].claimDecline)).toFixed(2);
          this.benefitClaim[index].totalChild.qty = (parseInt(this.benefitClaim[index].totalChild.qty) + parseInt(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].index].invoice[j].qty));
          this.benefitClaim[index].totalChild.benefitAmount = (parseFloat(this.benefitClaim[index].totalChild.benefitAmount) + parseFloat(this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[this.benefitClaim[index].index].invoice[j].benefitAmount)).toFixed(2);
        }
      }
    },100, index, diagnose_index);
  }

  addDeclineStatement() {
    this.fullDeclineStatement.push('');
  }

  deleteDeclineStatement(index) {
    this.fullDeclineStatement.splice(index, 1);
    this.claimTask.jsonRequest.payload[this.selectedData.index].fullDeclineStatement[0][`uncoverParameterP${(index+1)}`] = '';
    this.claimTask.jsonRequest.payload[this.selectedData.index].fullDeclineStatement[0]
  }

  onDeclineStatementInput(value, index) {
    this.claimTask.jsonRequest.payload[this.selectedData.index].fullDeclineStatement[0][`uncoverParameterP${(index+1)}`] = value;
  }

  customTrackBy(index) {
    return index;
  }

  addLabCode(diagnose_index, benefit_index){
    if (this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[benefit_index].labCode) {
      let length = this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[benefit_index].labCode.length;
      let newLabCode = {
        "seqnum": length+1,
        "dteiss": "",
        "labcde": ""
      }

      this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[benefit_index].labCode.push(newLabCode);
    }
  }

  removeLabCode(diagnose_index, benefit_index, lab_code_index){
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.claimTask.jsonRequest.payload[this.selectedData.index].hospitalisationDetail.diagnose[diagnose_index].benefitClaim[benefit_index].labCode.splice(lab_code_index, 1);
          }
        }
      ]
    });
    alert.present();
  }
}
