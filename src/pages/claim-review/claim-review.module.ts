import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClaimReviewPage } from './claim-review';

import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ClaimReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ClaimReviewPage),
    PipesModule
  ],
})
export class ClaimReviewPageModule {}
