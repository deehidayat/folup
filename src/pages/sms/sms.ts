import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SmsProvider } from '../../providers/sms/sms';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { SelectAgentProvider } from '../../providers/select-agent/select-agent';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { RequestOptions, ResponseContentType, Http } from '@angular/http';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../providers/file-download-helper';
import { URLServices } from '../../providers/url-services';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';


@IonicPage()
@Component({
  selector: 'page-sms',
  templateUrl: 'sms.html',
})
export class SmsPage {

  @ViewChild('fileAgentCodes') fileAgentCodes;

  public sendToModel: any;
  public bodyModel: any;
  public messageSmsModel: any;
  public sendTimeModel: any;
  public success: any;
  public report: any;

  // public agentTypes = ['AG', 'MA', 'AA', 'AD', 'FC', 'MF', 'AS', 'NS', 'NA', 'GAO'];
  public agency = ['AA','AD','AG', 'GAO', 'MA'];
  public fd = ['AS','FC', 'MF', 'NA' , 'NS'];


  public agent: any;
  public selectedAgentTypes: any;
  public officeCodes: any;
  public availableOfficeCodes: any;
  public loading:any;
 

  public todayDate: Date;
  public listError: string[];
  public valid: boolean;
  public limitval = 160;

  toggleButton: boolean;
  public isSms: boolean = true;
  public minute = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];

  public sendTimeLater: any;
  public phoneReceiversModel: any;
  public agentTypeModel: any;
  public nameModel: any;
  public phoneNumber: any;
  public sendtos: any;
  public username: string;
  tempSendTime:any;
  public currentDate = new Date();
  public nowDate = new Date(this.currentDate.setHours(this.currentDate.getHours()+7)).toISOString();
  public maxDate = new Date(this.currentDate.setHours(this.currentDate.getHours()+239)).toISOString();

  //limit character
  maxlength=160;
  characterleft=this.maxlength;
  toggleButton2 = false;
  searchOption:any;
  actionOption: string= 'now';
  actionOption1: string= 'manual';

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    private smsProvider: SmsProvider, public storage: Storage, public toastCtrl: ToastController, public agentProvider:
    SelectAgentProvider, public loadingCtrl: LoadingController, public urlServices:URLServices, private http:Http,
    private alertCtrl:AlertController) {
      this.getOfficeCodes();
      this.searchOption = {manual:false, upload:false};
    
  }

  ionViewDidLoad() {
    this.storage.get('username').then((username) => {
        this.username = username;
      })
    console.log('ionViewDidLoad EmailPage');
    this.agent="";
}

nowTime() {
  this.sendTimeModel = this.nowDate;
}

download() {
  const url = this.urlServices.diskomApi+"/agentapi/downloadTemplate";
  const options = new RequestOptions({responseType: ResponseContentType.Blob });

  // Process the file downloaded
  this.http.get(url, options).subscribe(res => {
      const fileName = getFileNameFromResponseContentDisposition(res);
      saveFile(res.blob(), fileName);
  });
}

  //fungsi buat buka select-agent dari sms

  //fungsi buat post sms
  postSms() {
    //validasi tanggal
   //validasi kalo subjek/body. Tanggal kosong=send now, Tanggal
  if (this.bodyModel == '' || this.bodyModel == null) {
    this.showToast('Please fill body message');
    return;
  }

  if (this.actionOption == "now") {
    this.sendTimeModel = this.todayDate;
  }

  if (this.actionOption == null) {
    this.showToast('Please choose time to send');
    return;   
  }

  if(this.actionOption == "now" && this.sendTimeModel != null){
    this.todayDate = new Date(this.sendTimeModel);
    this.sendTimeLater = this.todayDate.setHours(this.todayDate.getHours()-7);
    this.sendTimeModel = this.sendTimeLater;
  }
  if (this.actionOption == "later" && this.sendTimeModel != null) {
    this.tempSendTime = new Date(this.sendTimeModel);
    this.sendTimeModel = this.tempSendTime.setHours(this.tempSendTime.getHours()-7);
  }

  else if(this.actionOption == 'later' && this.sendTimeModel == null){
    this.showToast('Please Fill Date');
    return;
  }

  let alert = this.alertCtrl.create({
    title: 'Send SMS',
    message: '"Are you sure you want to send this SMS?"',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel');
        }
      }, {
        text: 'Yes',
        handler: () => {
          const formData = new FormData();

          if ( this.fileAgentCodes ) {
            let fileAgentCodes = this.fileAgentCodes.nativeElement;
            const formDataAgent = new FormData();
            if (fileAgentCodes.files && fileAgentCodes.files[0]) {
                formData.append("agentcodes", fileAgentCodes.files[0]);
            };
          }
      
          let body = {
            body: this.bodyModel,
            sendTime: this.sendTimeModel,
            username: this.username,
            filter: {
              agentTypes: this.selectedAgentTypes,
              officeCodes: this.officeCodes,
              agent: this.agent
            }
          }
          formData.append('sms', JSON.stringify(body));
          //sms rest sukses muncul toaster succes, err muncul pesan error
      
          this.showLoading();
          this.storage.get('Authorization').then((Authorization) => {
            this.smsProvider.createSms(formData, Authorization)
              .then((sms:[any]) => {
                this.showToast("Creating SMS Success");
                this.loading.dismiss();
                this.navCtrl.popTo('SmsListPage')
              }, (error) => {
                this.showToast(error.message);
                this.loading.dismiss();
              });
        })
        }
        }
      ]
    })
    alert.present();
}

openAccordionBody(e){
  let target = e.currentTarget ? e.currentTarget : e.target;
  let element = target.nextElementSibling;
  let height = element.children[0].offsetHeight;
  if (height > 1000){element.setAttribute("transition-duration", "1000");}
  else{element.setAttribute("transition-duration", "300");}
  if (element.hasAttribute('style')){
    element.removeAttribute("style");
    target.children[0].removeAttribute('style');
    target.children[1].setAttribute("style", "display:none");
  }else{
    element.setAttribute("style", "height: "+height+"px;");
    target.children[1].removeAttribute('style');
    target.children[0].setAttribute("style", "display:none");
  }     
}

//accordion function

openChildAccordion(e){
  let element = e.currentTarget.nextElementSibling;
  let height = element.children[0].offsetHeight;
  let parentAcc = element.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
  let parentAccHeight = parentAcc.offsetHeight;

  if (parentAcc.className != 'accordion-body'){
    let element = e.target;
    while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
      parentAcc = element.parentElement;
    }
    parentAccHeight = parentAcc.offsetHeight;
  }

  e.stopPropagation();
  if (height > 1000){element.setAttribute("transition-duration", "1000");}
  else{element.setAttribute("transition-duration", "300");}
  if (element.hasAttribute('style')){
    element.removeAttribute("style");
    parentAcc.setAttribute("style", "height:"+(parentAccHeight-height)+"px");
    e.currentTarget.children[0].removeAttribute('style');
    e.currentTarget.children[1].setAttribute("style", "display:none");
  } else {
    element.setAttribute("style", "height:"+height+"px;");
    parentAcc.setAttribute("style", "height:"+(height+parentAccHeight)+"px");
    e.currentTarget.children[1].removeAttribute('style');
    e.currentTarget.children[0].setAttribute("style", "display:none");
    }
  }

  setParentHeight(e){
    let parent = e.currentTarget ? e.currentTarget : e.target;
    if (parent.className != 'accordion-body'){
      let element = e.target;
      while((element = element.parentElement) && !element.classList.contains('accordion-body')) {
        parent = element.parentElement;
      }
    }
    setTimeout(()=>{
      let contentHeight = parent.children[0].offsetHeight;
      parent.setAttribute("style", "height:"+contentHeight+"px");
    },100);
  }
//end accordion function


  _stopPropagation(e){
    e.stopPropagation();
  }

  accordionLoaded(e):boolean{
    let target = e.currentTarget ? e.currentTarget : e.target;
    let element = target.parentElement;
    let loaded = element.classList.contains('loaded');
    return loaded;
  }

  openSendTo(e) {
    if (!this.accordionLoaded(e)) {
        this.showLoading();
        this.loading.dismiss().then(()=>{this.openAccordionBody(e);});
        e.target.parentElement.classList.add('loaded');
      }else{
        this.openAccordionBody(e);
      }
    }

  getOfficeCodes() {
    this.storage.get('Authorization').then((Authorization) => {
        this.agentProvider.getFilterOfficeCode(Authorization).
        subscribe(data => {
            this.availableOfficeCodes = data;
            console.log(data);
        }, err => {
            return;
        });
    })
  }

  selectAll() {
    this.reset();
    for(let i = 0;i < this.availableOfficeCodes.length;i++)
        this.officeCodes.push(this.availableOfficeCodes[i]);
  }

  reset() {
    this.officeCodes = [];
  }

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true,
        cssClass: 'toast'
    });
    toast.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
}
