import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmsReceiverDetailsPage } from './sms-receiver-details';

@NgModule({
  declarations: [
    SmsReceiverDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SmsReceiverDetailsPage),
  ],
})
export class SmsReceiverDetailsPageModule {}