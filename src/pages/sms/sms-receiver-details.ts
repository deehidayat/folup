import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Storage } from '@ionic/storage';
import { SmsProvider } from '../../providers/sms/sms';
import { InfiniteScroll } from 'ionic-angular/components/infinite-scroll/infinite-scroll';


@IonicPage()
@Component({
  selector: 'page-sms-receiver-details',
  templateUrl: 'sms-receiver-details.html',
})
export class SmsReceiverDetailsPage {

  id :any;
  message:any;
  messageReceivers: any;
  loading:Loading;
  agentInvolved:any;
  page:0;
  size:10;
  lengthData:0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public smsProvider : SmsProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    this.id = navParams.get('id');
    this.agentInvolved = navParams.get('agentInvolved');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmailReceiverDetailsPage');
    this.search();
  }

  search() {
    this.messageReceivers = [];
    this.page = 0;
    this.size = 10;
    this.lengthData = 0;
    this.showLoading();
    this.getMessage();
    this.getMessageReceivers(null);
  }

  getMessageReceivers(infiniteScroll) {
    this.storage.get('Authorization').then((Authorization) => {
      this.smsProvider.getMessageReceivers(this.id, this.page, this.size, Authorization)
      .subscribe(
        result => {
          for (let i = 0; i < result.length; i++) {
            this.messageReceivers.push(result[i]);
          }
          this.lengthData = result.length;
          this.loading.dismiss();
          if(null != infiniteScroll)
          infiniteScroll.complete(); 
        },
        err => {
          this.loading.dismiss();
        }
      )
    })
  }

  getMessage() {
    this.storage.get('Authorization').then((Authorization) => {
        this.smsProvider.getMessageById(this.id, Authorization)
        .subscribe(
        result => {
            this.message = result;
        },
        err => {
            this.showToast(err.message);
        });
    });
}

//infinite scroll
doInfinite(infiniteScroll) {
    this.page++;
    this.getMessageReceivers(infiniteScroll);
}

  showToast(text) {
    let toast = this.toastCtrl.create({
        message: text,
        duration: 15000,
        position: 'top',
        showCloseButton: true,
        cssClass: 'toast'
    });
    toast.present();
}

showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
}
