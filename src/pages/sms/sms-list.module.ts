import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CKEditorModule } from 'ng2-ckeditor';
import { SmsListPage } from './sms-list';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    SmsListPage
  ],
  imports: [
    IonicPageModule.forChild(SmsListPage),    
    CKEditorModule,
    ComponentsModule
  ],
  entryComponents: [
    SmsListPage
  ]
})
export class SmsListPageModule {}
