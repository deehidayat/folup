import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmsPage } from './sms';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    SmsPage,
  ],
  imports: [
    IonicPageModule.forChild(SmsPage),
    SelectSearchableModule
  ],
})
export class SmsPageModule {}
