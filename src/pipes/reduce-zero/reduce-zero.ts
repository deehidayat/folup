import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reduceZero',
})
export class ReduceZeroPipe implements PipeTransform {
  transform(value: string, qty) {
    if (value) {
      let _value = value.toString()
      return _value.slice(0, (-1 * qty));
    }else{
      return value;
    }      
  }
}
