import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ObjToArrPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'objToArr',
})
export class ObjToArrPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    return Object.keys(value).map((key)=>{ return [key, value[key]]});
  }
}