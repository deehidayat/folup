import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], term: string, attr: string): any {
    return items.filter(item => item[attr].indexOf(term) !== -1);
  }
}
