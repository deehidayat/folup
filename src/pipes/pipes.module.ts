import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";

import { SplitDatePipe } from './split-date/split-date';
import { FilterPipe } from './filter/filter';
import { ObjToArrPipe } from './obj-to-arr/obj-to-arr';
import { RemoveCommaPipe } from './remove-comma/remove-comma';
import { ReduceZeroPipe } from './reduce-zero/reduce-zero';
@NgModule({
	declarations: [SplitDatePipe,
    FilterPipe,
    ObjToArrPipe,
    RemoveCommaPipe,
    ReduceZeroPipe],
	imports: [],
	exports: [SplitDatePipe,
    FilterPipe,
    ObjToArrPipe,
    RemoveCommaPipe,
    ReduceZeroPipe]
})
export class PipesModule {}
