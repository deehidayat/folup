import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeComma',
})
export class RemoveCommaPipe implements PipeTransform {

  transform(value: string) {
    let _string = value.toString();
    let get_int = _string.split('.');
    let after_comma = '00';
    if (get_int[1]) {
      after_comma = get_int[1].replace(/\D/g, "");
    }
    let before_comma = get_int[0].replace(/\D/g, "");
    return parseFloat(before_comma+'.'+after_comma).toFixed(2);
  }
}
