import { Pipe, PipeTransform } from '@angular/core'; 
 
@Pipe({ 
    name: 'splitDate', 
    pure: false 
}) 

export class SplitDatePipe implements PipeTransform { 
  transform(string): any { 
    if(string){ 
      if(string.length <= 8){
        let _string = string.toString();
        let segment1 = _string.substr(0,4); 
        let segment2 = _string.substr(4,2); 
        let segment3 = _string.substr(6,2); 
        return segment1+'-'+segment2+'-'+segment3; 
      }else{
        return string;
      }
    }else{ 
      return ""; 
    } 
  } 
}
