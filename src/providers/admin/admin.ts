import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response, ResponseOptions, RequestOptions, Headers } from '@angular/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { URLServices } from '../url-services';

@Injectable()
export class AdminProvider {

  constructor(public http: HttpClient, private urlServices: URLServices) {
    //...
  }

  //start - remote action for get List User
  public getListUser(Authorization) {
    let url = this.urlServices.admin+"/base/user";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end get List User

  //start - remote action for get User Details
  public getUserDetails(Authorization, id) {
    let url = this.urlServices.admin+"/base/user/"+id;
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end get User Details

  private extractData(body: any) {
    return Object.assign(body.data || body);
  }

  private handleError(error: HttpErrorResponse | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let errObj: any;

    if (error instanceof HttpErrorResponse) {
      const err = error.message || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = error.message;
    } else {
      errMsg = error.message ? error.message : error.toString();
      const body = error.message || '';
      errObj = body;
    }

    return Observable.throw(errObj);
  }
}
