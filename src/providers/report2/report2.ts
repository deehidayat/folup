import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../providers/file-download-helper';
import { Http, RequestOptions, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { URLServices } from '../url-services';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class Report2Provider {

    constructor(private httpClient: HttpClient, private http:Http,private urlServices: URLServices) {    
    }

    public getReports2(startDate, endDate, channel, Authorization) {
        let url = this.urlServices.diskomApi+"/report2api" 
			+ "?channel=" + channel + "&startDate=" + startDate + "&endDate=" +endDate;
        
		let headers    = new HttpHeaders({ 
           'Content-Type': 'application/json',
           'X-Requested-Url': url,
           'X-Requested-Method': 'GET',
           'Authorization': Authorization
        });
        let options    = { headers: headers };
    
        return this.httpClient.get(this.urlServices.baseProxy, options)
        .map(this.extractData)
        .catch(this.handleError);
		
      }

      // Bentuk baru dari export, open confirmation dialog

      downloadFile(startDate, endDate, channel, Authorization) {
        const url = this.urlServices.diskomApi+"/report2api/export?channel="+ channel +"&startDate="+startDate+"&endDate="+endDate;
        const options = new RequestOptions({responseType: ResponseContentType.Blob });
      
        // Process the file downloaded
        this.http.get(url, options).subscribe(res => {
          const fileName = getFileNameFromResponseContentDisposition(res);
          saveFile(res.blob(), fileName);
        });
        }

	// end
    
      private extractData(body: any) {
        return Object.assign(body.data || body);
      }
    
      private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        let errObj: any;
    
        if (error instanceof HttpErrorResponse) {
          const err = error.message || JSON.stringify(error);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
          errObj = error.message;
        } else {
          errMsg = error.message ? error.message : error.toString();
          const body = error.message || '';
          errObj = body;
        }
    
        return Observable.throw(errObj);
      }
    
}