export const DaysRemains:any[] = [
    { id: 1, field: 'Remaining Room and Board', value: 130 },
    { id: 2, field: 'Remaining ICU', value: 30 },
    { id: 3, field: 'Remaining Physiotheraphy', value: 30 },
];

export const BoardTypes:any[] = [
    { type: 't01', name: 'Type 1', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submit', value: 900000 },
      { label: 'Personal Fee', value: 900000 },
      { label: 'Claim Decline', value: 0 },
      { label: 'Claim Paid', value: 8100000 },
    ] },
    { type: 't02', name: 'Type 2', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submitted', value: 0 },
      { label: 'Personal Fee', value: 0 },
      { label: 'Claim Declined', value: 0 },
      { label: 'Claim Paid', value: 0 },
    ] }    
  ];

  export const Doctors:any[] = [
    { id: 1, title: 'General Doctor', details: [
      { label: 'Inner Limit Benefit', amount: 0 },
      { label: 'Quantity', amount: 5 },
      { label: 'Claim Submitted', amount: 9000000 },
      { label: 'Personal Fee', amount: 5000000 },
      { label: 'Claim Declined', amount: 0 },
      { label: 'Claim Paid', amount: 7000000 },
    ]}
  ];

  export const ICUs:any[] = [
    { id: 1, name: 'ICU', details: [
      { label: 'Inner Limit Benefit', amount: 0 },
      { label: 'Quantity', amount: 5 },
      { label: 'Claim Submitted', amount: 9000000 },
      { label: 'Personal Fee', amount: 5000000 },
      { label: 'Claim Declined', amount: 0 },
      { label: 'Claim Paid', amount: 7000000 },
    ]},
    { id: 2, name: 'NICU', details: [
      { label: 'Inner Limit Benefit', amount: 0 },
      { label: 'Quantity', amount: 5 },
      { label: 'Claim Submitted', amount: 9000000 },
      { label: 'Personal Fee', amount: 5000000 },
      { label: 'Claim Declined', amount: 0 },
      { label: 'Claim Paid', amount: 7000000 },
    ]},    
  ];

  export const Visits:any[] = [
    { id: 1, name: 'Visit 1', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submit', value: 900000 },
      { label: 'Personal Fee', value: 900000 },
      { label: 'Claim Decline', value: 0 },
      { label: 'Claim Paid', value: 8100000 },
    ] }
  ];

  export const PreHospitalisations:any[] = [
    { id: 1, name: 'Pre-hospitalisation 1', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submit', value: 900000 },
      { label: 'Personal Fee', value: 900000 },
      { label: 'Claim Decline', value: 0 },
      { label: 'Claim Paid', value: 8100000 },
    ] }
  ];

  export const Surgeries:any[] = [
    { id: 1, name: 'Minor Surgery', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submit', value: 900000 },
      { label: 'Personal Fee', value: 900000 },
      { label: 'Claim Decline', value: 0 },
      { label: 'Claim Paid', value: 8100000 },
    ] },
    { id: 2, name: 'Major Surgery', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submitted', value: 0 },
      { label: 'Personal Fee', value: 0 },
      { label: 'Claim Declined', value: 0 },
      { label: 'Claim Paid', value: 0 },
    ] }    
  ];

  export const Miscs:any[] = [
    { id: 1, name: 'Total expenses 1', 
    details: [
      { label: 'Inner Limit Benefit', value: 0 },
      { label: 'Quantity', value: 5 },
      { label: 'Claim Submit', value: 900000 },
      { label: 'Personal Fee', value: 900000 },
      { label: 'Claim Decline', value: 0 },
      { label: 'Claim Paid', value: 8100000 } ], 
      expenseDetails: [
      { name: 'Biaya Obat-obatan', amount: 100000 },
      { name: 'Biaya Pemeriksaan Perunjang', amount: 100000 },
      { name: 'Biaya Test Lab', amount: 100000 },
      { name: 'Biaya Aneka Perawatan Lainnyo', amount: 100000 }] 
    }
  ]