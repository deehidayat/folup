export const HistoryOfUnderwriting:any[] = [
    {
        id:1, 
        items: [
            { col:1, field: 'Decision Code', value: 'A/T'}, 
            { col:2, field: 'Reminder Date', value: '13 Jan 2016'}, 
            { col:3, field: 'Remark', value: 'Winter is coming'}, 
            { col:4, field: 'User Profile', value: 'House Stark'},
        ]
    },
    
    { 
        id:2,
        items: [
            { col: 1, field: 'Decision Code', value:'ABC'},
            { col: 2, field: 'Reminder Date', value: '12 Feb 2016'}, 
            { col:3, field: 'Remark', value: 'Fire and blood'}, 
            { col:4, field: 'User Profile', value: 'House Targaryen'}
        ]
    },
    { 
        id:3,
        items: [
            { col: 1, field: 'Decision Code', value:'DEF'},
            { col: 2, field: 'Reminder Date', value: '11 Mar 2016'}, 
            { col:3, field: 'Remark', value: 'Hear me roar'}, 
            { col:4, field: 'User Profile', value: 'House Lannister'}
        ]
    }, 
    { 
        id:4,
        items: [
            { col: 1, field: 'Decision Code', value:'GHI'},
            { col: 2, field: 'Reminder Date', value: '14 Apr 2016'}, 
            { col:3, field: 'Remark', value: 'We do not sow'}, 
            { col:4, field: 'User Profile', value: 'House Greyjoy'}
        ]
    }       
];

export const AlterationProcess:any[] = [
    {
        id:1,
        items: [
            { col:1, field: 'Transaction No.', value: '05'},
            { col:2, field: 'Rider Code', value: 'H1GR'},
            { col:3, field: 'Plan', value: 'B'},
            { col:4, field: 'Start Date', value: '13 Jan 2015'},
            { col:5, field: 'End Date', value: '13 Jan 2016'},
            { col:6, field: 'Prorate Flag', value: 'N'},
        ]
    },
    {
        id:2,
        items: [
            { col:1, field: 'Transaction No.', value: '23'},
            { col:2, field: 'Rider Code', value: 'H1TR'},
            { col:3, field: 'Plan', value: 'A'},
            { col:4, field: 'Start Date', value: '13 Feb 2016'},
            { col:5, field: 'End Date', value: '25 Feb 2016'},
            { col:6, field: 'Prorate Flag', value: 'Y'},
        ]
    },    
    {
        id:3,
        items: [
            { col:1, field: 'Transaction No.', value: '05'},
            { col:2, field: 'Rider Code', value: 'H1GR'},
            { col:3, field: 'Plan', value: 'B'},
            { col:4, field: 'Start Date', value: '13 Jan 2015'},
            { col:5, field: 'End Date', value: '13 Jan 2016'},
            { col:6, field: 'Prorate Flag', value: 'N'},
        ]
    },    
];

export const AnotherRiderCode:any[] = [
    {
        id:1,
        items: [
            { col:1, field: 'Rider Code', value: 'H1GR' },
            { col:2, field: 'RCD Rider', value: '13 Jan 2016' },
            { col:3, field: 'Rider Description', value: 'PRUlhospital and Surgery Plus' },
            { col:2, field: 'Rider Status', value: 'Exclude' },
        ]
    },
    {
        id:2,
        items: [
            { col:1, field: 'Rider Code', value: 'H1BR' },
            { col:2, field: 'RCD Rider', value: '13 Jan 2016' },
            { col:3, field: 'Rider Description', value: 'PRUmed' },
            { col:2, field: 'Rider Status', value: 'In Force' },
        ]
    },  
    {
        id:3,
        items: [
            { col:1, field: 'Rider Code', value: 'CLR' },
            { col:2, field: 'RCD Rider', value: '13 Jan 2016' },
            { col:3, field: 'Rider Description', value: 'PRUcrisis cover benefit 34' },
            { col:2, field: 'Rider Status', value: 'In Force' },
        ]
    },        
];

export const RiderOnClientLevel:any[] = [
    {
        id:1,
        items: [
            { col:1, field: 'Policy No.', value: '11685211' },
            { col:2, field: 'Rider Code', value: 'H1GR' },
            { col:3, field: 'Rider Description.', value: 'PRUlhospital and Surgery Plus' },
            { col:4, field: 'RCD Rider.', value: '13 Jan 2016' },
            { col:5, field: 'Rider Status', value: 'Exclude' },
        ]
    },
    {
        id:2,
        items: [
            { col:1, field: 'Policy No.', value: '52614671' },
            { col:2, field: 'Rider Code', value: 'H1BR' },
            { col:3, field: 'Rider Description.', value: 'PRUmed' },
            { col:4, field: 'RCD Rider.', value: '13 Jan 2016' },
            { col:5, field: 'Rider Status', value: 'In Force' },
        ]
    }, 
    {
        id:3,
        items: [
            { col:1, field: 'Policy No.', value: '74823758' },
            { col:2, field: 'Rider Code', value: 'C1LR' },
            { col:3, field: 'Rider Description.', value: 'PRUcrisis cover benefit 34' },
            { col:4, field: 'RCD Rider.', value: '13 Jan 2016' },
            { col:5, field: 'Rider Status', value: 'In Force' },
        ]
    },        
]

