import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response, ResponseOptions, RequestOptions, Headers } from '@angular/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { URLServices } from '../url-services';

export class User {
  name: string;
  email: string;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}
 
@Injectable()
export class UserProvider {

  currentUser: User;
  role: string;

  _isLoggedin: boolean = false;
  roleDistributionList: any[] = [];

  constructor(public http: HttpClient, public storage: Storage, public urlServices: URLServices) {
  }

  public login(credentials): Observable<any> {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point make a request to your backend to make a real check!
      let access = (credentials.password === "pass" && credentials.email === "email");
      let body:any = JSON.stringify(credentials);
      let url = this.urlServices.auth+"/base/api/login";
      let response:any;
      let headers    = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options    = { headers: headers };

      return this.http.post(url, body, options)
      .map(this.extractData)
      .catch(this.handleError);
    }
  }

  public encrypt(username, password, Authorization): Observable<any> {
    let url = this.urlServices.api+"/nb-bpm-services/encrypt/"+username+"/"+password;
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  
  public listRoleDistributionUserByUsername(username, Authorization) {
    let body:any = {"username": username};
    let url = this.urlServices.api+"/nb-config-services/listRoleDistributionUserByUsername";
    let response:any;
    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
 
  public getUserInfo() : User {
    return this.currentUser;
  }
 
  public logout() {
    this.storage.set('isLoggedin', false);
    this._isLoggedin = false;
    this.storage.clear();
  }

  loggedIn() : boolean {
    if (this._isLoggedin) {
      return true;
    }
    return false;
  }


  private extractData(body: any) {
    return Object.assign(body);
  }

  public getMenu(Authorization): Observable<any> {
    let body:any = {};
    let url = this.urlServices.auth+"/base/menu/getListWithUnlimitedDepth";
    let response:any;
    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(url, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  //Claim mandatory
  public getMandatoryField(Authorization): Observable<any> {
    let body:any = {
        "mandatory":"true",
        "menu_Id":"346650"
    };
    let url = "http://10.170.49.104/base/menu/getListWithUnlimitedDepth";
    let response:any;
    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(url, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  //end

  private handleError(error: HttpErrorResponse | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let errObj: any;

    if (error instanceof HttpErrorResponse) {
      const err = error.message || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = error.message;
    } else {
      errMsg = error.message ? error.message : error.toString();
      const body = error.message || '';
      errObj = body;
    }

    return Observable.throw(errObj);
  }
}