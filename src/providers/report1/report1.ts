import { Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { URLServices} from '../url-services'

@Injectable()
export class Report1Provider {

    constructor(private http: HttpClient, private urlServices: URLServices) {    
    }

    //rest untuk get report failed: belum selesai
    getReportsFailed(startDate, endDate, typeOfChannel, Authorization) {
        let url = this.urlServices.diskomApi+"/report1api"+"?startDate=" + startDate + "&endDate="
          + endDate + "&typeOfChannel=" + typeOfChannel;

        let headers    = new HttpHeaders({ 
          'Content-Type': 'application/json',
          'X-Requested-Url': url,
          'X-Requested-Method': 'GET',
          'Authorization': Authorization
        });
        let options    = { headers: headers };
    
        return this.http.get(this.urlServices.baseProxy, options)
        .map(this.extractData)
        .catch(this.handleError); 
      }

      private extractData(body: any) {
        return Object.assign(body.data || body);
      }
    
      private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        let errObj: any;
    
        if (error instanceof HttpErrorResponse) {
          const err = error.message || JSON.stringify(error);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
          errObj = error.message;
        } else {
          errMsg = error.message ? error.message : error.toString();
          const body = error.message || '';
          errObj = body;
        }
    
        return Observable.throw(errObj);
      }
}