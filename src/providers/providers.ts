import { MainMenuProvider } from './main-menu/main-menu';
import { MasterDataProvider } from './master-data/master-data';
import { TaskProvider } from './task/task';
import { UserProvider } from './user/user';
import { AdminProvider } from './admin/admin';

export {
    MainMenuProvider,
    MasterDataProvider,
    TaskProvider,
    UserProvider,
    AdminProvider
};