import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, ResponseOptions ,RequestOptions, Headers, Response } from '@angular/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { URLServices } from '../url-services';

@Injectable()
export class PruforceProvider {

	constructor(public http: Http ,public httpClient: HttpClient, public urlServices: URLServices) {
	}

	// start - 
	public createPru(formData, Authorization) {
		let body:any = formData;
		let url = this.urlServices.diskomApi + "/pruforcenotificationapi";
		let response:any;

		return new Promise((resolve, reject) => {
			this.http.post(url, formData)
			  .map((res: Response) => res.json())
			  .catch((err: Response) => {
				reject((err.json()));
				return Observable.throw(err);
			}).subscribe(data => { resolve(data) })
		});

	}
	// end
  
	// start - get Pruforce Notification based on filter
	public getFilterPruforceNotification(body, startDate, endDate, page, size, Authorization) {
		let url = this.urlServices.diskomApi+"/pruforcenotificationapi/findByBodyAndSendDateBetween" + "?body=" + body
			+ "&startDate=" + startDate + "&endDate=" + endDate + "&page=" + page + "&size=" + size;
		let response:any;

		let headers    = new HttpHeaders({ 
		  'Content-Type': 'application/json',
		  'X-Requested-Url': url,
		  'X-Requested-Method': 'GET',
		  'Authorization': Authorization
		});
		let options    = { headers: headers };

		return this.httpClient.get(this.urlServices.baseProxy, options)
		.map(this.extractData)
		.catch(this.handleError);
	}
	// end - get sms based on filter

	public getPruforceById(id, Authorization) {
		let url = this.urlServices.diskomApi + "/pruforcenotificationapi" + "?id=" + id;
	
		let headers = new HttpHeaders({
		  'Content-Type' : 'application/json',
		  'X-Requested-Url': url,
		  'X-Requested-Method': 'GET',
		  'Authorization': Authorization
		});
		let options = { headers:headers };
	
		return this.httpClient.get(this.urlServices.baseProxy, options)
		.map(this.extractData)
		.catch(this.handleError);
	  }

	  public getReceivers(id, page, size, Authorization) {
		let url = this.urlServices.diskomApi + "/pruforcenotificationapi/findReceiver" + "?id=" + id + "&page=" + page + "&size=" + size;
	
		let headers = new HttpHeaders({
		  'Content-Type' : 'application/json',
		  'X-Requested-Url': url,
		  'X-Requested-Method': 'GET',
		  'Authorization': Authorization
		});
		let options = { headers:headers };
	
		return this.httpClient.get(this.urlServices.baseProxy, options)
		.map(this.extractData)
		.catch(this.handleError);
	  }

	private extractData(body: any) {
		return Object.assign(body.data || body);
	}

	private handleError(error: HttpErrorResponse | any) {
		// In a real world app, we might use a remote logging infrastructure
		let errMsg: string;
		let errObj: any;

		if (error instanceof HttpErrorResponse) {
			const err = error.message || JSON.stringify(error);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
			errObj = error.message;
		} else {
			errMsg = error.message ? error.message : error.toString();
			const body = error.message || '';
			errObj = body;
		}

		return Observable.throw(errObj);
	}
}
