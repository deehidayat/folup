export class URLServices {

  api: string = "http://10.170.49.199";

  apiary: string = "https://10.170.49.199/analisverifier/rest/api/";

  doc: string = "https://microservices-uat.pru.intranet.asia";
  
  baseProxy = "https://microservices-uat.pru.intranet.asia/base/proxy";

  auth = "https://microservices-uat.pru.intranet.asia/";

  admin = "https://microservices-uat.pru.intranet.asia/";
  
  diskomApi = "https://10.170.49.103/distribution-communication-system";

  brms = "https://10.170.49.199";

  constructor(){
  }
}
