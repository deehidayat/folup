import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { URLServices } from '../url-services'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable()
export class SelectAgentProvider {

    constructor(private http: HttpClient, public urlServices: URLServices, public storage: Storage) {    
    }

    //rest untuk get officeCode
    getFilterOfficeCode(Authorization){
        const url = this.urlServices.diskomApi + "/agentapi/finddistinctofficecodes";
        let headers    = new HttpHeaders({ 
            'Content-Type': 'application/json',
            'X-Requested-Url': url,
            'X-Requested-Method': 'GET',
            'Authorization': Authorization
          });
          let options    = { headers: headers };
      
          return this.http.get(this.urlServices.baseProxy, options)
          .map(this.extractData)
          .catch(this.handleError);
        }

    // rest untuk get agents dan bisa filter
    getAgents(agents, agentTypes, officeCodes, pageNumbers, Authorization) {
        const url = this.urlServices.diskomApi + "/agentapi/findbyagenttypesofficecodesandagent" + "?agents=" +
        agents + "&agentTypes=" + agentTypes + "&officeCodes=" + officeCodes + "&pageNumbers=" + pageNumbers;
        let headers    = new HttpHeaders({ 
            'Content-Type': 'application/json',
            'X-Requested-Url': url,
            'X-Requested-Method': 'GET',
            'Authorization': Authorization
          });
          let options    = { headers: headers };
      
          return this.http.get(this.urlServices.baseProxy, options)
          .map(this.extractData)
          .catch(this.handleError);
    }
        
    private extractData(body: any) {
        return Object.assign(body.data || body);
      }
      
    private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        let errObj: any;
    
        if (error instanceof HttpErrorResponse) {
          const err = error.message || JSON.stringify(error);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
          errObj = error.message;
        } else {
          errMsg = error.message ? error.message : error.toString();
          const body = error.message || '';
          errObj = body;
        }
    
        return Observable.throw(errObj);
      }


}