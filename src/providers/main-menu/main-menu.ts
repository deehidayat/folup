import { Injectable } from '@angular/core';

@Injectable()
export class MainMenuProvider {

  public activePage: string = 'Login';

  public activeChildPage: string = 'User Action';

  public taskDueCount: number;
  
  public generalCaseCount: number;

  public username: string = '';

  public mainNav: any[] = [];

  public selectedMenu: number;

  public menuListPerPage: number = 5;

  public userActionIndex: number = 0;
  
  constructor(){
    this.mainMenuInit();
  }

  mainMenuInit(){
    this.taskDueCount = 0;
    this.generalCaseCount = 0;  
    this.loadMenuList();
  }

  openMenu(bool){
    let body = document.getElementsByTagName('body')[0];
    if(bool){
      body.classList.add('open-sub-menu');
    }else{
      body.classList.remove('open-sub-menu');
    }
    
  }

  loadMenuList(){
    let menuList = [  
      { name: 'User Action', component: '', icon: 'paper', checked: false, children: []}
    ];
    this.mainNav = menuList;
  }
}