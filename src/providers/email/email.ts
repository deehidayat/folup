import { HttpHeaders, HttpErrorResponse, HttpClient } from "@angular/common/http";
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';

import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { URLServices } from '../url-services';


@Injectable()
export class EmailProvider {

  constructor(public http: Http, public httpClient: HttpClient, public storage: Storage, public urlServices: URLServices) {
  }

  public sendEmail(formData, Authorization) {
    let url = this.urlServices.diskomApi + "/emailapi";
    let body:any = formData;   
    let response:any;

    return new Promise((resolve, reject) => {
      this.http.post(url, formData)
        .map((res: Response) => res.json())
        .catch((err: Response) => {
          reject((err.json()));
          return Observable.throw(err);
        }).subscribe(data => { resolve(data) })
    });
  }

  public getFilterEmail(subject, startDate, endDate, page, size, Authorization) {
    let url = this.urlServices.diskomApi+"/emailapi/findBySubjectAndSendDateBetween" + "?subject=" + subject
        + "&startDate=" + startDate + "&endDate=" + endDate + "&page=" + page + "&size=" + size ;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.httpClient.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public resendEmail(id, username, Authorization) {
    let url = this.urlServices.diskomApi + "/emailapi" + "?id=" + id + "&username=" + username;

    let body = {
      id : id
    };

    let headers = new HttpHeaders({
      'Content-Type' : 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'PUT',
      'Authorization': Authorization
    });

    let options = { headers:headers };

    return this.httpClient.put(this.urlServices.baseProxy,body , options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public getEmailById(id, Authorization) {
    let url = this.urlServices.diskomApi + "/emailapi" + "?id=" + id;

    let headers = new HttpHeaders({
      'Content-Type' : 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options = { headers:headers };

    return this.httpClient.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public getReceivers(id, page, size, Authorization) {
    let url = this.urlServices.diskomApi + "/emailapi/findReceiver" + "?id=" + id + "&page=" + page + "&size=" + size;

    let headers = new HttpHeaders({
      'Content-Type' : 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options = { headers:headers };

    return this.httpClient.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }


  private extractData(body: any) {
    return Object.assign(body.data || body);
  }

  private handleError(error: HttpErrorResponse | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let errObj: any;

    if (error instanceof HttpErrorResponse) {
      const err = error.message || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = error.message;
    } else {
      errMsg = error.message ? error.message : error.toString();
      const body = error.message || '';
      errObj = body;
    }

    return Observable.throw(errObj);
  }
}