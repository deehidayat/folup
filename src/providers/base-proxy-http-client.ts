import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response, ResponseOptions, RequestOptions, Headers } from '@angular/http';
import { HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { URLServices } from './url-services';

@Injectable()
export class BaseProxyHttpClient {

  constructor(private http: HttpClient, private storage: Storage, private urlServices: URLServices) {
  }

  public execute(method: string, url: string, headers: {}, body: {}, successCallback, errorCallback) {

    let _url = this.urlServices.baseProxy;
    let _body = JSON.stringify(body);

    let _headers = new HttpHeaders(headers);
    _headers = _headers.set("X-Requested-Url", url);
    _headers = _headers.set("X-Requested-Method", method);

    let _authorization = null;

    this.storage.get('Authorization').then((Authorization) => {
      _authorization = Authorization;

      _headers = _headers.set("Authorization", _authorization);
      let _options    = { headers: _headers };

      let observable = this.http.post(_url, _body, _options);
      observable.subscribe(
        (data) => {
          successCallback(data);
        },
        (error) => {
          console.error(error);
          errorCallback(error);
        }
      );

    }, (error) => {
      console.error(error);
    });
  }

}
