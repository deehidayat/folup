import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response, ResponseOptions ,RequestOptions, Headers } from '@angular/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { URLServices } from '../url-services';

@Injectable()
export class MasterDataProvider {

  constructor(public http: HttpClient, public storage: Storage, public urlServices: URLServices) {
  }

  //start - remote action for get master sex name
  public getMasterSex(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterSex";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Marital Status
  public getMasterMaritalStatus(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterMaritalStatus";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Master Religion
  public getMasterReligion(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterReligion";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Country Codes
  public getMasterCountryCodes(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterCountryCodes";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Country
  public getMasterCountry(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterCountry";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Education
  public getMasterEducation(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterEducation";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Occupation
  public getMasterOccupation(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterOccupation";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Province
  public getMasterProvince(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterProvince";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Fund
  public getMasterFund(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterFund";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for Evidence Description
  public getEvidenceDescription(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getEvidenceDescription";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get master Product Code
  public getMasterProductCodes(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterProductCodes";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get Master Frequencies
  public getMasterFrequencies(Authorization) {
    let url = this.urlServices.api+"/master-data-services/getMasterFrequencies";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  private extractData(body: any) {
    return Object.assign(body);
  }
  
  private handleError(error: HttpErrorResponse | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let errObj: any;

    if (error instanceof HttpErrorResponse) {
      const err = error.message || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = error.message;
    } else {
      errMsg = error.message ? error.message : error.toString();
      const body = error.message || '';
      errObj = body;
    }

    return Observable.throw(errObj);
  }

}