import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response, ResponseOptions ,RequestOptions, Headers } from '@angular/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { URLServices } from '../url-services';

@Injectable()
export class TaskProvider {

  constructor(public http: HttpClient, public storage: Storage, public urlServices: URLServices) {
  }

  // start - remote action for get available tasks
  public getAvailableTasks(encrypted, Authorization): Observable<any[]> {
    let body:any = encrypted;
    let url = this.urlServices.api+"/nb-bpm-services/cmListWorkOnTask";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });

    let options    = { headers: headers };
    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end - remote action for get available tasks

  // start - remote action for get available tasks
  public getClaimTask(_encrypted, Authorization): Observable<any[]> {
    let body:any = _encrypted;
    let url = this.urlServices.api+"/nb-bpm-services/claimTask";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end - remote action for get available tasks

  //start - remote action for get remarks
  public getRemarks(spajNumber, Authorization) {
    let body:any = { "prop_no": spajNumber };
    let url = this.urlServices.api+"/nb-bpm-services/getLogBpmRemarkByPropNo";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get Case History
  public getCaseHistory(spajNumber, Authorization) {
    let body:any = { "propNo":spajNumber };
    let url = this.urlServices.api+"/nb-bpm-log-services/getLogBpmTrackByPropNo";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

//start - remote action for get Evidence Code
  public getEvidenceCode(_instance_id, Authorization) {
    let body:any = { "instance_id": _instance_id, "is_active": "1" };
    let url = this.urlServices.api+"/nb-data-services/getBrmsLogEvidence";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for create Remark Note
  public createBpmRemark(_prop_no, _policy_no, note, _instance_id, _task_id, Authorization) {
    let body:any = {
                "propNo":_prop_no,
                "policyNo":_policy_no,
                "remarkType":"user",
                "remarkNote": note,
                "bpmId": _instance_id,
                "taskId":_task_id
              };
    let url = this.urlServices.api+"/nb-bpm-log-services/createLogBpmRemark";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get decision
  public getDecisions(role_name, activity, Authorization) {
    let body:any = { "role_name": role_name, "activity": activity};
    let url = this.urlServices.api+"/nb-bpm-log-services/getBpmDecision";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get decision
  public getDecisionTransferRole(trn, Authorization) {
    let body:any = { "transfer_role_name" : trn };
    let url = this.urlServices.api+"/nb-bpm-log-services/getBpmDecisionTransferRole";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get decision
  public getDocuments(spajNumber, Authorization) {
    console.log('spaj number for docs', spajNumber);
    let body:any = {
                      "transactionId" : "161020171510move-it",
                      "transactionTime" : "16/10/2017 15:10:20",
                      "channelId" : "move-it",
                      "signatureString" : "0e67281e6d870c0f9575040763dcff1740d5399b",
                      "itemTypes" : [ "POLDOCTEMP", "POLDOC", "LOOSEMAILS", "NBBillDoc" ],
                      "params" : [{
                        "type" : "SPAJ_Number",
                        "value" : spajNumber
                      }],
                      "processCode" : "retrieveFiles"
                    };
    let url = this.urlServices.doc+"/cxf/cm-services/retrieve_list_file";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for complete task
  public completeTask(json, Authorization) {
    let body:any = json;
    let url = this.urlServices.api+"/nb-bpm-services/cmCompleteTask";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get uw scoring
  public getUwScoring(json, Authorization) {
    let body:any = json;
    let url = this.urlServices.api+"/nb-services/lifeasia/getUWScoring";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for search task
  public getLogBpmTrackByStatus(srchOpt, Authorization) {
    let body:any = srchOpt;
    let url = this.urlServices.api+"/nb-bpm-services/getLogBpmTrackByStatus";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for transfer case
  public transferCaseByBpmId(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-bpm-services/transferCaseByBpmId";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for or suspend task
  public getLogBpmSuspend(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-bpm-services/getLogBpmSuspend";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for or update log suspend
  public updateLogBpmSuspend(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-bpm-services/updateLogBpmSuspend";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for or retrigger
  public retriggerWF(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-bpm-services/retriggerWF";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get list user
  public listUsers(Authorization) {
    let url = this.urlServices.api+"/nb-config-services/listUsers";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  public getCMInfoAndValidationByPropNo(spajNumber, itemType, Authorization) {
    let body:any = {
      "spajNumber": spajNumber,
      "itemType": itemType
    };
    let url = this.urlServices.api+"/nb-cm-services/getCMInfoAndValidationByPropNo";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  public updateCmInfoById(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-cm-services/updateCmInfoById";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  //start - remote action for get Policy Inquiry New
  public getPolicyClientInquiry(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-services/lifeasia/getPolicyClientInquiry";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for or retrigger
  public administratorTerminate(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-bpm-services/administratorTerminate";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for or update evidence code
  public createUpdateBrmsLogEvidence(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-data-services/createUpdateBrmsLogEvidence";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

 //start - remote action for resume all error case
  public getAutoStartExceptionBlock(Authorization) {
    let url = this.urlServices.api+"/nb-bpm-services/getAutoStartExceptionBlock";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'GET',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.get(this.urlServices.baseProxy, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get follow up
  public getBrmsLogFolup(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-data-services/getBrmsLogFolup";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  //start - remote action for get follow up
  public createUpdateBrmsLogFolup(_body, Authorization) {
    let body:any = _body;
    let url = this.urlServices.api+"/nb-data-services/createUpdateBrmsLogFolup";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST',
      'Authorization': Authorization
    });
    let options    = { headers: headers };

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  // end

  // Claim mock service
  public getVerfierUIData(claimid) {
    let url = this.urlServices.api+"/analisverifier/rest/api/analisverifierui";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST'
    });
    let options    = { headers: headers };
  let body:any = {"claimUnique": claimid};

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);   
  }

  public getClientWorksheet(claimid) {
    let url = this.urlServices.api+"/handler/rest/api/clientHandler";
    let response:any;

    let headers    = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST'
    });
    let options    = { headers: headers };
  let body:any = {"claimUnique": claimid};

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);   
  }

  public getDupWorksheet(claimid) {
    let url = this.urlServices.api+"/handler/rest/api/duplicatehandler";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST'
    });
    let options    = { headers: headers };
  let body:any = {"claimUnique": claimid};

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);   
  }

  public getLooseHandler(claimid) {
    let url = this.urlServices.api+"/handler/rest/api/loosemailHandler";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST'
    });
    let options    = { headers: headers };
  let body:any = {"claimUnique": claimid};

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);   
  }

  public getIncomingHandler(claimid) {
    let url = this.urlServices.api+"/handler/rest/api/incomingHandler";
    let response:any;

    let headers    = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'X-Requested-Url': url,
      'X-Requested-Method': 'POST'
    });
    let options    = { headers: headers };
  let body:any = {"CLAIM_UNIQUE": claimid};

    return this.http.post(this.urlServices.baseProxy, body, options)
    .map(this.extractData)
    .catch(this.handleError);   
  }

  private extractData(body: any) {
    return Object.assign(body);
  }

  private handleError(error: HttpErrorResponse | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let errObj: any;

    if (error instanceof HttpErrorResponse) {
      const err = error.message || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = error.message;
    } else {
      errMsg = error.message ? error.message : error.toString();
      const body = error.message || '';
      errObj = body;
    }

    return Observable.throw(errObj);
  }

}