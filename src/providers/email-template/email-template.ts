import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { URLServices } from '../../providers/url-services';
import { HttpClient, HttpHeaders, HttpHeaderResponse, HttpErrorResponse } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable()
export class EmailTemplateProvider {

  constructor(private http: HttpClient, public urlServices: URLServices, public storage: Storage) {    
  }

  //rest untuk get email dan bisa filter
  getFilterEmails(body, subject, pageNumber, size, Authorization) {
      const url = this.urlServices.diskomApi + "/emailtemplateapi/findbysubjectandbody" + "?body=" + body +
      "&subject=" + subject + "&page=" + pageNumber + "&size=" + size;

      let headers     = new HttpHeaders({
          'Content-Type':'application/json',
          'X-Requested-Url':url,
          'X-Requested-Method':'GET',
          'Authorization': Authorization
        });
        let options     = { headers: headers };
    
        return this.http.get(this.urlServices.baseProxy, options)
        .map(this.extractData)
        .catch(this.handleError);
  }

  postEmailTemplate(emailTemplates, Authorization) {
      const url = this.urlServices.diskomApi + "/emailtemplateapi";
      let body:any = emailTemplates;
      let headers     = new HttpHeaders({
          'Content-Type':'application/json',
          'X-Requested-Url':url,
          'X-Requested-Method':'POST',
          'Authorization': Authorization
        });
        let options     = { headers: headers };
    
        return this.http.post(this.urlServices.baseProxy, body, options)
        .map(this.extractData)
        .catch(this.handleError);
  }

  //res untuk edit email template
  editEmailTemplate(id, emailEdit, Authorization) {
      const url = this.urlServices.diskomApi + "/emailtemplateapi" + "?ids=" + id;
      let headers     = new HttpHeaders({
          'Content-Type':'application/json',
          'X-Requested-Url':url,
          'X-Requested-Method':'PUT',
          'Authorization': Authorization
        });
        let options     = { headers: headers };
    
        return this.http.put(this.urlServices.baseProxy, emailEdit, options)
        .map(this.extractData)
        .catch(this.handleError);
  }

  // rest untuk delete email template
  deleteEmailTemplate(id, username, Authorization) {
      const url = this.urlServices.diskomApi + "/emailtemplateapi" + "?ids=" + id + "&username=" + username ;
      let headers     = new HttpHeaders({
          'Content-Type':'application/json',
          'X-Requested-Url':url,
          'X-Requested-Method':'DELETE',
          'Authorization': Authorization
        });
        let options     = { headers: headers };
    
        return this.http.delete(this.urlServices.baseProxy,  options )
        .map(this.extractData)
        .catch(this.handleError);
  }

  private extractData(body: any) {
      return Object.assign(body.data || body);
  }
  
  private handleError(error: HttpErrorResponse | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    let errObj: any;

    if (error instanceof HttpErrorResponse) {
      const err = error.message || JSON.stringify(error);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errObj = error.message;
    } else {
      errMsg = error.message ? error.message : error.toString();
      const body = error.message || '';
      errObj = body;
    }
    return Observable.throw(errObj);
  }
}